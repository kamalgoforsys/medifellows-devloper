<style>
.content{
    padding: 8px 31px !important;
    font-size: 13px  !important;
}
</style>
@include('email_templates.header')
    <tr>
        <td class="content">
            <span><strong>By : </strong> {{ @$params['name'] }} ({{ @$params['email'] }}) </span><br/>
            <span><strong>Subject : </strong> {{ @$params['title'] }} </span><br/>
        </td>
    </tr>
    <tr>
        <td style="text-align: center;">
            <strong>Message </strong>
        </td>
    </tr>
     <tr>
        <td class="content">
          {!! @$params['message'] !!}
        </td>
    </tr>
@include('email_templates.footer')