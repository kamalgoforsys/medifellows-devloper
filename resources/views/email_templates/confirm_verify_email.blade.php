@include('email_templates.header')
 
                <tr>
                    <td class="content">

                        <h5>Dear {{ ucfirst($params['user_name']) }}</h5>
                        
                        <p>Please confirm your email address by clicking on the button below. We'll communicate with you from time to time via email so it's important that we have an up-to-date email address on file. </p>

                        <table>
                            <tr>
                                <td align="center">
                                    <p>
                                        <a href="{{url('api/confirm-email-verify',['email'=>base64_encode($params['email'])])}}" class="button">Verify Your E-mail</a>
                                    </p>
                                </td>
                            </tr>
                        </table>
                        
                        <br/>
                        <p>Regards,<br/>
                        <em>Team MediFellows</em></p>                        
                    </td>
                </tr>
            </table>

        </td>
    </tr>
   
@include('email_templates.footer')