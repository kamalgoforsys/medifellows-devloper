<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;
class Event extends Model
{
    public function event_admin(){
        return $this->hasOne('App\User','id','user_id');
    } 
    public function eventhost(){
        return $this->hasOne('App\User','id','event_host');
    } 
    public function tag(){
        return $this->hasOne('App\Speciality','id','event_tags');
    } 
    public function guest(){
        return $this->hasOne('App\Event_joined_user','id','event_id');
    }
    public function totalgoing(){
        return $this->hasMany('App\Event_joined_user','event_id','id')->where('response_type','1');
        //->select(DB::raw('count(id) as total'))->where('response_type','1');
    } 
    public function commentsCount()
{
  return $this->totalgoing()
    ->selectRaw('count(*) as aggregate')
    ->groupBy('id');
}
}
