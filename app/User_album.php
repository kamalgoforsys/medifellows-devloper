<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class User_album extends Model
{
    public function user_last_post(){
        return $this->hasOne('App\User_post','album_id','id')->take(1)
        ->orderBy('id', 'DESC');
    }
    public function thumnail(){
        return $this->hasOne('App\User_media','user_post_id','id')->take(1)->orderBy('id', 'DESC');
    }
}
