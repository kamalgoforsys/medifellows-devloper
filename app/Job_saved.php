<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class Job_saved extends Model
{
    protected $table='job_saved';
	public function job_info(){
        return $this->belongsTo('App\JobPost','job_id','id')->with('job_employment_type','job_category');  
        
    }
}
