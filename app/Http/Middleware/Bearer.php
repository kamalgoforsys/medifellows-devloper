<?php

namespace App\Http\Middleware;
use Closure;
use Illuminate\Support\Facades\Auth;
use DB;
class Bearer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null){
        // $headers = apache_request_headers();
        // dd($headers);
        if($request->bearerToken()!='')
        {
            if($request->bearerToken()=='fAFOOrMiRNSEjOJoelhoDjoKhVokoRvxKwRkouRVNdyxQUdDmCQnVtQyNzqGtwglfUHgJYvitsTyIVMBNkwOpYvqMgzKisfgSFfHCGvRWfRATMhkHTMmaWllbRoyaCaYIPYnrPzcslAetbYavGEEOKeSWvcuNbuxWdrboNbkkVAWssoFKSGdcVxxxzJfdjdciHSUOLscQnGpQFJaXraiAEljjoKkLVbSiCJyLrAabrAdOkaXFcSrUfomQDyVwYUkOXixAFactmEraPeqQDHsoSbVXyxljdYyZhavXGDlbxpuvFPldXgddsEsLkKYARjBybykgkDVFpfJHRUrzMNDXnZhFSHvHvJXKVRpXQTFgYGJwlHfEHyeKRlddakllyTrONgGOXUiBNqWytrRwnKKesADLfRjkPwPAmnuQUjGZBemjVUOBWZSAXnwhASWUWqdSSBNMhyfMhDhYewcuwecHnsqezrNLdacvhvWDSWATfoodBgnevRZfUaDPgfXpHPHSeaDCoKANpippngobqgulPqaRpKxEdRqTKjhZlFnTKSBkwhVnqEMLlUsAKBFFbgGJOmwcGggZtkQhQtjikGTGKpUyVoEsFPCShJejnyVVDEBwMJeHqGnAMAOGIaeCXtSTZrvtPzaLrAxHXNmHTTlmNjaVYWtxkZsLWhFmtlzyCbpTUncTLKKPgiIBhGrYdlbhEIndNwKXRVtEQZYffRoxsQAvKXNYxNHcxtARyMweVjCLxLKejGCSFRpZjMuUgqMabCWstErBQzRTjBiQYVOupTxfvEoLBvkpfXktZBeiprstfxwMPtVdZdRLMmbVfEnzbcUnKnpWCKVvrLXJwCAHykMfjyvuJgYsvySfADtyUFpcscIFolsEBIAwzCnIqTLCpGTNNyorIqMpDqCVSqQqBTYhmbrWfogoolKSYyZjJHPVfNjgpKJnsDWwgYROeAaNVBtQqgCnrYpwgApZrcQfbbXcsbFmSNTenELDBviGjwsfkGgElZQVniKLmqhfEZm'){
                return $next($request);
            }
        }
    
        return response()->json(['status'=>FALSE,'msg'=>'Unauthorized Bearer Token.'],401);
    }
}