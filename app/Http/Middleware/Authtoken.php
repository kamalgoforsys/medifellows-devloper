<?php

namespace App\Http\Middleware;
use Closure;
use Illuminate\Support\Facades\Auth;
use DB;
class Authtoken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null){
        $headers = apache_request_headers();
        //dd($request->header('token'));
        
        if($request->header('token')!='' )
            {
                $rs = DB::table('users')->where('remember_token',$request->header('token'))->get();
                if(count($rs)>0){
                    return $next($request);
                }
            }
    
        return response()->json(['status'=>FALSE,'msg'=>'Unauthorized Token.'],401);
    }
}