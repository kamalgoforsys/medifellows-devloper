<?php
namespace App\Http\Controllers;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Validator; 
use App\User,App\Country,App\User_contact_detail,App\User_qualification,App\User_intern_info,App\User_workspace,App\User_certificate_and_registration,App\User_post,App\Follows,App\User_media,App\User_album,App\Post_meta,App\Event_joined_user,App\Event,App\User_friend,App\Marketplace_category,App\Marketplace,App\Librery,App\User_additional_info,App\Company,App\User_block,App\Title,App\Field_of_interest,App\Marketplace_saved,App\Marketplace_count,App\Marketplace_gallery,App\Event_co_hosts;
use DB;
use App\Group,App\GroupMember,App\GroupPost,App\GroupMedia,App\Group_report,App\Group_mute,App\Group_blocked,App\Post_tag;
use App\JobPost,App\JobApplyUser,App\JobEducation,App\Job_saved,App\Job_employment_type,App\Job_seniority_level,App\Job_count,App\User_mute,App\Group_co_admin,App\Event_attachment,App\Field;
use App\Profile_blocked,App\Profile_muted,App\Profile_muted_group,App\Profile_blocked_group,App\Comment_meta;
use App\Group_meta,App\Group_comment_meta,App\Album_tag;
use App\EventPost,App\EventMedia,App\Event_meta,App\Event_comment_meta;
use App\Notification;
use App\Page,App\Page_meta,App\Page_media,App\PagePost,App\Page_comment_meta,App\Page_category,App\Page_review,App\Page_follow;

class ApiController_V1 extends Controller
{
    private $core = 0;
    private $msg_array=NULL;
    private $img_path = '';
    public function __construct()
    {
        
        $this->img_path = public_path('/uploads/avatar/');
        $this->core = app(\App\Http\Controllers\CoreController::class);
    }
/*****************Page Module******************** */
    public function myPages(Request $request) 
    {
        $pages=Page::where('user_id',$request->user_id)->get();
        if(count($pages)>0)
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1'],'data'=>$pages]); 
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400);
    }
    public function addUpdatePage(Request $request)
    {
        $res = $this->validation($request,'user_id,title,about,category_id,number,email,message_setting,publish_setting');
        if($res){
            return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        if(isset($request->id))
        { 
            $result=Page::where('id',$request->id)->update($request->except(['id']));
        
        }
        else
        {
            $result=Page::insert($request->all());
        }

        if($result)
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]); 
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400); 


    }
    public function updatePublish(Request $request)
    {
        $res = $this->validation($request,'id,publish_setting');
        if($res){
            return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        $result=Page::where('id',$request->id)->update($request->except(['id']));
        if($result)
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]); 
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400); 
    }
    public function pageDetails(Request $request)
    {
        $res = $this->validation($request,'user_id,page_id');
        if($res){
            return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        $result=Page::where('id',$request->page_id)->first();
        if($result)
        {
            /*************check review by login user*************** */
            $review_by_user=Page_review::where('user_id',$request->user_id)->where('page_id',$request->page_id)->first();
            if($review_by_user)
            $status=$review_by_user->review;
            else
            $status=0;
            /*************check page avrage review************** */
            $avrage = Page_review::where('page_id',$request->page_id)->avg('review');
            if($avrage)
            $total_avrage=round($avrage,2);
            else
            $total_avrage=0;
            /*************check page follow by login user********** */
            $is_page_follow=Page_follow::where('user_id',$request->user_id)->where('page_id',$request->page_id)->first();
            if($is_page_follow)
            $is_page_follow=1;
            else
            $is_page_follow=0;

            $data=array('details'=>$result,'review_by_me'=>$status,'avrage_review'=>$total_avrage,'is_page_follow'=>$is_page_follow);

            return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1'],'data'=>$data]); 
           
        }
        
        else
        {return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400);}
    }
    public function pageAddPost(Request $request)
    {   

        $res = $this->validation($request,'user_id,page_id,type');
        if($res){
            return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        
        //////group details
        $page_datails=Page::where('id',$request->page_id)->first();
        
        if($page_datails)
        {
            $post_id = PagePost::insertGetId([
                'user_id'=>$request->user_id,
                'page_id'=>$request->page_id,
                'type'=>$request->type,
                'location'=>$request->location,
                'post_text'=>$request->post_text
            ]);
        if($request->type=='media')
        {
            if(!$request->media && $post_id>0){
                return response()->json(['status'=>FALSE,'msg'=>'Media is missing'],400);
            } else {
                $media=json_decode($request->media);
                foreach($media as $raw)
                {
                    $media_array[]=array(
                        'page_id'=>$request->page_id,
                        'media_type'=>$raw->type,
                        'page_media'=>$raw->media,
                        'page_post_id'=>$post_id
                    );
                }
                Page_media::insert($media_array);
            }
        }
        if($post_id){
            $user_info=User::select('full_name')->where('id',$request->user_id)->first();
            $page_info=Page::where('id',$request->page_id)->with('page_admin')->first();
            if($request->user_id!=$page_info->user_id){
            Notification::insert(['user_id'=>$page_info->user_id,'type'=>'Page_post','post_id'=>$post_id,'msg'=>@$user_info->full_name.' made a post on your page']);
            if(@$page_info->page_admin->device_token){
                $msg=array('title'=>'New post on page '.@$page_info->title,'body'=>@$user_info->full_name.' made a post on your page '.@$page_info->title,'sound'=>'default');
                $data=array('url'=>'','type'=>'Page_new_post','post_type'=>'Page_post','id'=>$post_id);
                $fields=array('to'=>$page_info->page_admin->device_token,'data'=>$data,'notification'=>$msg,'priority'=>'high','content_available'=>true);
                $this->send_messages_android($fields);
                }
            }
            
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['POST_ADD_SUCCESS']]); 
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
        
        }
        else
        {
            return response()->json(['status'=>FALSE,'msg'=>'invalid page id'],400);   
        }
    }
    public function pagePosts(Request $request)
    {
        
        $user_array=array();
        $user_id=$request->user_id;
        //DB::enableQueryLog();dd(DB::getQueryLog());
        $posts = PagePost::where('page_id',$request->page_id)->withCount('like_count','comment_count','share_count')->with(['is_like'=> function ($query) use ($user_id) {
            $query->where('user_id', $user_id);
        },'user_info','page_post_media'])->orderBy('created_at', 'desc')->paginate(10, ['*'], '', $request->page);
        //dd(DB::getQueryLog());
        //dd($posts);
        if(count($posts)>0) {
            foreach($posts as $raw)
            { 
            
                if (count($raw->is_like)>0)
                $is_liked=1;
                else
                $is_liked=0;

                $media=array();
                if($raw->type=='media')
                {
                    foreach($raw->page_post_media as $media_raw)
                    {
                        $media[]=array('media_type'=>$media_raw->media_type,'media_url'=>$media_raw->page_media);  
                    }
                }
                
                
                $user_array[]=array(
                    "post_id"=>$raw->id,
                    "post_type"=>'Page_post',
                    "user_id"=>$raw->user_id,
                    "post_by"=>@$raw->user_info->full_name,
                    "profile_image"=>@$raw->user_info->profile_image,
                    "date_of_post"=>date('d-m-Y',strtotime($raw->created_at)),
                    "time_of_post"=>date('h:i:s A',strtotime($raw->created_at)),
                    "location_of_post"=>$raw->location,
                    "text_message"=>$raw->post_text,
                    "media"=>$media,
                    "total_number_of_likes"=>$raw->like_count_count,
                    "total_number_of_comments"=>$raw->comment_count_count,
                    "total_number_of_share"=>$raw->share_count_count,
                    "is_liked"=>$is_liked
                    ); 
                
            }
        }
        if(count($user_array)>0)
        return response()->json(['status'=>TRUE,"msg"=>config('constants')['SUCCESS'],'data'=>$user_array],200, [], JSON_NUMERIC_CHECK);
        else
        return response()->json(['status'=>TRUE,"msg"=>config('constants')['NOT_FOUND'],'data'=>[]]);
    }
    public function pagePostAllLike(Request $request)
    {
        $post=PagePost::where('id',$request->post_id)->first();  
        if($post) {
        $user_id=$post->user_id;

        $all_like=Page_meta::where('post_id',$request->post_id)->where('type','Like')->with(['like_user_info','is_friend' => function ($q) use($user_id) {
            $q->where('user_id','=',$user_id);
            }
            ])->get();
        //dd($all_like);
        if(count($all_like)>0)
        {
            foreach($all_like as $raw)
            {
                    if($raw->is_friend)
                    $is_friend=1;
                    else
                    $is_friend=0;
                    $data[]=array(
                    "user_id"=>$raw->like_user_info->id,
                    "full_name"=>$raw->like_user_info->full_name,
                    "profile_pic"=>$raw->like_user_info->profile_image,
                    "color"=>$raw->like_user_info->user_title,
                    "is_friend"=>$is_friend );
            }
            return response()->json(['status'=>TRUE,"msg"=>config('constants')['SUCCESS'],'data'=>$data],200, [], JSON_NUMERIC_CHECK);
        }
        return response()->json(['status'=>FALSE,"msg"=>config('constants')['NO_LIKE_POST']],400);
        }
        else
        {
            return response()->json(['status'=>FALSE,"msg"=>'Invalid Post Id'],400);
        }
    }
    public function pagePostLikeDislike(Request $request)
    {   //dd(GroupPost::where('id',$request->post_id)->first());
        $res = $this->validation($request,'loggedin_user_id,post_user_id,post_id,current_status');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        $group=PagePost::select('id')->where('id',$request->post_id)->first(['id']);
        
        if(@$group->id){
        if($request->current_status==0)
        {
        $like=Page_meta::where('user_id',$request->loggedin_user_id)->where('post_id',$request->post_id)->where('type','Like')->first();
        if($like){ $like->delete(); }
        }
        else
        {
            $user_info=User::select('full_name')->where('id',$request->loggedin_user_id)->first();
            $friend_info=User::select('device_token')->where('id',$request->post_user_id)->first();
            Notification::insert(['user_id'=>$request->post_user_id,'type'=>'Page_post','post_id'=>$request->post_id,'msg'=>@$user_info->full_name.' Like Your Post']);
            if($friend_info->device_token){
                $msg=array('title'=>'New Like On Post','body'=>@$user_info->full_name.' Like Your Post','sound'=>'default');
                $data=array('url'=>'','type'=>'post_like','post_type'=>'Page_post','id'=>$request->post_id);
                $fields=array('to'=>$friend_info->device_token,'data'=>$data,'notification'=>$msg,'priority'=>'high','content_available'=>true);
                $this->send_messages_android($fields);
                }
        $like=Page_meta::updateOrCreate(['type'=>'Like','post_id'=>$request->post_id,'user_id'=>$request->loggedin_user_id],['type'=>'Like','post_user_id'=>$request->post_user_id,'user_id'=>$request->loggedin_user_id,'post_id'=>$request->post_id]);
        }
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]); 
        }
        else
        {
        return response()->json(['status'=>FALSE,'msg'=>'Invalid Post Id']);    
        }
    }
    public function pagePostComments(Request $request)
    {
        $user_id=$request->user_id;
        $comments=Page_meta::where('type','Comment')->where('post_id',$request->post_id)->with(['is_like'=> function ($query) use ($user_id) {
            $query->where('user_id', $user_id);
        },'comment_like','comment_reply','comment_user_info'])->get();
        
        if(count($comments)>0)
        {
            foreach($comments as $raw)
            {       if($raw->is_like)
                    $is_like=1;
                    else
                    $is_like=0;
                    $data[]=array(
                    "user_id"=>$raw->like_user_info->id,
                    "profile_pic"=>$raw->like_user_info->profile_image,
                    "user_full_name"=>$raw->like_user_info->full_name,
                    "comment_id"=>$raw->id,
                    "comment_text"=>$raw->comment_text,
                    "is_like"=>$is_like,
                    "like_count"=>count($raw->comment_like),
                    'comment_reply'=>$raw->comment_reply
                );
            }
            return response()->json(['status'=>TRUE,"msg"=>config('constants')['SUCCESS'],'data'=>$data],200, [], JSON_NUMERIC_CHECK);
        }
        return response()->json(['status'=>FALSE,"msg"=>config('constants')['NO_POST_COMMENTS']],400);
    }
    public function pagePostAddComment(Request $request)
    {
        $res = $this->validation($request,'loggedin_user_id,post_user_id,post_id,comment_text');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        $request->merge(['user_id'=>$request->loggedin_user_id,'type'=>'Comment']);
        $comment=Page_meta::insert($request->except(['loggedin_user_id','post_type']));
        if($comment)
        {
            $user_info=User::select('full_name')->where('id',$request->loggedin_user_id)->first();
            $friend_info=User::select('device_token')->where('id',$request->post_user_id)->first();
            Notification::insert(['user_id'=>$request->post_user_id,'type'=>'Page_post','post_id'=>$request->post_id,'msg'=>@$user_info->full_name.' Comment on Your Post']);
            if($friend_info->device_token){
                $msg=array('title'=>'New Comment On Post','body'=>@$user_info->full_name.' Comment on Your Post','sound'=>'default');
                $data=array('url'=>'','type'=>'post_comment','post_type'=>'Page_post','id'=>$request->post_id);
                $fields=array('to'=>$friend_info->device_token,'data'=>$data,'notification'=>$msg,'priority'=>'high','content_available'=>true);
                $this->send_messages_android($fields);
                }
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]);
        }
        else
        {return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);}
        
    }
    public function pagePostEditComment(Request $request)
    {
        $res = $this->validation($request,'comment_id,comment_edited_text');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        $request->merge(['comment_text'=>$request->comment_edited_text]);
        $comment=Page_meta::where('id',$request->comment_id)->update($request->except(['post_type','comment_edited_text','comment_id']));
        if($comment)
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]); 
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400); 
        
    }
    public function pagePostDeleteComment(Request $request)
    {
        $res = $this->validation($request,'comment_id,loggedin_user_id');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        
        $comment=Page_meta::where('user_id',$request->loggedin_user_id)->where('id',$request->comment_id)->first();
        if($comment){ $comment->delete(); }
        if($comment)
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]); 
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400); 
        
    }
    public function pageCommentLikeDislike(Request $request)
    {
        $res = $this->validation($request,'user_id,comment_id');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        if($request->current_status=='0')
        {
        $like=Page_comment_meta::where('user_id',$request->user_id)->where('comment_id',$request->comment_id)->where('type','Like')->first();
        if($like){ $like->delete(); }
        }
        else
        {
            $post_id=Page_meta::select('post_user_id','post_id')->where('id',$request->comment_id)->first();
            $user_info=User::select('full_name')->where('id',$request->user_id)->first();
            $friend_info=User::select('device_token')->where('id',$post_id->post_user_id)->first();
            Notification::insert(['user_id'=>$post_id->post_user_id,'comment_id'=>$request->comment_id,'type'=>'Page_post','post_id'=>@$post_id->post_id,'msg'=>@$user_info->full_name.' Like your Comment']);
            if($friend_info->device_token){
                $msg=array('title'=>'New Comment Like','body'=>@$user_info->full_name.' Like your Comment','sound'=>'default');
                $data=array('url'=>'','type'=>'post_comment_like','post_type'=>'Page_post','id'=>@$post_id->post_id);
                $fields=array('to'=>$friend_info->device_token,'data'=>$data,'notification'=>$msg,'priority'=>'high','content_available'=>true);
                $this->send_messages_android($fields);
                }
        $like=Page_comment_meta::updateOrCreate(['type'=>'Like','comment_id'=>$request->comment_id,'user_id'=>$request->user_id],['type'=>'Like','comment_id'=>$request->comment_id,'user_id'=>$request->user_id]);
        }
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]);
    }
    public function pageCommentReply(Request $request)
    {
        $res = $this->validation($request,'user_id,comment_id,text');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
            
        Page_comment_meta::insert(['type'=>'Reply','comment_id'=>$request->comment_id,'user_id'=>$request->user_id,'text'=>$request->text]);
        $post_id=Page_meta::select('post_user_id','post_id')->where('id',$request->comment_id)->first();
        $user_info=User::select('full_name')->where('id',$request->user_id)->first();
        $friend_info=User::select('device_token')->where('id',$post_id->post_user_id)->first();
        Notification::insert(['user_id'=>$post_id->post_user_id,'type'=>'Page_post','post_id'=>@$post_id->post_id, 'comment_id'=>$request->comment_id,'msg'=>@$user_info->full_name.' Reply on your Comment']);
        if($friend_info->device_token){
            $msg=array('title'=>'New Comment Reply','body'=>@$user_info->full_name.' Reply on your Comment','sound'=>'default');
            $data=array('url'=>'','type'=>'post_comment_reply','post_type'=>'Page_post','id'=>@$post_id->post_id,'comment_id'=>$request->comment_id,'name'=>@$user_info->full_name,'reply'=>$request->text);
            $fields=array('to'=>$friend_info->device_token,'data'=>$data,'notification'=>$msg,'priority'=>'high','content_available'=>true);
            $this->send_messages_android($fields);
            }
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]);
    }
    public function pageDeletePost(Request $request)
    {
        $res = $this->validation($request,'post_id,user_id');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        
        $post_details=PagePost::where('user_id',$request->user_id)->where('id',$request->post_id)->first();
        if($post_details){ $post_details->delete(); }
        if($post_details)
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]); 
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400); 
        
    }
    public function pagePostShare(Request $request)
    {
        $res = $this->validation($request,'post_id,user_id,post_user_id');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        $request->merge(['type'=>'Share']);
        //$share=Post::insert($request->all());
        $share=Page_meta::insert($request->except(['post_type']));
        if($share)
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]); 
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400); 
        
    }
    public function pageMedia($id)
    {
       if(Page::where('id',$id)->first())
       {
         $data=Page_media::select('page_post_id','page_media','media_type')->where('page_id',$id)->get();
         return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1'],'data'=>$data],200, [], JSON_NUMERIC_CHECK);
       }
       else
       {
        return response()->json(['status'=>FALSE,'msg'=>'Invalid Page ID'],400);
       }
    }
    public function pageReview(Request $request)
    {
        $res = $this->validation($request,'user_id,page_id');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        $result=Page::where('id',$request->page_id)->first();
        if($result){
        $review_by_user=Page_review::where('user_id',$request->user_id)->where('page_id',$request->page_id)->first();
        
        if($review_by_user)
        $status=$review_by_user->review;
        else
        $status=0;
        
        $avrage = Page_review::where('page_id',$request->page_id)->avg('review');
        if($avrage)
        $total_avrage=round($avrage,2);
        else
        $total_avrage=0;
        
        $data=array('review_by_me'=>$status,'avrage_review'=>$total_avrage);

        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1'],'data'=>$data]); 
        }
        else{
        return response()->json(['status'=>FALSE,'msg'=>'Page Id Not Found'],400);
        }
    }
    public function pageAddReview(Request $request)
    {
        $res = $this->validation($request,'user_id,page_id,review');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }

        $result=Page_review::updateOrCreate(['user_id'=>$request->user_id,'page_id'=>$request->page_id],['user_id'=>$request->user_id,'page_id'=>$request->page_id,'review'=>$request->review]);
        if($result)
        {
            $user_info=User::select('full_name')->where('id',$request->user_id)->first();
            $page_info=Page::where('id',$request->page_id)->with('page_admin')->first();
            
            Notification::insert(['user_id'=>$page_info->user_id,'type'=>'Page_detail','msg'=>@$user_info->full_name.' has rated your page']);
            if(@$page_info->page_admin->device_token){
                $msg=array('title'=>'New rating page '.@$page_info->title,'body'=>@$user_info->full_name.' has rated your page '.@$page_info->title,'sound'=>'default');
                $data=array('url'=>'','type'=>'Page_ratting','id'=>$request->page_id);
                $fields=array('to'=>$page_info->page_admin->device_token,'data'=>$data,'notification'=>$msg,'priority'=>'high','content_available'=>true);
                $this->send_messages_android($fields);
                }

            return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]);
        }
        else
        {return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);}
    } 
    public function FollowPage(Request $request)
    {
        $res = $this->validation($request,'user_id,page_id,status');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        if($request->status==1){
        
            $user_info=User::select('full_name')->where('id',$request->user_id)->first();
            $page_info=Page::where('id',$request->page_id)->with('page_admin')->first();
            
            Notification::insert(['user_id'=>$page_info->user_id,'type'=>'Page_detail','msg'=>@$user_info->full_name.' has started Following your Page']);
            if(@$page_info->page_admin->device_token){
                $msg=array('title'=>'New Follow on page '.@$page_info->title,'body'=>@$user_info->full_name.' Start Following your page '.@$page_info->title,'sound'=>'default');
                $data=array('url'=>'','type'=>'Page_follow','id'=>$request->page_id);
                $fields=array('to'=>$page_info->page_admin->device_token,'data'=>$data,'notification'=>$msg,'priority'=>'high','content_available'=>true);
                $this->send_messages_android($fields);
                }
        Page_follow::updateOrCreate(['user_id'=>$request->user_id,'page_id'=>$request->page_id],['user_id'=>$request->user_id,'page_id'=>$request->page_id]);
        
        }else{
        $d=Page_follow::where('user_id',$request->user_id)->where('page_id',$request->page_id)->first();
        if($d){ $d->delete(); }
        }
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]); 

    }


/************************************************ */

    public function masterDelete(Request $request)
    {
        DB::table($request->type)->where('id',$request->id)->delete();
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']],200);    
    }
    public function testmail()
    {  
        $mailData['params']=['email'=>'kamalps008@gmail.com','user_id'=>1,'user_name'=>'kamal','subject'=>'E-mail Confirmation','template'=>'verify_email'];
        $d= $this->core->SendEmail($mailData);
        dd($d);
    }
    function send_messages_android($data=null)
    {
        //dd('ok');
        $url            = 'https://fcm.googleapis.com/fcm/send';
        $server_key     = 'AAAAfukOv0c:APA91bGkwGeKm09ZjFU3ZL7uRjT-40iJqYj6L_tqbrzR_i1EOvbAma3bTTwwo7TMYyT8Ogvy0aVDxNXduiPh-jC2_VJziYNJftcyJibXs_HIdgXkSJ5i32bDc-Vh4q1jpORcWInK1c4t';
        $headers = array(
            'Content-Type:application/json',
            'Authorization:key=' . $server_key,
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        $result = curl_exec($ch);
        //var_dump($result);die;
        if ($result === false) {
            die('FCM Send Error: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }
    public function logout($id)
    {
        User::where('id',$id)->update(['device_token'=>'']);
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']],200);    
    }

    public function validation($request,$fields){
        $array = explode(',', $fields);
        $error = [];
        foreach ($array as $key => $value) {
           
         if(strlen($request->$value)==0){
            $error[]= $value.' is Required';
         }
        }
       
        return $error;
       }
/********************login/Register API******************* */
    public function loginUserInfo(Request $request)
    {
        $res = $this->validation($request,'id');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        $user = User::where('id',$request->id)->with('user_title')->first();
            if($user){
                if($user->email_verified==0)
                $email_verified=false;
                else 
                $email_verified=true;
                
                if($user->mobile_verified==0)
                $mobile_verified=false;
                else 
                $mobile_verified=true;

                if($user->profile_completed==0)
                $profile_completed=false;
                else 
                $profile_completed=true;

                $data=array(
                    'id'=>$user->id,
                    'title'=>@$user->user_title->title,
                    'first_name'=>$user->first_name,
                    'last_name'=>$user->last_name,
                    'full_name'=>$user->full_name,
                    'dob'=>$user->dob,
                    'email'=>$user->email,
                    'profile_image'=>$user->profile_image,
                    'cover_image'=>$user->cover_image,
                    'mobile'=>$user->mobile,
                    'device_type'=>$user->device_type,
                    'device_token'=>$user->device_token,
                    'status'=>$user->status,
                    'country_id'=>$user->country_id,
                    'level_id'=>$user->level_id,
                    'specialitie_id'=>$user->specialitie_id,
                    'profession_id'=>$user->profession_id,
                    'profession_skill'=>$user->profession_skill,
                    'about_me'=>$user->about_me,
                    'email_verified'=>$email_verified,
                    'mobile_verified'=>$mobile_verified,
                    'profile_completed'=>$profile_completed,
                    'company_registration_number'=>$user->company_registration_number,
                    'remember_token'=>$user->remember_token
                    
                );
                if($user->email_verified==0)
                {
                    return response()->json(['status'=>TRUE,'msg'=>config('constants')['EMAIL_NOT_VERIFIED'],'data'=>$data]);       
                }
                if($user->profile_completed==0)
                {
                    return response()->json(['status'=>TRUE,'msg'=>config('constants')['PROFILE_NOT_COMPLETED'],'data'=>$data]);       
                }
                //dd($user->id);
                $user->remember_token=$this->mc_encrypt($user->id);
                if($request->device_type!='' && ($request->device_type=='Android' || $request->device_type=='IOS'))
                {
                $user->device_type = $request->device_type;
                if($request->device_token!='')
                {$user->device_token = $request->device_token;}
                
                }
                $user->save();
                return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS_LOGIN'],'data'=>$data]);
            
        }else
            return response()->json(['status'=>FALSE,'msg'=>config('constants')['INVALID_USERNAME_OR_EMAIL']],400);
    }
    public function login(Request $request)
    {
        $res = $this->validation($request,'username,password');
        if($res){
           return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
           $user = User::where('user_name',$request->username)->orWhere('email',$request->username)->with('user_title')->first();
            if($user){
               
               if(password_verify($request->password,$user->password)){

                   if($user->email_verified==0)
                   $email_verified=false;
                   else 
                   $email_verified=true;
                   
                   if($user->mobile_verified==0)
                   $mobile_verified=false;
                   else 
                   $mobile_verified=true;
   
                   if($user->profile_completed==0)
                   $profile_completed=false;
                   else 
                   $profile_completed=true;
   
                   $data=array(
                       'id'=>$user->id,
                       'title'=>@$user->user_title->title,
                       'first_name'=>$user->first_name,
                       'last_name'=>$user->last_name,
                       'full_name'=>$user->full_name,
                       'dob'=>$user->dob,
                       'email'=>$user->email,
                       'profile_image'=>$user->profile_image,
                       'cover_image'=>$user->cover_image,
                       'mobile'=>$user->mobile,
                       'device_type'=>$user->device_type,
                       'device_token'=>$user->device_token,
                       'status'=>$user->status,
                       'country_id'=>$user->country_id,
                       'level_id'=>$user->level_id,
                       'specialitie_id'=>$user->specialitie_id,
                       'profession_id'=>$user->profession_id,
                       'profession_skill'=>$user->profession_skill,
                       'about_me'=>$user->about_me,
                       'email_verified'=>$email_verified,
                       'mobile_verified'=>$mobile_verified,
                       'profile_completed'=>$profile_completed,
                       'company_registration_number'=>$user->company_registration_number,
                       'remember_token'=>$user->remember_token
                       
                   );
                   if($user->email_verified==0)
                   {
                       return response()->json(['status'=>TRUE,'msg'=>config('constants')['EMAIL_NOT_VERIFIED'],'data'=>$data]);       
                   }
                   if($user->profile_completed==0)
                   {
                       return response()->json(['status'=>TRUE,'msg'=>config('constants')['PROFILE_NOT_COMPLETED'],'data'=>$data]);       
                   }
                   //dd($user->id);
                   $user->remember_token=$this->mc_encrypt($user->id);
                   if($request->device_type!='' && ($request->device_type=='Android' || $request->device_type=='IOS'))
                   {
                   $user->device_type = $request->device_type;
                   if($request->device_token!='')
                   {$user->device_token = $request->device_token;}
                   
                   }
                   $user->save();
                   return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS_LOGIN'],'data'=>$data]);
               }else
                   return response()->json(['status'=>FALSE,'msg'=>config('constants')['INVALID_PASS']],400);    
           }else
               return response()->json(['status'=>FALSE,'msg'=>config('constants')['INVALID_USERNAME_OR_EMAIL']],400);
    }
    public function registration(Request $request)
    {   
          // dd($request->all());
           ////////////////////////input validations/////////////////////////////////////
           $res = $this->validation($request,'title,first_name,last_name,country_id,dob,email,password');
           if($res){
           return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
           }
           ///////////////////////Check email already exist//////////////////////////////
           if(User::where('email',$request->email)->count()){
               return response()->json(['status'=>FALSE,'msg'=>config('constants')['EMAIL_ALREADY_TAKEN']],400);
           }
           ////////////////////////insert user details///////////////////////////////////
           $title_name=Title::where('id',$request->title)->first();
           $request->merge(['password'=>Hash::make($request->password),'title_name'=>$title_name->title,'full_name'=>$title_name->title.' '.$request->first_name.' '.$request->last_name,'dob'=>date('Y-m-d',strtotime($request->dob))]);
           $user = User::insertGetId($request->all());
           if($user){
               
               $remember_token=$this->mc_encrypt($user);
               $data=array();
               $data=User::where('id',$user)->update(['remember_token'=>$remember_token]);
               $uData =User::where('id',$user)->with('user_title')->first();
               $mailData['params']=['email'=>$uData->email,'user_id'=>$uData->id,'user_name'=>$uData->full_name,'subject'=>'E-mail Confirmation','template'=>'verify_email'];
               $d= $this->core->SendEmail($mailData);
              
               if($uData->email_verified==0)
               $email_verified=false;
               else 
               $email_verified=true;
               
               if($uData->mobile_verified==0)
               $mobile_verified=false;
               else 
               $mobile_verified=true;
   
               if($uData->profile_completed==0)
               $profile_completed=false;
               else 
               $profile_completed=true;
   
               $data=array(
                   'id'=>$uData->id,
                   'title'=>$uData->user_title->title,
                   'first_name'=>$uData->first_name,
                   'last_name'=>$uData->last_name,
                   'full_name'=>$uData->full_name,
                   'dob'=>$uData->dob,
                   'email'=>$uData->email,
                   'profile_image'=>$uData->profile_image,
                   'cover_image'=>$uData->cover_image,
                   'mobile'=>$uData->mobile,
                   'device_type'=>$uData->device_type,
                   'device_token'=>$uData->device_token,
                   'status'=>$uData->status,
                   'country_id'=>$uData->country_id,
                   'level_id'=>$uData->level_id,
                   'specialitie_id'=>$uData->specialitie_id,
                   'profession_id'=>$uData->profession_id,
                   'profession_skill'=>$uData->profession_skill,
                   'about_me'=>$uData->about_me,
                   'email_verified'=>$email_verified,
                   'mobile_verified'=>$mobile_verified,
                   'profile_completed'=>$profile_completed,
                   'company_registration_number'=>$uData->company_registration_number,
                   'remember_token'=>$uData->remember_token
                   
               );
               
               return response()->json(['status'=>TRUE,'msg'=>config('constants')['REG_SUCCESS'],'data'=>$data]);
           }
           return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
    }

/*********************login/Register End************** */
/******************Complete & update Profile****************** */         
    public function completeProfile(Request $request)
    {
          $res = $this->validation($request,'user_id,qualification_category');
            if($res){
            return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
            }
            if(User::where('id',$request->user_id)->first()){ 
            // qualification_category :: 1=Student 2=Qualified 3=Other
            if($request->qualification_category==1){
                
                $student_intern_info=json_decode($request->student_info);
                if($student_intern_info)
                {
                    $student_intern_info_array=array(
                            'user_id'=>$request->user_id,
                            'university_id'=>$student_intern_info->university_id,
                            'intern_id'=>$student_intern_info->intern_id,
                            
                            'country_id'=>$student_intern_info->country_id,
                            'university_name'=>$student_intern_info->university_name,
                            'intern_name'=>$student_intern_info->intern_name
                    );
                    
                User_intern_info::insert($student_intern_info_array);
                }
                else
                {
                    return response()->json(['status'=>FALSE,'msg'=>'Student info Missing'],400);  
                }
            } 
            else if($request->qualification_category==2){
                $professional_info=json_decode($request->professional_info);
                if($professional_info){
                foreach($professional_info->qualification as $raw)
                    {
                        $qualification_array=array(
                                        'user_id'=>$request->user_id,
                                        'default_qualification'=>$raw->default_qualification,
                                        'speciality_id'=>$raw->speciality_id,
                                        'qualification_id'=>$raw->qualification_id,
                                        'country_id'=>$raw->country_id,
                                        'registration_number'=>$professional_info->certificate_info->registration_number,
                                        'certificate'=>$professional_info->certificate_info->certificate,
                                        );
                    }
                   
                    
                    foreach($professional_info->work_place_info as $raw1)
                    {
                    $current_place_of_work_array[]=array('user_id'=>$request->user_id,
                                    'current_work_place_name'=>$raw1->current_work_place_name,
                                    'country_id'=>$raw1->country_id,
                                    'position'=>$raw1->position
                                    
                                    );
                    }
                    foreach($professional_info->field_of_interest as $raw2)
                    {
                    $field_of_interest_array[]=array('user_id'=>$request->user_id,
                                    'field_id'=>$raw2->field_id
                                                        );
                    }
                    User_workspace::insert($current_place_of_work_array);
                    User_qualification::insert($qualification_array);
                    Field_of_interest::insert($field_of_interest_array);
                                }else
                                {
                                    return response()->json(['status'=>FALSE,'msg'=>'Professional info Missing'],400);      
                                } 
                } else if($request->qualification_category==3) {

                $other_info=json_decode($request->other_info);
                if($other_info){
                    foreach($other_info as $raw3){
                    $user_additional_info[]=array(
                        'user_id'=>$request->user_id,
                        'position'=>$raw3->position,
                        'current_work_place_name'=>$raw3->workplace,
                        'practice_no'=>$raw3->practice_no,
                        'country_id'=>$raw3->country_id
                        
                );
                }
            
                User_workspace::insert($user_additional_info);
            }
            else
            {
                return response()->json(['status'=>FALSE,'msg'=>'Other info Missing'],400);       
            }
                    
            }
            else
            {
                return response()->json(['status'=>FALSE,'msg'=>config('constants')['INVALID_Q_CATEGORY']],400);
            }
                        
            $data=array('profile_image'=>$request->profile_image,
                        'qualification_category'=>$request->qualification_category,
                        'profile_completed'=>1
                    );
            $data=User::where('id',$request->user_id)->update($data);
            if($data){
                return response()->json(['status'=>TRUE,'msg'=>config('constants')['PROFILE_UPDATE']]);
            }
            return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
            }
            else
            {
            return response()->json(['status'=>FALSE,'msg'=>'User id Not Found'],400);
            }
    }
    public function updateCoverPic(Request $request)
    {
     $res = $this->validation($request,'user_id,cover_image');
     if($res){
         return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
     }
     $data=User::where('id',$request->user_id)->update($request->except(['user_id']));
     if($data){
     ////// add profile pic add Post ///////
     $user_post_array=array(
         'user_id'=>$request->user_id,
         'post_type'=>'cover_pic',
         'type'=>'media',
         'album_id'=>2,
      );
     $post_id=User_post::insertGetId($user_post_array);

     //////// update user media //////
     $user_media_array=array(
         'user_id'=>$request->user_id,
         'type'=>'cover_image_update',
         'url'=>$request->cover_image,
         'user_post_id'=>$post_id,
         'album_id' =>2
         
      );
     User_media::insert($user_media_array);
     return response()->json(['status'=>TRUE,'msg'=>config('constants')['PROFILE_COVER_UPDATE']]);
     }
     return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
    }
    public function getProfile($id)
    {
            if(!$id){
                return response()->json(['status'=>FALSE,'msg'=>'User id missing'],400);
                }  
            $user_profile=User::where('id',$id)->with('user_title','user_intern_infos','user_qualification','current_work_place','user_website','user_mobile','field_of_interest')->first();
            if($user_profile)
            {

              $title= $user_profile->user_title;

              $intern_info=array();
              $user_qualification=array();
              $current_work_place=array();
              $user_website=array();
              $user_mobile=array();
              $field_of_interest=array();
              $university_name='';
              $speciality_title='';
              $current_work_place_name='';
              foreach($user_profile->user_intern_infos as $raw_intern)
              {
                $university_name=$raw_intern->university_name;
                $intern_info[]=array( 
                "id"=>$raw_intern->id,
                "user_id"=>(int)$raw_intern->user_id,
                "university_id"=>(int)$raw_intern->university_id,
                "intern_id"=>(int)$raw_intern->intern_id,
                "practice_number"=>$raw_intern->practice_number,
                "country_id"=>(int)$raw_intern->country_id,
                "university_name"=>$raw_intern->university_name,
                "intern_name"=>$raw_intern->intern_name,
                "created_at"=>date('Y-m-d',strtotime($raw_intern->created_at)),
                "updated_at"=>date('Y-m-d',strtotime($raw_intern->updated_at))
                );
              }
              foreach($user_profile->user_qualification  as $raw_qualification)
              {
                if((int)$raw_qualification->default_qualification==1){  
                $speciality_title=$raw_qualification->speciality[0]->title;
                }
                  $q=array();
                  $s=array();
                  $q[]=array(
                        "id"=>(int)@$raw_qualification->qualification[0]->id,
                        "title"=>$raw_qualification->qualification[0]->title,
                        "code"=>$raw_qualification->qualification[0]->code,
                        "created_at"=>date('Y-m-d',strtotime($raw_qualification->qualification[0]->created_at)),
                        "updated_at"=>date('Y-m-d',strtotime($raw_qualification->qualification[0]->updated_at))
                           );
                  $s[]=array(
                    "id"=>(int)@$raw_qualification->speciality[0]->id,
                    "title"=>$raw_qualification->speciality[0]->title,
                    "icon"=>$raw_qualification->speciality[0]->icon,
                    "created_at"=>date('Y-m-d',strtotime($raw_qualification->speciality[0]->created_at)),
                    "updated_at"=>date('Y-m-d',strtotime($raw_qualification->speciality[0]->updated_at))
                  );          
                $user_qualification[]=array(
                    "id"=>(int)$raw_qualification->id,
                    "user_id"=>(int)$raw_qualification->user_id,
                    "qualification_id"=>(int)$raw_qualification->qualification_id,
                    "speciality_id"=>(int)$raw_qualification->speciality_id,
                    "default_qualification"=>(int)$raw_qualification->default_qualification,
                    "country_id"=>(int)$raw_qualification->country_id,
                    "registration_number"=>$raw_qualification->registration_number,
                    "certificate"=>$raw_qualification->certificate,
                    "created_at"=>date('Y-m-d',strtotime($raw_qualification->created_at)),
                    "updated_at"=>date('Y-m-d',strtotime($raw_qualification->updated_at)),
                    "qualification"=>$q,
                    "speciality"=>$s,
                    );
              }
              
              foreach($user_profile->current_work_place as $raw_work)
              {
                $current_work_place_name=$raw_work->current_work_place_name;
                $current_work_place[]=array(
                    "id"=>(int)$raw_work->id,
                    "user_id"=>(int)$raw_work->user_id,
                    "position"=>$raw_work->position,
                    "current_work_place_name"=>$raw_work->current_work_place_name,
                    "country_id"=>(int)$raw_work->country_id,
                    "practice_no"=>$raw_work->practice_no,
                    "from_date"=>$raw_work->from_date,
                    "to_date"=>$raw_work->to_date,
                    "status"=>(int)$raw_work->status,
                    "created_at"=>date('Y-m-d',strtotime($raw_work->created_at)),
                    "updated_at"=>date('Y-m-d',strtotime($raw_work->updated_at))
                  );
              }
              
              foreach($user_profile->user_website as $raw_user_website)
              {
                
                  $user_website[]=array(
                    "id"=>(int)$raw_user_website->id,
                    "user_id"=>(int)$raw_user_website->user_id,
                    "meta_type"=>$raw_user_website->meta_type,
                    "website"=>$raw_user_website->website,
                    "created_at"=>date('Y-m-d',strtotime($raw_user_website->created_at)),
                    "updated_at"=>date('Y-m-d',strtotime($raw_user_website->updated_at))
                  );
              }
              foreach($user_profile->user_mobile as $raw_user_mobile)
              {
                  $user_mobile[]=array(
                    "id"=>(int)$raw_user_mobile->id,
                    "user_id"=>(int)$raw_user_mobile->user_id,
                    "meta_type"=>$raw_user_mobile->meta_type,
                    "code"=>$raw_user_mobile->code,
                    "phone_number"=>$raw_user_mobile->phone_number,
                    "created_at"=>date('Y-m-d',strtotime($raw_user_mobile->created_at)),
                    "updated_at"=>date('Y-m-d',strtotime($raw_user_mobile->updated_at))
                  );
              }
              foreach($user_profile->field_of_interest as $r_field_of_interest)
              {
                  
                  $f=array(
                    "id"=>$r_field_of_interest->field->id,
                    "title"=>$r_field_of_interest->field->title,
                    "icon"=>$r_field_of_interest->field->icon,
                    "created_at"=>date('Y-m-d',strtotime($r_field_of_interest->field->created_at)),
                    "updated_at"=>date('Y-m-d',strtotime($r_field_of_interest->field->updated_at))
                  );

                $field_of_interest[]=array(
                "id"=>$r_field_of_interest->id,
                "user_id"=>(int)$r_field_of_interest->user_id,
                "field_id"=>(int)$r_field_of_interest->field_id,
                "updated_at"=>date('Y-m-d',strtotime($r_field_of_interest->updated_at)),
                "created_at"=>date('Y-m-d',strtotime($r_field_of_interest->created_at)),
                "field"=>$f
                  );
              }
              
              if((int)$user_profile->qualification_category==1)
              $cat_title=$university_name;
              else if ((int)$user_profile->qualification_category==2)
              $cat_title=$speciality_title;
              else if ((int)$user_profile->qualification_category==3)
              $cat_title=$current_work_place_name;
              else
              $cat_title='';

              //get user category info
              
              $data=array(
                "id"=>(int)$user_profile->id,
                "qualification_category"=>(int)$user_profile->qualification_category,
                'qualification_title'=>$cat_title,
                "title"=>(int)$user_profile->title,
                "first_name"=>$user_profile->first_name,
                "last_name"=>$user_profile->last_name,
                "full_name"=>$user_profile->full_name,
                "dob"=>$user_profile->dob,
                "email"=>$user_profile->email,
                "profile_image"=>$user_profile->profile_image,
                "cover_image"=>$user_profile->cover_image,
                "mobile"=>$user_profile->mobile,
                "device_type"=>$user_profile->device_type,
                "device_token"=>$user_profile->device_token,
                "status"=>(int)$user_profile->status,
                "country_id"=>(int)$user_profile->country_id,
                "about_me"=>$user_profile->about_me,
                "email_verified"=>(int)$user_profile->email_verified,
                "mobile_verified"=>(int)$user_profile->mobile_verified,
                "profile_completed"=>(int)$user_profile->profile_completed,
                "created_at"=>date('Y-m-d',strtotime($user_profile->created_at)),
                "updated_at"=>date('Y-m-d',strtotime($user_profile->updated_at)),
                "user_title"=>$title,
                "user_intern_infos"=>$intern_info,
                "user_qualification"=>$user_qualification,
                "current_work_place"=>$current_work_place,
                "user_website"=>$user_website,
                "user_mobile"=>$user_mobile,
                "field_of_interest"=>$field_of_interest
              );  
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS'],'data'=>$data]);
            }else{
            return response()->json(['status'=>FALSE,'msg'=>config('constants')['INVALID_USER']],400);
            }
        }
 
 
 
    public function updateAbout(Request $request)
    {
        $res = $this->validation($request,'user_id,title,first_name,last_name,country_id,dob,about_me');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        $title_name=Title::where('id',$request->title)->first();
        $request->merge([
            'title_name'=>$title_name->title,
            'full_name' => $title_name->title.' '.$request->first_name.' '.$request->last_name,
            'dob'=>date('Y-m-d',strtotime($request->dob))
        ]);
        
        $data=User::where('id',$request->user_id)->update($request->except(['user_id']));
        if($data){
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]);   
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
    }
    public function updateContact(Request $request)
    {
        $res = $this->validation($request,'user_id');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        /////delete old data/////
        User_contact_detail::where('user_id',$request->user_id)->delete();

        $mobile=json_decode($request->mobile);
        $website=json_decode($request->website);
        $mobile_array=array();
        $website_array=array();
                        foreach($mobile as $raw)
                        {
                        $mobile_array[]=array('user_id'=>$request->user_id,
                                        'meta_type'=>'Phone',
                                        'code'=>$raw->code,
                                        'phone_number'=>$raw->mobile
                                                            );
                        }
                        foreach($website as $raw1)
                        {
                        $website_array[]=array('user_id'=>$request->user_id,
                                        'meta_type'=>'Website',
                                        'website'=>$raw1->website
                                                            );
                        }

        $data=User_contact_detail::insert($mobile_array);
        User_contact_detail::insert($website_array);
        if($data){
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]);   
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
    }
    public function addUpdateQulification(Request $request)
    {
        $res = $this->validation($request,'user_id,qualification_id,speciality_id,country_id,registration_number,certificate,default_qualification');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }

        if($request->id>0)
        $data=User_qualification::where('id',$request->id)->update($request->except(['user_id']));
        else
        $data=User_qualification::insert($request->all());

        if($data){
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]);   
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
    }
    public function addUpdateInterninfo(Request $request)
    {
        $res = $this->validation($request,'user_id,university_id,university_name,country_id');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        if($request->id>0)
        $data=User_intern_info::where('id',$request->id)->update($request->except(['user_id']));
        else
        $data=User_intern_info::insert($request->all());

        if($data){
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]);   
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);

    }
    public function addUpdateWorkplace(Request $request)
    {
        $res = $this->validation($request,'user_id,position,current_work_place_name,country_id');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        if($request->id>0)
        $data=User_workspace::where('id',$request->id)->update($request->except(['user_id']));
        else
        $data=User_workspace::insert($request->all());

        if($data){
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]);   
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);

    }
    public function updateIntrest(Request $request)
    {
        $res = $this->validation($request,'user_id');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        
        /////delete old ///////
        Field_of_interest::where('user_id',$request->user_id)->delete();
        $field_of_interest=json_decode($request->field);
        foreach($field_of_interest as $raw2)
                        {
                        $field_of_interest_array[]=array('user_id'=>$request->user_id,
                                        'field_id'=>$raw2->field_id
                                                            );
                        }
        $data=Field_of_interest::insert($field_of_interest_array);

        if($data){
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]);   
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
    }
    public function updateProfile(Request $request){
                
        $res = $this->validation($request,'id');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        $qualification=json_decode($request->qualification);
        $current_place_of_work=json_decode($request->current_place_of_work);
        $certificate_and_registration=json_decode($request->certificate_and_registration);
        $student_intern_info=json_decode($request->student_intern_info);
        $contact_detail=json_decode($request->contact_detail);
        
        foreach($qualification as $raw)
        {
                $qualification_array[]=array('user_id'=>$request->id,
                            'level_id'=>$raw->level_id,
                            'speciality_id'=>$raw->speciality_id,
                            'qualification_id'=>$raw->qualification_id,
                            'institution_id'=>$raw->institution_id,
                            );
        }
        foreach($current_place_of_work as $raw_work)
        {
                $current_place_of_work_array[]=array('user_id'=>$request->id,
                            'current_work_place_name'=>$raw_work->current_work_place_name,
                            'country_id'=>$raw_work->country_id,
                            'state'=>$raw_work->state,
                            'zip_code'=>$raw_work->zip_code,
                            );
        }
        
        foreach($certificate_and_registration as $raw_certificate)
        {
                $certificate_and_registration_array[]=array('user_id'=>$request->id,
                            'country_id'=>$raw_certificate->country_id,
                            'registration_number'=>$raw_certificate->registration_number,
                            'certificate'=>$raw_certificate->certificate,
                            );
        }
        foreach($student_intern_info as $raw_intern)
        {
                $student_intern_info_array[]=array('user_id'=>$request->id,
                            'university_id'=>$raw_intern->university_id,
                            'intern_id'=>$raw_intern->intern_id,
                            );
        }
        
        foreach($contact_detail[0]->phone_array as $raw)
            {
                $phone_array[]=array('user_id'=>$request->id,
                            'meta_type'=>'Phone',
                            'code'=>$raw->code,
                            'phone_number'=>$raw->phone_number,
                            );
            }
        foreach($contact_detail[0]->email_array as $raw)
            {
                $email_array[]=array('user_id'=>$request->id,
                            'meta_type'=>'Email',
                            'email_id'=>$raw->email_id,
                                    );
            }
        
        User_contact_detail::insert($phone_array);
        User_contact_detail::insert($email_array);
        User_qualification::insert($qualification_array);
        User_intern_info::insert($student_intern_info_array);
        User_workspace::insert($current_place_of_work_array);
        User_certificate_and_registration::insert($certificate_and_registration_array);
        
        $data=array('profile_image'=>$request->profile_image,
                    'company_registration_number'=>$contact_detail[0]->company_registration_number
                );
        $data=User::where('id',$request->id)->update($data);
        if($data){
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['PROFILE_UPDATE']]);
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
    }
/******************Complete & update Profile end****************** */

/******************Basi APIS****************** */   
    public function privacyPolicy(){
        $pp=DB::table('privacy_policys')->get();
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['NO_ERROR'],'content'=>$pp[0]->text],'200');
    }
    public function terms(){
        $pp=DB::table('privacy_policys')->get();
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['NO_ERROR'],'content'=>$pp[0]->text],'200');
    }
    public function checkExist(Request $request){
        if(User::where('email',$request->email)->count()){
           return response()->json(['status'=>TRUE,'msg'=>config('constants')['EMAIL_ALREADY_EXIST']]);
        }else{
           return response()->json(['status'=>FALSE,'msg'=>config('constants')['EMAIL_AVILABLE']],400);
        }
    }

    public function checkEmailVerify($id)
    {
        if(!$id){
            return response()->json(['status'=>FALSE,'msg'=>'ID is Missing'],400);
         }
         
        $user=User::where('id',$id)->first();
        if($user){
            if($user->email_verified==0)
            return response()->json(['status'=>FALSE,'msg'=>config('constants')['EMAIL_NOT_VERIFIED']],400);
            else
            return response()->json(['status'=>TRUE,'msg'=>'Email Verified']);
         }
         else
         {
            return response()->json(['status'=>FALSE,'msg'=>'User_id Not Found'],400);   
         }
    }
     ////////// check company Exist API/////////////////////
     public function checkCompany(Request $request){
        if(Company::where('company_reg_no',$request->company_reg_no)->count()){
           return response()->json(['status'=>TRUE,'msg'=>config('constants')['VALID_COMPANY']]);
        }else{
           return response()->json(['status'=>FALSE,'msg'=>config('constants')['INVALID_COMPANY']],400);
        }
    }
/************user settings */
    public function changePassword(Request $request)
    {
        $res = $this->validation($request,'user_id,current_password,new_password,confirm_password');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        $user = User::where('id',$request->user_id)->first();
        if($user){
            if(password_verify($request->current_password,$user->password)){

                $request->merge(['password'=>Hash::make($request->new_password)]);
                $data=User::where('id',$request->user_id)->update($request->except(['user_id','current_password','new_password','confirm_password']));
                return response()->json(['status'=>TRUE,'msg'=>config('constants')['PASSWORD_UPDATED']]);   
            }
            else
            {
                return response()->json(['status'=>FALSE,'msg'=>config('constants')['CURRENT_PASSWORD_NOT_MATCH']],400);   
            }
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400);
    }
    public function addEmail(Request $request)
    {
        $res = $this->validation($request,'user_id,email');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        $user=User::where('email',$request->email)->where('id',$request->user_id)->first();
        
        if($user)
        {
            //change_email_verified
            $mailData['params']=['email'=>$user->email,'user_id'=>$user->id,'user_name'=>$user->full_name,'subject'=>'E-mail Confirmation','template'=>'confirm_verify_email'];
            $this->core->SendEmail($mailData);
            User::where('id',$request->user_id)->update(['change_email_verified'=>0]);
            return response()->json(['status'=>TRUE,'msg'=>'Confirmation Email Sent Successfully']);   
        }
        else
        {
            return response()->json(['status'=>FALSE,'msg'=>'Invalid User id & Email']);
        }
    }
    public function addEmailVerified(Request $request)
    {
        $res = $this->validation($request,'user_id,email');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        $user=User::where('email',$request->email)->where('id',$request->user_id)->where('change_email_verified',1)->first();
        if($user)
        {
            return response()->json(['status'=>TRUE,'msg'=>'Confirmation Email Verified']);   
        }
        else
        {
            return response()->json(['status'=>FALSE,'msg'=>'Confirmation Email Not Verified Yet']);
        }
    }
    public function changeEmailVerify(Request $request)
     {
         $email=base64_decode($request->email);
         if(User::where('email',$email)->count()){
             User::where('email',$email)->update($data=array('change_email_verified'=>1));
             $this->data['r']=1;
             return view('success',$this->data);
             
          }else{
            $this->data['r']=0;
            return view('success',$this->data);
             
          }
     }
    public function changeEmail(Request $request)
    {
        $res = $this->validation($request,'user_id,old_email,new_email');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        $user=User::where('id',$request->user_id)->where('change_email_verified',1)->update(['email'=>$request->new_email]);
        if($user)
        {
            return response()->json(['status'=>TRUE,'msg'=>'Success']);   
        }
        else
        {
            return response()->json(['status'=>FALSE,'msg'=>'Server Error']);
        }
    }
    public function addMobileNumber(Request $request)
    {
        $res = $this->validation($request,'user_id,code,mobile_number');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        ///////check Mobile no already exist or not//////////////////////
        if(User::where('mobile',$request->code.''.$request->mobile_number)->count()){
            return response()->json(['status'=>FALSE,'msg'=>config('constants')['MOBILE_ALREADY_TAKEN']],400);
        }
        /////////////////////////////////////////////////////////////////
        $otp=rand ( 10000 , 99999 );
        $data=User::where('id',$request->user_id)->update(['otp'=>$otp]);
        if($data){
            ///todo///
            //send sms//
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['OTP_SENT'],'data'=>$otp]);   
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);

    }
    public function mobileOTPVerified(Request $request)
    {
        $res = $this->validation($request,'user_id','otp','code','mobile_number');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        
        $request->merge(['mobile'=>$request->code.''.$request->mobile_number,'mobile_verified'=>1]);
        $data=User::where('id',$request->user_id)->where('otp',$request->otp)->update($request->except(['user_id','otp','code','mobile_number']));
        if($data){
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['OTP_VERIFIED_SUCCESS']]);   
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['INVALID_OTP']],400);
    }
    public function updateCountry(Request $request)
    {
        $res = $this->validation($request,'user_id,country_id');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        $user=User::where('id',$request->user_id)->update(['country_id'=>$request->country_id]);
        if($user)
        {
            return response()->json(['status'=>TRUE,'msg'=>'Success']);   
        }
        else
        {
            return response()->json(['status'=>FALSE,'msg'=>'Server Error']);
        }
    }
    public function discoverability(Request $request)
    {
        $res = $this->validation($request,'user_id');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        $user=User::select('find_by_email','find_by_mobile')->where('id',$request->user_id)->first();
        if($user)
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1'],'data'=>$user]); 
        else
        return response()->json(['status'=>FALSE,'msg'=>'Invalid User id']);     

    }
    public function updateDiscoverability(Request $request)
    {
        $res = $this->validation($request,'user_id,field,value');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }    
        if($request->field!='find_by_email' && $request->field!='find_by_mobile')
        return response()->json(['status'=>FALSE,'msg'=>'invalid field'],400);
        
        if($request->value!=0 && $request->value!=1)
        return response()->json(['status'=>FALSE,'msg'=>'invalid value'],400);


        if(User::where('id',$request->user_id)->update([$request->field=>$request->value]))
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]); 
        else
        return response()->json(['status'=>FALSE,'msg'=>'Invalid User id']); 
    }

    public function profileBlocked($id)
    {

        $date=date('Y-m-d H:i:s');
        $muted_list=DB::select("SELECT users.id,users.email,users.profile_image,users.full_name,
        (profile_muted.user_id IS NOT NULL) as muted FROM profile_blocked 
        left join users on users.id=profile_blocked.block_user_id
        left join profile_muted on profile_muted.user_id=profile_blocked.user_id and profile_muted.mute_user_id=profile_blocked.block_user_id
        where profile_blocked.user_id=$id ");
        if(count($muted_list)>0)
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1'],'data'=>$muted_list],200,[],JSON_NUMERIC_CHECK);
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']]);
    
    }
    public function blockProfile(Request $request)
    {
        $res = $this->validation($request,'user_id,block_user_id');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }    
        if(!Profile_blocked::where('user_id',$request->user_id)->where('block_user_id',$request->block_user_id)->first())
        {
            Profile_blocked::insertGetId($request->all());
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]); 
        }
        else
        {
            return response()->json(['status'=>FALSE,'msg'=>config('constants')['ALREADY_BLOCK']]); 
        }
    } 
    public function unBlockProfile(Request $request)
    {
        $res = $this->validation($request,'user_id,block_user_id');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        if(Profile_blocked::where('user_id',$request->user_id)->where('block_user_id',$request->block_user_id)->delete())
        {
         return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]); 
        }
        else
        {
         return response()->json(['status'=>FALSE,'msg'=>'invalid user & block user id']); 
        }
    } 
    public function profileMutedList($id)
    {
        $date=date('Y-m-d H:i:s');
        $muted_list=DB::select("SELECT users.id,users.email,users.profile_image,users.full_name,
        (profile_blocked.block_user_id IS NOT NULL) as blocked FROM profile_muted 
        left join users on users.id=profile_muted.mute_user_id
        left join profile_blocked on profile_blocked.user_id=profile_muted.user_id and profile_blocked.block_user_id=profile_muted.mute_user_id
        where profile_muted.user_id=$id and profile_muted.expire>='$date'");
        if(count($muted_list)>0)
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1'],'data'=>$muted_list],200,[],JSON_NUMERIC_CHECK);
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']]);

    }
    public function profileAddMute(Request $request)
    {
        $res = $this->validation($request,'user_id,mute_user_id,duration');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        
        $current_date=Date('Y-m-d H:i:s');
        if($request->duration==0)
        $expire='';
        else if($request->duration==1)
        $expire = date("Y-m-d H:i:s", strtotime('+24 hours'));
        else if($request->duration==2)
        $expire =date('Y-m-d H:i:s', strtotime($current_date. ' + 7 days'));
        else if($request->duration==4)
        $expire =date('Y-m-d H:i:s', strtotime($current_date. ' + 30 days'));
        
        if($expire=='')
        $expire_type=1;
        else
        $expire_type=0;
       
        profile_muted::updateOrCreate(['user_id'=>$request->user_id,'mute_user_id'=>$request->mute_user_id],
        ['user_id'=>$request->user_id,'mute_user_id'=>$request->mute_user_id,'expire'=>$expire,'home'=>$request->home,
        'notification'=>$request->notification,'any'=>$request->any,'from_follow'=>$request->from_follow,'expire_type'=>$expire_type]);
        
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]);
    }
    public function profileUnMute(Request $request)
    {
        $res = $this->validation($request,'user_id,mute_user_id');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }   
        if(profile_muted::where('user_id',$request->user_id)->where('mute_user_id',$request->mute_user_id)->delete())
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]);
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']]);
    }
    public function profileGroupList($id)
    {
        $group_list=DB::select("SELECT groups.id,groups.title,groups.image,(select count(id) from group_members where group_id=groups.id) as total_members
        FROM group_members 
        left join groups on groups.id=group_members.group_id
        where group_members.user_id=$id and group_members.role='Members' and group_members.status='Joined'");
        if(count($group_list)>0)
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1'],'data'=>$group_list],200,[],JSON_NUMERIC_CHECK);
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']]);
    }
    public function profileGroupMute(Request $request)
    {
        $res = $this->validation($request,'user_id,group_id,duration');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        
        $current_date=Date('Y-m-d H:i:s');
        if($request->duration==0)
        $expire='';
        else if($request->duration==1)
        $expire = date("Y-m-d H:i:s", strtotime('+24 hours'));
        else if($request->duration==2)
        $expire =date('Y-m-d H:i:s', strtotime($current_date. ' + 7 days'));
        else if($request->duration==4)
        $expire =date('Y-m-d H:i:s', strtotime($current_date. ' + 30 days'));
        
        if($expire=='')
        $expire_type=1;
        else
        $expire_type=0;
       
        Profile_muted_group::updateOrCreate(['user_id'=>$request->user_id,'group_id'=>$request->group_id],
        ['user_id'=>$request->user_id,'group_id'=>$request->group_id,'expire'=>$expire,'home'=>$request->home,
        'notification'=>$request->notification,'any'=>$request->any,'from_follow'=>$request->from_follow,'expire_type'=>$expire_type]);
        
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]);
    }
    public function profileMutedGroupList($id)
    {
        $date=date('Y-m-d H:i:s');
        $group_list=DB::select("SELECT groups.id,groups.title,groups.image,(select count(id) from group_members where group_id=groups.id) as total_members
        ,(profile_blocked_group.group_id IS NOT NULL) as blocked FROM profile_muted_group 
        left join groups on groups.id=profile_muted_group.group_id
        left join profile_blocked_group on profile_blocked_group.user_id=profile_muted_group.user_id and profile_blocked_group.group_id=profile_muted_group.group_id
        where profile_muted_group.user_id=$id and profile_muted_group.expire>='$date' ");
        if(count($group_list)>0)
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1'],'data'=>$group_list],200,[],JSON_NUMERIC_CHECK);
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']]);
    }
    public function profileGroupUnMute(Request $request)
    {
        $res = $this->validation($request,'user_id,group_id');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }   
        if(Profile_muted_group::where('user_id',$request->user_id)->where('group_id',$request->group_id)->delete())
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]);
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']]);
    }
    public function profileGroupBlock(Request $request)
    {
        $res = $this->validation($request,'user_id,group_id');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }    
        if(!Profile_blocked_group::where('user_id',$request->user_id)->where('group_id',$request->group_id)->first())
        {
            Profile_blocked_group::insertGetId($request->all());
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]); 
        }
        else
        {
            return response()->json(['status'=>FALSE,'msg'=>config('constants')['GROUP_ALREADY_BLOCK']]); 
        }
        
    }
    public function profileGroupUnBlock(Request $request)
    {
        $res = $this->validation($request,'user_id,group_id');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        if(Profile_blocked_group::where('user_id',$request->user_id)->where('group_id',$request->group_id)->delete())
        {
         return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]); 
        }
        else
        {
         return response()->json(['status'=>FALSE,'msg'=>'invalid user & Group id']); 
        }
    } 
    public function profileBlockedGroupList($id)
    {
        $date=date('Y-m-d H:i:s');
        $group_list=DB::select("SELECT groups.id,groups.title,groups.image,(select count(id) from group_members where group_id=groups.id) as total_members
        ,(profile_muted_group.group_id IS NOT NULL) as muted FROM profile_blocked_group 
        left join groups on groups.id=profile_blocked_group.group_id
        left join profile_muted_group on profile_blocked_group.user_id=profile_muted_group.user_id and profile_blocked_group.group_id=profile_muted_group.group_id
        where profile_blocked_group.user_id=$id");
        if(count($group_list)>0)
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1'],'data'=>$group_list],200,[],JSON_NUMERIC_CHECK);
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']]);
    }
    public function photoTagging($id)
    {
        $data=User::select('photo_tagging')->where('id',$id)->first();
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1'],'data'=>$data->photo_tagging],200,[],JSON_NUMERIC_CHECK);
    }
    public function updatePhotoTagging(Request $request)
    {
        $res = $this->validation($request,'user_id,photo_tagging');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        if($request->photo_tagging!=0 && $request->photo_tagging!=1)
        {
            return response()->json(['status'=>FALSE,'msg'=>'invalid photo_tagging']); 
        }
        if(user::where('id',$request->user_id)->update($request->except(['user_id'])))
        {
         return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]); 
        }
        else
        {
         return response()->json(['status'=>FALSE,'msg'=>'invalid user id']); 
        }
    } 
    public function forgotPassword(Request $request)
    {
        $res = $this->validation($request,'email');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        $user = User::where('mobile',$request->email)->orWhere('email',$request->email)->first();
        if($user){
        $request->merge(['otp'=>rand ( 10000 , 99999 )]);
        $data=User::where('mobile',$request->email)->orWhere('email',$request->email)->update($request->except(['email']));
        if($data){
            $mailData['params']=['email'=>$user->email,'user_id'=>$user->id,'user_name'=>$user->full_name,'otp'=>$request->otp,'subject'=>'Forgot Password Mail','template'=>'forgot_password'];
            $this->core->SendEmail($mailData);
           
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1'],'data'=>$user->id]);   
        }
       }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['EMAIL_NOT_EXIST']],400);
    }
    public function forgotPasswordOTPVerified(Request $request)
    {
        $res = $this->validation($request,'user_id','otp');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        
        $data=User::where('id',$request->user_id)->first();
        if($data){
            if($data->otp==$request->otp)
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['OTP_VERIFIED_SUCCESS'],'data'=>$data->id]);  
            else
            return response()->json(['status'=>FALSE,'msg'=>config('constants')['INVALID_OTP']],400);
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['INVALID_OTP']],400);
    }
    public function forgotPasswordChangePassword(Request $request)
    {
        $res = $this->validation($request,'user_id,new_password,confirm_password');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        $user = User::where('id',$request->user_id)->first();
        if(count($user) > 0){
                $request->merge(['password'=>Hash::make($request->new_password)]);
                $data=User::where('id',$request->user_id)->update($request->except(['user_id','new_password','confirm_password']));
                return response()->json(['status'=>TRUE,'msg'=>config('constants')['PASSWORD_UPDATED']]);   
                   }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400);
    }

/******************************** */
     
     ////////// resend Email Verify Link API/////////////////////
     public function resendEmailVerifyLink(Request $request)
     {
         $res = $this->validation($request,'email');
         if($res){
         return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
         }
         $user = User::where('email',$request->email);
         if($user->count()>0){
            $uData = $user->first();
            $mailData['params']=['email'=>$request->email,'user_id'=>$uData->id,'user_name'=>$uData->full_name,'subject'=>'E-mail Confirmation','template'=>'verify_email'];
            $this->core->SendEmail($mailData);
             return response()->json(['status'=>TRUE,'msg'=>config('constants')['VERIFY_LINK_SENT']]);
          }else{
             return response()->json(['status'=>FALSE,'msg'=>config('constants')['EMAIL_NOT_EXIST']],400);
          }
     }
      ////////// Email Verify API/////////////////////
     public function EmailVerify(Request $request)
     {
         $email=base64_decode($request->email);
         if(User::where('email',$email)->count()){
             User::where('email',$email)->update($data=array('email_verified'=>1));
             $this->data['r']=1;
             return view('success',$this->data);
             
          }else{
            $this->data['r']=0;
            return view('success',$this->data);
             
          }
     }
     
    ////////// update Profile Pic API/////////////////////
    public function updateProfilePic(Request $request)
    {
        //dd(date('Y-m-d H:i:s'));
        $res = $this->validation($request,'user_id,profile_image');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        $data=User::where('id',$request->user_id)->update($request->except(['user_id']));
        if($data){
            ////// add profile pic add Post ///////
            $user_post_array=array(
                'user_id'=>$request->user_id,
                'post_type'=>'profile_pic',
                'type'=>'media',
                'album_id'=>1,
            );
            $post_id=User_post::insertGetId($user_post_array);

            //////// update user media //////
            $user_media_array=array(
                'user_id'=>$request->user_id,
                'type'=>'profile_pic_update',
                'url'=>$request->profile_image,
                'user_post_id'=>$post_id,
                'album_id' =>1
                
            );
            User_media::insert($user_media_array);

            $join_user=DB::select("SELECT GROUP_CONCAT(users.id) as users_id,GROUP_CONCAT(users.device_token) as device_token  
            FROM `user_friends`
            LEFT JOIN users on users.id=`user_friends`.friend_id 
            where user_friends.user_id='$request->user_id' and user_friends.status='Accepted' ");
           //dd($join_user[0]->title_of_event);
           
           if($join_user){
            /***********Add Notification**********/   
            $postUserFullName=User::select('full_name')->where('id',$request->user_id)->first(); 
            $usersId=explode(',',$join_user[0]->users_id);
                foreach($usersId as $raw){
                $notification[]=array(
                'user_id'=>$raw,
                'type'=>'Post',
                'post_id'=>$post_id,
                'msg'=>@$postUserFullName->full_name.' change his profile pic'
            );
            }
            Notification::insert($notification);

            /***********Send Push Notification**********/    
            $user=explode(',',$join_user[0]->device_token);
            
            $msg=array('title'=>'profile pic changed','body'=>@$postUserFullName->full_name.' change his profile pic','sound'=>'default');
            $data=array('url'=>'','type'=>'change_profile','post_type'=>'Post','id'=>$post_id);
            $fields=array('registration_ids'=>$user,'data'=>$data,'notification'=>$msg,'priority'=>'high','content_available'=>true);
            $this->send_messages_android($fields);

           }
            //dd($fields);
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['PROFILE_PIC_UPDATE']]);
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
    }
    
    
    public function removeProfilePic($id)
    {
        if(!$id){ return response()->json(['status'=>FALSE,'msg'=>'user_id is missing'],400); }
        $data=User::where('id',$id)->update($data=array('profile_image'=>''));
        if($data){
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['PROFILE_PIC_REMOVE']]);
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
        
    }

/****************User Post */
    public function addPost(Request $request)
    {
       
        $res = $this->validation($request,'user_id,privacy');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        if(isset($request->album_id) && $request->album_id>0)
        $album_id=$request->album_id;
        else
        $album_id=3;
        
        $tags=json_decode($request->tags);

        if($request->type=='media')
        {
            if(!$request->media)
            return response()->json(['status'=>FALSE,'msg'=>'Media is missing'],400);
        }
        else
        {
            if(!$request->text_message_typed)
            return response()->json(['status'=>FALSE,'msg'=>'Text Message Typed is missing'],400);
        }
        if(isset($request->post_type))
        $post_type=$request->post_type;
        else
        $post_type='Post';

        $request->merge(['album_id' =>$album_id,'post_type'=>$post_type]);
        $post_id = User_post::insertGetId($request->except(['media','tags']));
        

            if($tags)
            {
                foreach($tags as $raw)
                {
                    $list[]=array(
                        'post_id'=>$post_id,
                        'friend_id'=>$raw->friend_id
                    );
                ///notification
                $notification[]=array(
                    'user_id'=>$raw->friend_id,
                    'type'=>'Post',
                    'post_id'=>$post_id,
                    'msg'=>@$user_info->full_name.' Tag you in a post'
                );
                $user_info=User::select('full_name')->where('id',$request->user_id)->first();
                $friend_info=User::select('device_token')->where('id',$raw->friend_id)->first();
                if($friend_info->device_token){
                $msg=array('title'=>'Tag you in post','body'=>@$user_info->full_name.' Tag you in a post','sound'=>'default');
                $data=array('url'=>'','type'=>'post_tag','post_type'=>'Post','id'=>$post_id);
                $fields=array('to'=>$friend_info->device_token,'data'=>$data,'notification'=>$msg,'priority'=>'high','content_available'=>true);
                $this->send_messages_android($fields);
                }
                }
                Notification::insert($notification);
                Post_tag::insert($list);
                
            
            } 
            else
            {
                if($request->post_type=='Well')
                { 
                    ///notification
                $notification[]=array(
                    'user_id'=>$request->friend_id,
                    'type'=>'Post',
                    'post_id'=>$post_id,
                    'msg'=>@$user_info->full_name.' made a post on your timeline'
                );
                $user_info=User::select('full_name')->where('id',$request->user_id)->first();
                $friend_info=User::select('device_token')->where('id',$request->friend_id)->first();
                if($friend_info->device_token){
                $msg=array('title'=>'post on your timeline','body'=>@$user_info->full_name.' made a post on your timeline','sound'=>'default');
                $data=array('url'=>'','type'=>'Well_post','post_type'=>'Post','id'=>$post_id);
                $fields=array('to'=>$friend_info->device_token,'data'=>$data,'notification'=>$msg,'priority'=>'high','content_available'=>true);
                $this->send_messages_android($fields);
                }
                Notification::insert($notification);
                }
            }
               
           //////// add post media //////
            $media=json_decode($request->media);
            if($media){
            foreach($media as $raw)
            {
                $media_array[]=array(
                    'user_id'=>$request->user_id,
                    'type'=>$raw->type,
                    'url'=>$raw->media,
                    'user_post_id'=>$post_id,
                    'album_id' =>$album_id
                                   );
            }
            User_media::insert($media_array);
            }
               
             if($post_id){
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['POST_ADD_SUCCESS']]); 
            }
            return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);

    }
    
    public function profileTimeline(Request $request)
    {
        $res = $this->validation($request,'user_id,login_user_id');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        //DB::enableQueryLog();
        $user=User::where('id',$request->user_id)->with('address:id,name','user_intern_infos','field_of_interest','user_qualification','current_work_place')->first();
        //dd($user);
        if($user){
            if($request->user_id==$request->login_user_id)
            {
                $is_friend=1;
                $is_follow=1;
            }
            else
            {
                $friend=User_friend::select('id')->where('user_id',$request->user_id)->where('friend_id',$request->login_user_id)->where('status','Accepted')->first();
                if($friend)
                $is_friend=1;
                else
                $is_friend=0;
                 
                $follow=Follows::select('id')->where('user_id',$request->user_id)->where('follower_id',$request->login_user_id)->first();
                if($follow)
                $is_follow=1;
                else
                $is_follow=0;
            }
            

        $sub_speciality=array();
        $university_name='';
              $speciality_title='';
              $current_work_place_name='';
              foreach($user->user_intern_infos as $raw_intern)
              {
                $university_name=$raw_intern->university_name;
              }
              foreach($user->user_qualification  as $raw_qualification)
              {
                if((int)$raw_qualification->default_qualification==1){  
                $speciality_title=$raw_qualification->speciality[0]->title;
                }
              }
              
              foreach($user->field_of_interest as $r_field_of_interest)
              {
                  
                  $sub_speciality[]=array(
                    "id"=>$r_field_of_interest->field->id,
                    "title"=>$r_field_of_interest->field->title,
                    "icon"=>$r_field_of_interest->field->icon,
                    
                  );
              }     
           
        $workplace='';
        if($user->current_work_place)
        {   foreach($user->current_work_place as $raw){
            if($raw->to_date=='Present')
            {$workplace=$raw->current_work_place_name;}
        }
        }
        if($workplace=='')
        {
            foreach($user->current_work_place as $raw){
                $workplace=$raw->current_work_place_name;
            }    
        }
        $speciality='';
        if($user->qualification_category==1)
        $speciality_name=$university_name;
        else if($user->qualification_category==2)
        $speciality_name=$speciality_title;
        else if($user->qualification_category==3)
        $speciality_name=$workplace;
        else
        $speciality_name='';
       
        $user_array=array(
            "user_id"=>$user->id,
            "qualification_category"=> $user->qualification_category,
            "qualification_title"=> $speciality_name,
            "field_of_interest"=>$sub_speciality,
            "cover_image"=>$user->cover_image,
            "profile_image"=>$user->profile_image,
            "full_name"=>$user->full_name,
            "email"=>$user->email,
            "dob"=>$user->dob,
            "country"=>$user->address,
            "about_me"=>$user->about_me,
            "total_number_of_followers"=>Follows::where('user_id',$request->user_id)->count(),
            "total_number_of_posts"=>User_post::where('user_id',$request->user_id)->count(),
            "total_number_of_follows"=>Follows::where('follower_id',$request->user_id)->count(),
            "is_friend"=>$is_friend,
            "is_follow"=>$is_follow
             );
            return response()->json(['status'=>TRUE,"msg"=>config('constants')['SUCCESS'],'data'=>$user_array],200,[],JSON_NUMERIC_CHECK);
            }
            return response()->json(['status'=>FALSE,"msg"=>config('constants')['INVALID_USER']],400); 
        
    }
    public function userPost(Request $request)
    {
        $res = $this->validation($request,'user_id,login_user_id');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        $user_array=array();
        $user_profile=User::where('id',$request->user_id)->first(['profile_image','full_name']);
        if($request->user_id==$request->login_user_id)
        $user_id=$request->user_id;
        else
        $user_id=$request->login_user_id;
        //////user posted post///////////////////////
        //DB::enableQueryLog();
        $user_post = User_post::where('user_id',$request->user_id)->withCount('like_count','comment_count','share_count')->with(['is_like'=> function ($query) use ($user_id) {
            $query->where('user_id', $user_id);
        },'post_media'])->orderBy('created_at', 'desc')->paginate(5, ['*'], '', $request->page);
        //dd(DB::getQueryLog());
       
        /////////////////////user taged post////////////////////////
        $user_tags=Post_tag::where('friend_id',$request->user_id)->withCount('like_count','comment_count','share_count')->with(['post_info','is_like'=> function ($query) use ($user_id) {
            $query->where('user_id', $user_id);
        }])->orderBy('created_at', 'desc')->paginate(1, ['*'], '', $request->page);
        ////////////////////user wall post//////////////////////
        $wall_post= User_post::where('friend_id',$request->user_id)->withCount('like_count','comment_count','share_count')->with(['user_info','is_like'=> function ($query) use ($user_id) {
            $query->where('user_id', $user_id);
        },'post_media'])->orderBy('created_at', 'desc')->paginate(1, ['*'], '', $request->page);
        
        ////////////user friend shaired post/////////////////////////
        //get all user friends//
        $friend_list=User_friend::select('friend_id')->where('user_id',$request->user_id)->where('status','Accepted')->get();
        $friend_list=$friend_list->toArray();
        $friend_list=array_column($friend_list,'friend_id');
        if(count($friend_list)>0){ 
        $share=Post_meta::where('type','Share')->whereIn('user_id',[$request->user_id,$friend_list])->withCount('like_count','comment_count','share_count')->with(['post_info','is_liked'=> function ($query) use ($user_id) {
            $query->where('user_id', $user_id);
        }])->orderBy('created_at', 'desc')->paginate(1, ['*'], '', $request->page);
        }
        else
        {
            $share=Post_meta::where('type','Share')->whereIn('user_id',[$request->user_id])->withCount('like_count','comment_count','share_count')->with(['post_info','is_liked'=> function ($query) use ($user_id) {
                $query->where('user_id', $user_id);
            }])->orderBy('created_at', 'desc')->paginate(1, ['*'], '', $request->page);
        }
            //dd($share);
        if(count($user_post)>0) {
            foreach($user_post as $raw)
            { 
               
                if (count($raw->is_like)>0)
                $is_liked=1;
                else
                $is_liked=0;
    
                $media=array();
                if($raw->type=='media')
                {
                    foreach($raw->post_media as $media_raw)
                    {
                        $media[]=array('media_type'=>$media_raw->type,'media_url'=>$media_raw->url);  
                    }
                }
                
                
                $user_array[]=array(
                    "post_id"=>$raw->id,
                    "post_type"=>$raw->post_type,
                    "user_id"=>$raw->user_id,
                    "post_by"=>$user_profile->full_name,
                    "profile_image"=>$user_profile->profile_image,
                    "date_of_post"=>date('d-m-Y',strtotime($raw->created_at)),
                    "time_of_post"=>date('h:i:s A',strtotime($raw->created_at)),
                    "location_of_post"=>$raw->location,
                    "text_message"=>$raw->text_message_typed,
                    "media"=>$media,
                    "total_number_of_likes"=>$raw->like_count_count,
                    "total_number_of_comments"=>$raw->comment_count_count,
                    "total_number_of_share"=>$raw->share_count_count,
                    "is_liked"=>$is_liked
                    ); 
                
            }
        }
        if(count($share)>0)
        {
            foreach($share as $raw)
            { 
            if($raw->post_info){
                //dd($raw->post_info->post_media_details[0]->url);
            if (count($raw->is_liked)>0)
            $is_liked=1;
            else
            $is_liked=0;

            $media=array();
            if($raw->post_info->type=='media')
            {
                foreach($raw->post_info->post_media_details as $media_raw)
                {
                    
                    $media[]=array('media_type'=>$media_raw->type,'media_url'=>$media_raw->url);  
                }
            }
            
            $user_array[]=array(
                "post_id"=>$raw->post_id,
                "post_type"=>'share',
                "user_id"=>$raw->post_info->user_id,
                "post_by"=>$raw->post_info->user_info->full_name,
                "profile_image"=>$raw->post_info->user_info->profile_image,
                "date_of_post"=>date('d-m-Y',strtotime($raw->post_info->created_at)),
                "time_of_post"=>date('h:i:s A',strtotime($raw->post_info->created_at)),
                "location_of_post"=>$raw->post_info->location,
                "text_message"=>$raw->post_info->text_message_typed,
                "media"=>$media,
                "total_number_of_likes"=>$raw->like_count_count,
                "total_number_of_comments"=>$raw->comment_count_count,
                "total_number_of_share"=>$raw->share_count_count,
                "is_liked"=>$is_liked
                ); 
            }
            }

        }
        
        if(count($user_tags)>0)
        {
            foreach($user_tags as $raw)
            {   //dd($raw);
                if (count($raw->is_like)>0)
                $is_liked=1;
                else
                $is_liked=0;

            $media=array();
            if($raw->post_info->type=='media')
            {
                foreach($raw->post_info->post_media_details as $media_raw)
                {
                    $media[]=array('media_type'=>$media_raw->type,'media_url'=>$media_raw->url);  
                }
            }
            
            $user_array[]=array(
                "post_id"=>$raw->post_id,
                "post_type"=>'Tag',
                "user_id"=>$raw->post_info->user_id,
                "post_by"=>$raw->post_info->user_info->full_name,
                "profile_image"=>$raw->post_info->user_info->profile_image,
                "date_of_post"=>date('d-m-Y',strtotime($raw->post_info->created_at)),
                "time_of_post"=>date('h:i:s A',strtotime($raw->post_info->created_at)),
                "location_of_post"=>$raw->post_info->location,
                "text_message"=>$raw->post_info->text_message_typed,
                "media"=>$media,
                "total_number_of_likes"=>$raw->like_count_count,
                "total_number_of_comments"=>$raw->comment_count_count,
                "total_number_of_share"=>$raw->share_count_count,
                "is_liked"=>$is_liked
                ); 
            }
        }
       
        if(count($wall_post)>0)
        {
            foreach($wall_post as $raw)
            {   
                if (count($raw->is_like)>0)
                $is_liked=1;
                else
                $is_liked=0;

            $media=array();
            if($raw->type=='media')
            {
                foreach($raw->post_media as $media_raw)
                {
                    $media[]=array('media_type'=>$media_raw->type,'media_url'=>$media_raw->url);  
                }
            }
            
            $user_array[]=array(
                "post_id"=>$raw->id,
                "post_type"=>'Well',
                "user_id"=>$raw->user_id,
                "post_by"=>$raw->user_info->full_name,
                "profile_image"=>$raw->user_info->profile_image,
                "date_of_post"=>date('d-m-Y',strtotime($raw->created_at)),
                "time_of_post"=>date('h:i:s A',strtotime($raw->created_at)),
                "location_of_post"=>$raw->location,
                "text_message"=>$raw->text_message_typed,
                "media"=>$media,
                "total_number_of_likes"=>$raw->like_count_count,
                "total_number_of_comments"=>$raw->comment_count_count,
                "total_number_of_share"=>$raw->share_count_count,
                "is_liked"=>$is_liked
                ); 
            }
        }
 
        if(count($user_array)>0)  
        return response()->json(['status'=>TRUE,"msg"=>config('constants')['SUCCESS'],'data'=>$user_array],200, [], JSON_NUMERIC_CHECK);
        else
        return response()->json(['status'=>TRUE,"msg"=>config('constants')['NO_POST'],'data'=>[]],400);  
         
    }
    public function userFeeds(Request $request)
    {
        $is_liked=0;
        $user_post=array();
        $share=array();
        $wall_post=array();
        $group_post=array();
        $event_post=array();
        $page_post=array();
        $user_array=array();
        $user_id=$request->user_id;

        //get all user friends//
        $friend_list=User_friend::select('friend_id')->where('user_id',$request->user_id)->where('status','Accepted')->get();
        $friend_list=$friend_list->toArray();
        $friend_list=array_column($friend_list,'friend_id');
        //->withCount('like_count','comment_count','share_count')
        if(count($friend_list)>0){
        $share=Post_meta::where('type','Share')
        ->whereIn('user_id',[$request->user_id,$friend_list])
        ->with(['post_info','is_liked'=> function ($query) use ($user_id) 
        {
            $query->where('user_id', $user_id);
        }])->orderBy('created_at', 'desc')->paginate(5, ['*'], '', $request->page);

        $user_post = User_post::where('post_type','!=','Well')->whereIn('user_id',[$friend_list])->withCount('like_count','comment_count','share_count')->with(['is_like'=> function ($query) use ($user_id) {
            $query->where('user_id', $user_id);
        },'post_media','user_info'])->orderBy('created_at', 'desc')->paginate(5, ['*'], '', $request->page);
        
        }
        else
        {
         $share=Post_meta::where('type','Share')
        ->whereIn('user_id',[$request->user_id])
        ->with(['post_info','is_liked'=> function ($query) use ($user_id) 
        {
            $query->where('user_id', $user_id);
        }])->orderBy('created_at', 'desc')->paginate(5, ['*'], '', $request->page);
       
        }
       // dd($share);
        
        $wall_post= User_post::where('post_type','Well')->where('user_id',$request->user_id)->withCount('like_count','comment_count','share_count')->with(['user_info_friend','is_like'=> function ($query) use ($user_id) {
            $query->where('user_id', $user_id);
        },'post_media'])->orderBy('created_at', 'desc')->paginate(5, ['*'], '', $request->page);
        
        //get user groups//
        $groups_list=GroupMember::select('group_id')->where('user_id',$request->user_id)->where('status','Joined')->get();
        $groups_list=$groups_list->toArray();
        $groups_list=array_column($groups_list,'group_id');
        //DB::enableQueryLog();
        if(count($groups_list)>0){
        $group_post= GroupPost::whereIn('group_id',[$groups_list])->withCount('like_count','comment_count','share_count')->with(['user_info','is_like'=> function ($query) use ($user_id) {
            $query->where('user_id', $user_id);
        },'group_post_media','group_info'])->orderBy('created_at', 'desc')->paginate(5, ['*'], '', $request->page);
         }
        //dd(DB::getQueryLog());
        //Group_meta

        ////events posts////////
        $events_list=Event_joined_user::select('event_id')->where('joined_user_id',$request->user_id)->where('response_type','1')->get();
        $events_list=$events_list->toArray();
        $events_list=array_column($events_list,'event_id');
        if(count($events_list)>0){
        $event_post=EventPost::whereIn('event_id',[$events_list])->withCount('like_count','comment_count','share_count')->with(['user_info','is_like'=> function ($query) use ($user_id) {
            $query->where('user_id', $user_id);
        },'event_post_media','event_info'])->orderBy('created_at', 'desc')->paginate(5, ['*'], '', $request->page);
        }
        
        ////Page posts////////
       
        if(count($friend_list)>0){
        $page_post=PagePost::whereIn('user_id',[$friend_list])->withCount('like_count','comment_count','share_count')->with(['user_info','is_like'=> function ($query) use ($user_id) {
            $query->where('user_id', $user_id);
        },'page_post_media','page_info'])->orderBy('created_at', 'desc')->paginate(5, ['*'], '', $request->page);
        }
        if(count($page_post)>0) 
        {
            foreach($page_post as $raw)
            { 
               
                if (count($raw->is_like)>0)
                $is_liked=1;
                else
                $is_liked=0;
    
                $media=array();
                if($raw->type=='media')
                {
                    foreach($raw->page_post_media as $media_raw)
                    {
                        $media[]=array('media_type'=>$media_raw->media_type,'media_url'=>$media_raw->page_media);  
                    }
                }
                
                
                $user_array[]=array(
                    "post_id"=>$raw->id,
                    "post_type"=>'Page_post',
                    "page_id"=>$raw->page_info->id,
                    "page_name"=>$raw->page_info->title,
                    "page_image"=>$raw->page_info->image,
                    "user_id"=>$raw->user_id,
                    "post_by"=>$raw->user_info->full_name,
                    "profile_image"=>$raw->user_info->profile_image,
                    "date_of_post"=>date('d-m-Y',strtotime($raw->created_at)),
                    "time_of_post"=>date('h:i:s A',strtotime($raw->created_at)),
                    "location_of_post"=>$raw->location,
                    "text_message"=>$raw->post_text,
                    "media"=>$media,
                    "total_number_of_likes"=>$raw->like_count_count,
                    "total_number_of_comments"=>$raw->comment_count_count,
                    "total_number_of_share"=>$raw->share_count_count,
                    "is_liked"=>$is_liked
                    ); 
                
            }
        }
        if(count($event_post)>0) 
        {
            foreach($event_post as $raw)
            { 
               
                if (count($raw->is_like)>0)
                $is_liked=1;
    
                $media=array();
                if($raw->type=='media')
                {
                    foreach($raw->event_post_media as $media_raw)
                    {
                        $media[]=array('media_type'=>$media_raw->media_type,'media_url'=>$media_raw->event_media);  
                    }
                }
                
                
                $user_array[]=array(
                    "post_id"=>$raw->id,
                    "post_type"=>'Event_post',
                    "event_id"=>$raw->event_info->id,
                    "event_name"=>$raw->event_info->title_of_event,
                    "event_image"=>$raw->event_info->event_image,
                    "user_id"=>$raw->user_id,
                    "post_by"=>$raw->user_info->full_name,
                    "profile_image"=>$raw->user_info->profile_image,
                    "date_of_post"=>date('d-m-Y',strtotime($raw->created_at)),
                    "time_of_post"=>date('h:i:s A',strtotime($raw->created_at)),
                    "location_of_post"=>$raw->location,
                    "text_message"=>$raw->post_text,
                    "media"=>$media,
                    "total_number_of_likes"=>$raw->like_count_count,
                    "total_number_of_comments"=>$raw->comment_count_count,
                    "total_number_of_share"=>$raw->share_count_count,
                    "is_liked"=>$is_liked
                    ); 
                
            }
        }
        
        if(count($group_post)>0) 
        {
            foreach($group_post as $raw)
            { 
               
                if (count($raw->is_like)>0)
                $is_liked=1;
    
                $media=array();
                if($raw->type=='media')
                {
                    foreach($raw->group_post_media as $media_raw)
                    {
                        $media[]=array('media_type'=>$media_raw->media_type,'media_url'=>$media_raw->group_media);  
                    }
                }
                
                
                $user_array[]=array(
                    "post_id"=>$raw->id,
                    "post_type"=>'Group_post',
                    "group_id"=>$raw->group_info->id,
                    "group_name"=>$raw->group_info->title,
                    "group_image"=>$raw->group_info->image,
                    "user_id"=>$raw->user_id,
                    "post_by"=>$raw->user_info->full_name,
                    "profile_image"=>$raw->user_info->profile_image,
                    "date_of_post"=>date('d-m-Y',strtotime($raw->created_at)),
                    "time_of_post"=>date('h:i:s A',strtotime($raw->created_at)),
                    "location_of_post"=>$raw->location,
                    "text_message"=>$raw->post_text,
                    "media"=>$media,
                    "total_number_of_likes"=>$raw->like_count_count,
                    "total_number_of_comments"=>$raw->comment_count_count,
                    "total_number_of_share"=>$raw->share_count_count,
                    "is_liked"=>$is_liked
                    ); 
                
            }
        }
        if(count($wall_post)>0)
        {  
            foreach($wall_post as $raw)
            { 
            if (count($raw->is_like)>0)
            $is_liked=1;

            $media=array();
            if($raw->type=='media')
            {
                foreach($raw->post_media as $media_raw)
                {
                    $media[]=array('media_type'=>$media_raw->type,'media_url'=>$media_raw->url);  
                }
            }
            
            $user_array[]=array(
                "post_id"=>$raw->id,
                "post_type"=>'Well',
                "user_id"=>$raw->friend_id,
                "post_by"=>$raw->user_info_friend->full_name,
                "profile_image"=>$raw->user_info->profile_image,
                "date_of_post"=>date('d-m-Y',strtotime($raw->created_at)),
                "time_of_post"=>date('h:i:s A',strtotime($raw->created_at)),
                "location_of_post"=>$raw->location,
                "text_message"=>$raw->text_message_typed,
                "media"=>$media,
                "total_number_of_likes"=>$raw->like_count_count,
                "total_number_of_comments"=>$raw->comment_count_count,
                "total_number_of_share"=>$raw->share_count_count,
                "is_liked"=>$is_liked
                ); 
            }
        }
        if(count($share)>0)
        {
            foreach($share as $raw)
            {  
            if($raw->post_info){
            if (count($raw->is_liked)>0)
            $is_liked=1;

            $media=array();
            if($raw->post_info->type=='media')
            {
                foreach($raw->post_info->post_media_details as $media_raw)
                {
                    $media[]=array('media_type'=>$media_raw->type,'media_url'=>$media_raw->url);  
                }
            }
            if(Post_meta::where('post_id',$raw->post_id)->where('type','like')->first())
            $like_by_me=1;
            else
            $like_by_me=0;
            $user_array[]=array(
                "post_id"=>$raw->post_id,
                "post_type"=>'Share',
                "user_id"=>$raw->post_info->user_id,
                "post_by"=>$raw->post_info->user_info->full_name,
                "profile_image"=>$raw->post_info->user_info->profile_image,
                "date_of_post"=>date('d-m-Y',strtotime($raw->post_info->created_at)),
                "time_of_post"=>date('h:i:s A',strtotime($raw->post_info->created_at)),
                "location_of_post"=>$raw->post_info->location,
                "text_message"=>$raw->post_info->text_message_typed,
                "media"=>$media,
                "total_number_of_likes"=>count(Post_meta::where('post_id',$raw->post_id)->where('type','like')->get()),
                "total_number_of_comments"=>count(Post_meta::where('post_id',$raw->post_id)->where('type','Comment')->get()),
                "total_number_of_share"=>count(Post_meta::where('post_id',$raw->post_id)->where('type','Share')->get()),
                "is_liked"=>$like_by_me
                ); 
            }
            }
        }
        if(count($user_post)>0) 
        {
            foreach($user_post as $raw)
            { 
               
                if (count($raw->is_like)>0)
                $is_liked=1;
    
                $media=array();
                if($raw->type=='media')
                {
                    foreach($raw->post_media as $media_raw)
                    {
                        $media[]=array('media_type'=>$media_raw->type,'media_url'=>$media_raw->url);  
                    }
                }
                
                
                $user_array[]=array(
                    "post_id"=>$raw->id,
                    "post_type"=>$raw->post_type,
                    "user_id"=>$raw->user_id,
                    "post_by"=>$raw->user_info->full_name,
                    "profile_image"=>$raw->user_info->profile_image,
                    "date_of_post"=>date('d-m-Y',strtotime($raw->created_at)),
                    "time_of_post"=>date('h:i:s A',strtotime($raw->created_at)),
                    "location_of_post"=>$raw->location,
                    "text_message"=>$raw->text_message_typed,
                    "media"=>$media,
                    "total_number_of_likes"=>$raw->like_count_count,
                    "total_number_of_comments"=>$raw->comment_count_count,
                    "total_number_of_share"=>$raw->share_count_count,
                    "is_liked"=>$is_liked
                    ); 
                
            }
        }
        if(count($user_array)>0)  {
        shuffle($user_array);
        return response()->json(['status'=>TRUE,"msg"=>config('constants')['SUCCESS'],'data'=>$user_array],200, [], JSON_NUMERIC_CHECK);
        }else
        {return response()->json(['status'=>TRUE,"msg"=>config('constants')['NO_POST'],'data'=>[]],200);}
    }
    public function postAllLike(Request $request)
    {
        if($request->post_type=='Group_post')
        {
         return $this->groupPostAllLike($request);
        }
        else if($request->post_type=='Event_post')
        {
            return  $this->eventPostAllLike($request);
        }
        else if($request->post_type=='Page_post')
        {
            return  $this->pagePostAllLike($request);
        }
        else
        {
        $post=User_post::where('id',$request->post_id)->first();   
        if($post){
        $user_id=$post->user_id;

        $all_like=Post_meta::where('post_id',$request->post_id)->where('type','Like')->with(['like_user_info','is_friend' => function ($q) use($user_id) {
            $q->where('user_id','=',$user_id);
            }
            ])->get();
        //dd($all_like);
        if(count($all_like)>0)
        {
            foreach($all_like as $raw)
            {
                    if($raw->is_friend)
                    $is_friend=1;
                    else
                    $is_friend=0;
                    $data[]=array(
                    "user_id"=>$raw->like_user_info->id,
                    "full_name"=>$raw->like_user_info->full_name,
                    "profile_pic"=>$raw->like_user_info->profile_image,
                    "color"=>$raw->like_user_info->user_title,
                    "is_friend"=>$is_friend );
            }
            return response()->json(['status'=>TRUE,"msg"=>config('constants')['SUCCESS'],'data'=>$data]);
        }
        return response()->json(['status'=>FALSE,"msg"=>config('constants')['NO_LIKE_POST']],400);
        }
        else
        {
            return response()->json(['status'=>FALSE,"msg"=>'Invalid Post id'],400); 
        }
        }
    }
    public function postLikeDislike(Request $request)
    {

        //dd($request->all());
        $res = $this->validation($request,'loggedin_user_id,post_user_id,post_id,current_status');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        if($request->post_type=='Group_post')
        {
         return $this->groupPostLikeDislike($request);
        }
        else if($request->post_type=='Event_post')
        {
            return  $this->eventPostLikeDislike($request);
        }
        else if($request->post_type=='Page_post')
        {
            return  $this->pagePostLikeDislike($request);
        }
        else
        {
        ////////check post exist//////////
        if(User_post::where('id',$request->post_id)->first(['id'])){
        if($request->current_status==0)
        {
         $like=Post_meta::where('user_id',$request->loggedin_user_id)->where('post_id',$request->post_id)->where('type','Like')->first();
         if($like){ $like->delete(); }
        }
        else
        {
            
            $user_info=User::select('full_name')->where('id',$request->loggedin_user_id)->first();
            $friend_info=User::select('device_token')->where('id',$request->post_user_id)->first();
            Notification::insert(['user_id'=>$request->post_user_id,'type'=>'Post','post_id'=>$request->post_id,'msg'=>@$user_info->full_name.' Like Your Post']);
            if($friend_info->device_token){
                $msg=array('title'=>'New Like On Post','body'=>@$user_info->full_name.' Like Your Post','sound'=>'default');
                $data=array('url'=>'','type'=>'post_like','post_type'=>'Post','id'=>$request->post_id);
                $fields=array('to'=>$friend_info->device_token,'data'=>$data,'notification'=>$msg,'priority'=>'high','content_available'=>true);
                $this->send_messages_android($fields);
                }
         $like=Post_meta::updateOrCreate(['type'=>'Like','post_id'=>$request->post_id,'user_id'=>$request->loggedin_user_id],['type'=>'Like','post_user_id'=>$request->post_user_id,'user_id'=>$request->loggedin_user_id,'post_id'=>$request->post_id]);
        }
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]); 
        }
        else
        {
            return response()->json(['status'=>FALSE,'msg'=>'Invalid Post ID']);    
        }
        }
       
    }
    public function postComments(Request $request)
    {
        if($request->post_type=='Group_post')
        {
         return $this->groupPostComments($request);
        }
        else if($request->post_type=='Event_post')
        {
            return  $this->eventPostComments($request);
        }
        else if($request->post_type=='Page_post')
        {
            return  $this->pagePostComments($request);
        }
        else
        {
        $user_id=$request->user_id;
        $comments=Post_meta::where('type','Comment')->where('post_id',$request->post_id)->with(['is_like'=> function ($query) use ($user_id) {
            $query->where('user_id', $user_id);
        },'comment_like','comment_reply','comment_user_info'])->get();
        
        if(count($comments)>0)
        {
            foreach($comments as $raw)
            {       if($raw->is_like)
                    $is_like=1;
                    else
                    $is_like=0;
                    $data[]=array(
                    "user_id"=>$raw->like_user_info->id,
                    "profile_pic"=>$raw->like_user_info->profile_image,
                    "user_full_name"=>$raw->like_user_info->full_name,
                    "comment_id"=>$raw->id,
                    "comment_text"=>$raw->comment_text,
                    "is_like"=>$is_like,
                    "like_count"=>count($raw->comment_like),
                    'comment_reply'=>$raw->comment_reply
                );
            }
            return response()->json(['status'=>TRUE,"msg"=>config('constants')['SUCCESS'],'data'=>$data]);
        }
        return response()->json(['status'=>FALSE,"msg"=>config('constants')['NO_POST_COMMENTS']],400);
    }
    }
    public function postAddComment(Request $request)
    {
        $res = $this->validation($request,'loggedin_user_id,post_user_id,post_id,comment_text');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  

        if($request->post_type=='Group_post')
        {
         return $this->groupPostAddComment($request);
        }
        else if($request->post_type=='Event_post')
        {
            return  $this->eventPostAddComment($request);
        }
        else if($request->post_type=='Page_post')
        {
            return  $this->pagePostAddComment($request);
        }
        else
        {
        $request->merge(['user_id'=>$request->loggedin_user_id,'type'=>'Comment']);
        $comment=Post_meta::insertGetId($request->except(['loggedin_user_id','post_type']));
        if($comment)
        {
            $user_info=User::select('full_name')->where('id',$request->loggedin_user_id)->first();
            $friend_info=User::select('device_token')->where('id',$request->post_user_id)->first();
            Notification::insert(['user_id'=>$request->post_user_id,'type'=>'Post','post_id'=>$request->post_id,'msg'=>@$user_info->full_name.' Comment on Your Post']);
            if($friend_info->device_token){
                $msg=array('title'=>'New Comment On Post','body'=>@$user_info->full_name.' Comment on Your Post','sound'=>'default');
                $data=array('url'=>'','type'=>'post_comment','post_type'=>'Post','id'=>$request->post_id);
                $fields=array('to'=>$friend_info->device_token,'data'=>$data,'notification'=>$msg,'priority'=>'high','content_available'=>true);
                $this->send_messages_android($fields);
                }
                return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1'],'data'=>$comment]); 
        }
        
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400); 
    }
    }
    public function postEditComment(Request $request)
    {
        $res = $this->validation($request,'comment_id,comment_edited_text');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        if($request->post_type=='Group_post')
        {
         return $this->groupPostEditComment($request);
        }
        else if($request->post_type=='Event_post')
        {
            return  $this->eventPostEditComment($request);
        }
        else if($request->post_type=='Page_post')
        {
            return  $this->pagePostEditComment($request);
        }
        else
        {
        $request->merge(['comment_text'=>$request->comment_edited_text]);
        $comment=Post_meta::where('id',$request->comment_id)->update($request->except(['comment_edited_text','comment_id','post_type']));
        if($comment)
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]); 
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400); 
        }
    }
    public function postDeleteComment(Request $request)
    {
        $res = $this->validation($request,'comment_id,loggedin_user_id');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        if($request->post_type=='Group_post')
        {
         return $this->groupPostDeleteComment($request);
        }
        else if($request->post_type=='Event_post')
        {
            return  $this->eventPostDeleteComment($request);
        }
        else if($request->post_type=='Page_post')
        {
            return  $this->pagePostDeleteComment($request);
        }
        else
        {
        $comment=Post_meta::where('user_id',$request->loggedin_user_id)->where('id',$request->comment_id)->first();
        if($comment){ $comment->delete(); }
        if($comment)
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]); 
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400); 
        }
    }
    public function commentLikeDislike(Request $request)
    {
        $res = $this->validation($request,'user_id,comment_id');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        if($request->post_type=='Group_post')
        {
         return $this->groupCommentLikeDislike($request);
        }
        else if($request->post_type=='Event_post')
        {
            return  $this->eventCommentLikeDislike($request);
        }
        else if($request->post_type=='Page_post')
        {
            return  $this->pageCommentLikeDislike($request);
        }
        else
        { 
        if($request->current_status=='0')
        {
         $like=Comment_meta::where('user_id',$request->user_id)->where('comment_id',$request->comment_id)->where('type','Like')->first();
         if($like){ $like->delete(); }
        }
        else
        {
            $post_id=Post_meta::select('post_user_id','post_id')->where('id',$request->comment_id)->first();
            $user_info=User::select('full_name')->where('id',$request->user_id)->first();
            $friend_info=User::select('device_token')->where('id',$post_id->post_user_id)->first();
            Notification::insert(['user_id'=>$post_id->post_user_id,'comment_id'=>$request->comment_id,'type'=>'Post','post_id'=>@$post_id->post_id,'msg'=>@$user_info->full_name.' Like your Comment']);
            if($friend_info->device_token){
                $msg=array('title'=>'New Comment Like','body'=>@$user_info->full_name.' Like your Comment','sound'=>'default');
                $data=array('url'=>'','type'=>'post_comment_like','post_type'=>'Post','id'=>@$post_id->post_id);
                $fields=array('to'=>$friend_info->device_token,'data'=>$data,'notification'=>$msg,'priority'=>'high','content_available'=>true);
                $this->send_messages_android($fields);
                }
         $like=Comment_meta::updateOrCreate(['type'=>'Like','comment_id'=>$request->comment_id,'user_id'=>$request->user_id],['type'=>'Like','comment_id'=>$request->comment_id,'user_id'=>$request->user_id]);
        }
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]);
       }
    }
    public function commentReply(Request $request)
    {
        $res = $this->validation($request,'user_id,comment_id,text');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        if($request->post_type=='Group_post')
        {
         return $this->groupCommentReply($request);
        }
        else if($request->post_type=='Event_post')
        {
            return  $this->eventCommentReply($request);
        }
        else if($request->post_type=='Page_post')
        {
            return  $this->pageCommentReply($request);
        }
        else
        { 
            Comment_meta::insert(['type'=>'Reply','comment_id'=>$request->comment_id,'user_id'=>$request->user_id,'text'=>$request->text]);
            $post_id=Post_meta::select('post_user_id','post_id')->where('id',$request->comment_id)->first();
            $user_info=User::select('full_name')->where('id',$request->user_id)->first();
            $friend_info=User::select('device_token')->where('id',$post_id->post_user_id)->first();
            Notification::insert(['user_id'=>$post_id->post_user_id,'type'=>'Post','post_id'=>@$post_id->post_id, 'comment_id'=>$request->comment_id,'msg'=>@$user_info->full_name.' Reply on your Comment']);
            if($friend_info->device_token){
                $msg=array('title'=>'New Comment Reply','body'=>@$user_info->full_name.' Reply on your Comment','sound'=>'default');
                $data=array('url'=>'','type'=>'post_comment_reply','post_type'=>'Post','id'=>@$post_id->post_id,'comment_id'=>$request->comment_id,'name'=>@$user_info->full_name,'reply'=>$request->text);
                $fields=array('to'=>$friend_info->device_token,'data'=>$data,'notification'=>$msg,'priority'=>'high','content_available'=>true);
                $this->send_messages_android($fields);
                }
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]);
        } 
    }
    public function deletePost(Request $request)
    {
        $res = $this->validation($request,'post_id,user_id');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        if($request->post_type=='Group_post')
        {
         return $this->groupDeletePost($request);
        }
        else if($request->post_type=='Event_post')
        {
            return  $this->eventDeletePost($request);
        }
        else if($request->post_type=='Page_post')
        {
            return  $this->pageDeletePost($request);
        }
        else
        { 
        $post_details=User_post::where('user_id',$request->user_id)->where('id',$request->post_id)->first();
        if($post_details){ $post_details->delete(); }
        if($post_details)
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]); 
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400); 
        }
    }
    public function postDetails(Request $request)
    {
        $is_liked=0;
        $user_array=array();
        $user_id=$request->user_id;
        
        if($request->post_type=='Post' || $request->post_type=='Share' || $request->post_type=='Well' || $request->post_type=='cover_pic' || $request->post_type=='profile_pic')
        {
            $raw= User_post::where('id',$request->post_id)->withCount('like_count','comment_count','share_count')->with(['user_info','is_like'=> function ($query) use ($user_id) {
                $query->where('user_id', $user_id);
            },'post_media'])->first();  

            if (count($raw->is_like)>0)
                $is_liked=1;
    
                $media=array();
                if($raw->type=='media')
                {
                    foreach($raw->post_media as $media_raw)
                    {
                        $media[]=array('media_type'=>$media_raw->type,'media_url'=>$media_raw->url);  
                    }
                }
                                
                $user_array=array(
                    "post_id"=>$raw->id,
                    "post_type"=>$raw->post_type,
                    "user_id"=>$raw->user_id,
                    "post_by"=>$raw->user_info->full_name,
                    "profile_image"=>$raw->user_info->profile_image,
                    "date_of_post"=>date('d-m-Y',strtotime($raw->created_at)),
                    "time_of_post"=>date('h:i:s A',strtotime($raw->created_at)),
                    "location_of_post"=>$raw->location,
                    "text_message"=>$raw->text_message_typed,
                    "media"=>$media,
                    "total_number_of_likes"=>$raw->like_count_count,
                    "total_number_of_comments"=>$raw->comment_count_count,
                    "total_number_of_share"=>$raw->share_count_count,
                    "is_liked"=>$is_liked
                    ); 
        }
        if($request->post_type=='Group_post')
        {
           $raw= GroupPost::where('id',$request->post_id)->withCount('like_count','comment_count','share_count')->with(['user_info','is_like'=> function ($query) use ($user_id) {
            $query->where('user_id', $user_id);
           },'group_post_media','group_info'])->first();
           //dd($raw->group_post_media);
            if (count($raw->is_like)>0)
            $is_liked=1;

            $media=array();
            if($raw->type=='media')
            {
                foreach($raw->group_post_media as $media_raw)
                {
                    $media[]=array('media_type'=>$media_raw->media_type,'media_url'=>$media_raw->group_media);  
                }
            }
                            
            $user_array=array(
                "post_id"=>$raw->id,
                "post_type"=>'Group_post',
                "group_id"=>$raw->group_info->id,
                "group_name"=>$raw->group_info->title,
                "group_image"=>$raw->group_info->image,
                "user_id"=>$raw->user_id,
                "post_by"=>$raw->user_info->full_name,
                "profile_image"=>$raw->user_info->profile_image,
                "date_of_post"=>date('d-m-Y',strtotime($raw->created_at)),
                "time_of_post"=>date('h:i:s A',strtotime($raw->created_at)),
                "location_of_post"=>$raw->location,
                "text_message"=>$raw->post_text,
                "media"=>$media,
                "total_number_of_likes"=>$raw->like_count_count,
                "total_number_of_comments"=>$raw->comment_count_count,
                "total_number_of_share"=>$raw->share_count_count,
                "is_liked"=>$is_liked
                ); 

        }

        if($request->post_type=='Event_post')
        {
            $raw=EventPost::where('id',$request->post_id)->withCount('like_count','comment_count','share_count')->with(['user_info','is_like'=> function ($query) use ($user_id) {
                $query->where('user_id', $user_id);
            },'event_post_media','event_info'])->first();
                
            if (count($raw->is_like)>0)
            $is_liked=1;

            $media=array();
            if($raw->type=='media')
            {
                foreach($raw->event_post_media as $media_raw)
                {
                    $media[]=array('media_type'=>$media_raw->media_type,'media_url'=>$media_raw->group_media);  
                }
            }
                            
            $user_array=array(
                "post_id"=>$raw->id,
                "post_type"=>'Event_post',
                "event_id"=>$raw->event_info->id,
                "event_name"=>$raw->event_info->title_of_event,
                "event_image"=>$raw->event_info->event_image,
                "user_id"=>$raw->user_id,
                "post_by"=>$raw->user_info->full_name,
                "profile_image"=>$raw->user_info->profile_image,
                "date_of_post"=>date('d-m-Y',strtotime($raw->created_at)),
                "time_of_post"=>date('h:i:s A',strtotime($raw->created_at)),
                "location_of_post"=>$raw->location,
                "text_message"=>$raw->post_text,
                "media"=>$media,
                "total_number_of_likes"=>$raw->like_count_count,
                "total_number_of_comments"=>$raw->comment_count_count,
                "total_number_of_share"=>$raw->share_count_count,
                "is_liked"=>$is_liked
                ); 

        }
        if($request->post_type=='Page_post')
        {
        $raw=PagePost::where('id',$request->post_id)->withCount('like_count','comment_count','share_count')->with(['user_info','is_like'=> function ($query) use ($user_id) {
            $query->where('user_id', $user_id);
        },'page_post_media','page_info'])->first();
            
            if (count($raw->is_like)>0)
            $is_liked=1;

            $media=array();
            if($raw->type=='media')
            {
                foreach($raw->page_post_media as $media_raw)
                {
                    $media[]=array('media_type'=>$media_raw->media_type,'media_url'=>$media_raw->page_media);  
                }
            }
                            
            $user_array=array(
                "post_id"=>$raw->id,
                "post_type"=>'Page_post',
                "page_id"=>$raw->page_info->id,
                "page_name"=>$raw->page_info->page_name,
                "page_image"=>$raw->page_info->image,
                "user_id"=>$raw->user_id,
                "post_by"=>$raw->user_info->full_name,
                "profile_image"=>$raw->user_info->profile_image,
                "date_of_post"=>date('d-m-Y',strtotime($raw->created_at)),
                "time_of_post"=>date('h:i:s A',strtotime($raw->created_at)),
                "location_of_post"=>$raw->location,
                "text_message"=>$raw->text_message_typed,
                "media"=>$media,
                "total_number_of_likes"=>$raw->like_count_count,
                "total_number_of_comments"=>$raw->comment_count_count,
                "total_number_of_share"=>$raw->share_count_count,
                "is_liked"=>$is_liked
                ); 

        }
        
        if(count($user_array)>0)  
        return response()->json(['status'=>TRUE,"msg"=>config('constants')['SUCCESS'],'data'=>$user_array],200, [], JSON_NUMERIC_CHECK);
        else
        return response()->json(['status'=>FALSE,"msg"=>config('constants')['NO_POST'],'data'=>[]],200);
        
        
    }
    public function postShare(Request $request)
    {
        $res = $this->validation($request,'post_id,user_id,post_user_id');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        if($request->post_type=='Group_post')
        {
         return $this->groupPostShare($request);
        }
        else if($request->post_type=='Event_post')
        {
            return  $this->eventPostShare($request);
        }
        else if($request->post_type=='Page_post')
        {
            return  $this->pagePostShare($request);
        }
        else
        { 
        $request->merge(['type'=>'Share']);
        //$share=Post::insert($request->all());
        $share=Post_meta::insert($request->except(['post_type']));
        if($share){
            if($request->user_id!=$request->post_user_id){
            $user_info=User::select('full_name')->where('id',$request->user_id)->first();
            $friend_info=User::select('device_token')->where('id',$request->post_user_id)->first();
            Notification::insert(['user_id'=>$request->post_user_id,'type'=>'Post','post_id'=>$request->post_id,'msg'=>@$user_info->full_name.' Share your post']);
            if($friend_info->device_token){
                $msg=array('title'=>'New Share On Post','body'=>@$user_info->full_name.' Shared Your Post','sound'=>'default');
                $data=array('url'=>'','type'=>'post_share','post_type'=>'Post','id'=>$request->post_id);
                $fields=array('to'=>$friend_info->device_token,'data'=>$data,'notification'=>$msg,'priority'=>'high','content_available'=>true);
                $this->send_messages_android($fields);
                }
            }
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]); 
        }
        else
        {return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);} 
        }
    }
    public function createAlbum(Request $request)
    {
        $res = $this->validation($request,'user_id,album_name,privacy,tags');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        //dd($request->all());
        $album=User_album::insertGetId($request->except(['tags','media']));
        if($album)
        { $tags=json_decode($request->tags);
            if($tags)
            {
                foreach($tags as $raw)
                {
                    $list[]=array(
                        'album_id'=>$album,
                        'user_id'=>$request->user_id,
                        'friend_id'=>$raw->user_id,
                    );
                }
                Album_tag::insert($list);
            }   
            $media=json_decode($request->media);
            if($media){
            foreach($media as $raw)
            {
                $media_array[]=array(
                    'user_id'=>$request->user_id,
                    'type'=>$raw->type,
                    'url'=>$raw->media,
                    'user_post_id'=>0,
                    'album_id' =>$album
                                   );
            }
            User_media::insert($media_array);
            }
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]); 
        }else{
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400); 
        }
    }
    public function gallery($id)
    {
        if(!$id){
            return response()->json(['status'=>FALSE,'msg'=>'user_id is missing'],400);
            }
        $gallery=User_media::where('user_id',$id)->with('album')->get();
        if(count($gallery)>0)
        {
            foreach($gallery as $raw)
            {
                if($raw->album_id==1 || $raw->album_id==2 || $raw->album_id==3)
                $is_default=1;
                else
                $is_default=0;

                $data[]=array(
                    'id'=>$raw->id,
                    'user_id'=>$raw->user_id,
                    'type'=>$raw->type,
                    'url'=>$raw->url,
                    'user_post_id'=>$raw->user_post_id,
                    'album_id'=>$raw->album_id,
                    'created_at'=>date($raw->created_at),
                    'album'=>$raw->album,
                    'is_default_album'=>$is_default,
                                
            );
            }
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1'],'data'=>$data]); 
        }
        else
        {
            return response()->json(['status'=>FALSE,'msg'=>config('constants')['GALLERY_EMPTY']],400); 
        }
    }
    public function albumUpload(Request $request)
    {
        $res = $this->validation($request,'user_id,album_id');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        ////check album exist//////////
        if(User_album::where('user_id',$request->user_id)->where('id',$request->album_id)->first())
        {
        $media=json_decode($request->media);
            if($media){
            foreach($media as $raw)
            {
                $media_array[]=array(
                    'user_id'=>$request->user_id,
                    'type'=>$raw->type,
                    'url'=>$raw->media,
                    'user_post_id'=>0,
                    'album_id' =>$request->album_id
                                   );
            }
            User_media::insert($media_array);
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]); 
            }
            return response()->json(['status'=>FALSE,'msg'=>'Please Select Media']); 
        }
        return response()->json(['status'=>FALSE,'msg'=>'Invalid Album Selected']);
        
    }
    public function notificationList(Request $request)
    {
        $list=Notification::where('user_id',$request->user_id)->orderBy('id','desc')->get();
        if(count($list)>0)
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1'],'data'=>$list],200,[],JSON_NUMERIC_CHECK); 
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400); 
    }

    /********************user post end */
/* user friends-------------------------*/    
    public function addFriend(Request $request)
    {
        $res = $this->validation($request,'user_id,friend_id');
        if($res){
            return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        $check_exist=User_friend::where('user_id',$request->user_id)->where('friend_id',$request->friend_id)->first();
        if(!$check_exist)
        {
            User_friend::insertGetId($request->all());
            /////send notification//////////////
            $friend_info=User::select('device_token')->where('id',$request->friend_id)->first();
            Notification::insert(['user_id'=>$request->friend_id,'type'=>'new_friend_request','msg'=>'You got a new friend request']);
            if($friend_info->device_token){
                $msg=array('title'=>'New Friend Request','body'=>'You got a new friend request','sound'=>'default');
                $data=array('url'=>'','type'=>'new_friend_request','id'=>'');
                $fields=array('to'=>$friend_info->device_token,'data'=>$data,'notification'=>$msg,'priority'=>'high','content_available'=>true);
                $this->send_messages_android($fields);
                }

            //User_friend::insertGetId(['user_id'=>$request->friend_id,'friend_id'=>$request->user_id]);
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]);
        }
        else
        {return response()->json(['status'=>FALSE,'msg'=>'Friends Request Already Sent and status is '.$check_exist->status],400);}
    }

    public function friendsRequest($id)
    {
        if(!$id){
            return response()->json(['status'=>FALSE,'msg'=>'user_id is missing'],400);
            }
            $friends_list=DB::select("SELECT users.id,users.email,users.profile_image,users.full_name,specialities.title as specialities,specialities.icon,
            (user_block.block_user_id IS NOT NULL) as blocked,(user_mute.mute_user_id IS NOT NULL) as muted, (a.user_id IS NOT NULL) as follow,(select count(friend_id) from user_friends where user_id=$id and friend_id in
            (select friend_id from user_friends where user_id=users.id and status='Accepted') ) as mutual_friends FROM user_friends 
            left join users on users.id=user_friends.user_id
            left join specialities on users.specialitie_id=specialities.id
            left join user_block on users.id=user_block.block_user_id and user_block.user_id=$id
            left join user_mute on users.id=user_mute.mute_user_id and user_mute.user_id=$id
            left join follows as a on users.id=a.user_id and a.follower_id=$id
            where user_friends.friend_id=$id and user_friends.status='Pending'");
        if(count($friends_list)>0)
        {
            foreach($friends_list as $raw)
            {
                if(@$raw->user_details->speciality)
                $speciality=$raw->user_details->speciality;
                else
                $speciality='';
                //dd($raw->user_details);
                $data[]=array(
                    'id'=>$raw->id,
                    'full_name_of_friend'=>$raw->full_name,
                    'email'=>$raw->email,
                    'speciality'=>$raw->specialities,
                    'speciality_icon'=>$raw->icon,
                    'friend_profile_image'=>$raw->profile_image,
                    'mutual_friends'=>$raw->mutual_friends,
                    'muted'=>$raw->muted,
                    'blocked'=>$raw->blocked,
                    'fellow'=>0,
                    'follow'=>$raw->follow,
                );
            }
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1'],'data'=>$data],200, [], JSON_NUMERIC_CHECK); 
        }
        else
        {
            return response()->json(['status'=>FALSE,'msg'=>config('constants')['NO_FRIENDS_REQUEST']],400); 
        }
    }

    public function respondeFriendsRequest(Request $request)
    {
        $res = $this->validation($request,'user_id,friend_id,status');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        $check_exist=User_friend::where('user_id',$request->friend_id)->where('friend_id',$request->user_id)->first();
        if($check_exist)
        {
            if($request->status=='Accepted'){
                $user_info=User::select('full_name')->where('id',$request->user_id)->first();
                $friend_info=User::select('device_token')->where('id',$request->friend_id)->first();
                Notification::insert(['user_id'=>$request->friend_id,'type'=>'Friend_request_accepted','msg'=>'Your friend request is accepted by '.@$user_info->full_name]);
                if($friend_info->device_token){
                    $msg=array('title'=>'Friend Request Accepted by '.@$user_info->full_name,'body'=>'Your friend request is accepted by '.@$user_info->full_name,'sound'=>'default');
                    $data=array('url'=>'','type'=>'Friend_request_accepted','id'=>$request->user_id);
                    $fields=array('to'=>$friend_info->device_token,'data'=>$data,'notification'=>$msg,'priority'=>'high','content_available'=>true);
                    $this->send_messages_android($fields);
                    }    
            User_friend::where('user_id',$request->friend_id)->where('friend_id',$request->user_id)->update(['status'=>$request->status]);
            User_friend::insertGetId(['user_id'=>$request->user_id,'friend_id'=>$request->friend_id,'status'=>'Accepted']);
            }
            else
            {
                User_friend::where('user_id',$request->friend_id)->where('friend_id',$request->user_id)->delete();  
            }
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]);
        }
        else
        {return response()->json(['status'=>FALSE,'msg'=>'Record not found'],400);}
        

    }
    public function userFriends($id)
    {
        if(!$id){
            return response()->json(['status'=>FALSE,'msg'=>'user_id is missing'],400);
            }
            $friends_list=DB::select("SELECT users.id,users.email,users.profile_image,users.full_name,specialities.title as specialities,specialities.icon,
            (profile_blocked.block_user_id IS NOT NULL) as blocked,(profile_muted.mute_user_id IS NOT NULL) as muted, (a.user_id IS NOT NULL) as follow FROM user_friends 
            left join users on users.id=user_friends.friend_id
            left join specialities on users.specialitie_id=specialities.id
            left join profile_blocked on users.id=profile_blocked.block_user_id and profile_blocked.user_id=$id
            left join profile_muted on users.id=profile_muted.mute_user_id and profile_muted.user_id=$id
            left join follows as a on users.id=a.user_id and a.follower_id=$id
            where user_friends.user_id=$id and user_friends.status='Accepted'");
        if(count($friends_list)>0)
        {
            foreach($friends_list as $raw)
            {
                if(@$raw->user_details->speciality)
                $speciality=$raw->user_details->speciality;
                else
                $speciality='';
                //dd($raw->user_details);
                $data[]=array(
                    'id'=>$raw->id,
                    'full_name_of_friend'=>$raw->full_name,
                    'email'=>$raw->email,
                    'speciality'=>$raw->specialities,
                    'speciality_icon'=>$raw->icon,
                    'friend_profile_image'=>$raw->profile_image,
                    'mutual_friends'=>0,
                    'muted'=>$raw->muted,
                    'blocked'=>$raw->blocked,
                    'fellow'=>1,
                    'follow'=>$raw->follow,
                );
            }
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1'],'data'=>$data],200, [], JSON_NUMERIC_CHECK); 
        }
        else
        {
            return response()->json(['status'=>FALSE,'msg'=>config('constants')['NO_FRIENDS']],400); 
        }
    }
    public function userFriends_event($id)
    {
        if(!$id){
            return response()->json(['status'=>FALSE,'msg'=>'user_id is missing'],400);
            }
            $friends_list=DB::select("SELECT users.id,users.email,users.profile_image,users.full_name,specialities.title as specialities,specialities.icon,
            (user_block.block_user_id IS NOT NULL) as blocked,(user_mute.mute_user_id IS NOT NULL) as muted, (a.user_id IS NOT NULL) as follow FROM user_friends 
            left join users on users.id=user_friends.friend_id
            left join specialities on users.specialitie_id=specialities.id
            left join user_block on users.id=user_block.block_user_id and user_block.user_id=$id
            left join user_mute on users.id=user_mute.mute_user_id and user_mute.user_id=$id
            left join follows as a on users.id=a.user_id and a.follower_id=$id
            where user_friends.user_id=$id and user_friends.status='Accepted'");
        if(count($friends_list)>0)
        {
            foreach($friends_list as $raw)
            {
                if(@$raw->user_details->speciality)
                $speciality=$raw->user_details->speciality;
                else
                $speciality='';
                //dd($raw->user_details);
                $data[]=array(
                    'id'=>$raw->id,
                    'full_name_of_friend'=>$raw->full_name,
                    'email'=>$raw->email,
                    'speciality'=>$raw->specialities,
                    'speciality_icon'=>$raw->icon,
                    'friend_profile_image'=>$raw->profile_image,
                    'mutual_friends'=>0,
                    'muted'=>$raw->muted,
                    'blocked'=>$raw->blocked,
                    'fellow'=>1,
                    'follow'=>$raw->follow,
                );
            }
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1'],'data'=>$data],200, [], JSON_NUMERIC_CHECK); 
        }
        else
        {
            return response()->json(['status'=>FALSE,'msg'=>config('constants')['NO_FRIENDS']],400); 
        }
    }

    public function followUser(Request $request)
    {
        $res = $this->validation($request,'user_id,follower_id');
        if($res){
            return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        $check_exist=Follows::where('user_id',$request->user_id)->where('follower_id',$request->follower_id)->first();
        if(!$check_exist)
        {
            $user_info=User::select('full_name')->where('id',$request->user_id)->first();
            $friend_info=User::select('device_token')->where('id',$request->follower_id)->first();
            Notification::insert(['user_id'=>$request->follower_id,'type'=>'Follow','post_id'=>$request->user_id,'msg'=>@$user_info->full_name.' started followed you']);
            if($friend_info->device_token){
                $msg=array('title'=>'New Follow by '.@$user_info->full_name,'body'=>@$user_info->full_name.' started followed you','sound'=>'default');
                $data=array('url'=>'','type'=>'Follow','id'=>$request->user_id);
                $fields=array('to'=>$friend_info->device_token,'data'=>$data,'notification'=>$msg,'priority'=>'high','content_available'=>true);
                $this->send_messages_android($fields);
                }    
        Follows::insertGetId($request->all());
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]);
        }
        else
        {return response()->json(['status'=>FALSE,'msg'=>config('constants')['ALREADY_FOLLOWING']],400);}
    }

    public function unFollowUser(Request $request)
    {
        $res = $this->validation($request,'user_id,follower_id');
        if($res){
            return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
       
        if(Follows::where('user_id',$request->user_id)->where('follower_id',$request->follower_id)->delete())
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]);
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400);
    }

    public function userFollower($id)
    {
        
        if(!$id){
            return response()->json(['status'=>FALSE,'msg'=>'user_id Missing'],400);
        }
        $friends_list=DB::select("SELECT users.id,users.email,users.profile_image,users.full_name,specialities.title as specialities,specialities.icon,
        (user_block.block_user_id IS NOT NULL) as blocked,(user_mute.mute_user_id IS NOT NULL) as muted, (user_friends.friend_id IS NOT NULL) as fellow, (a.user_id IS NOT NULL) as follow FROM follows 
        left join users on users.id=follows.follower_id
        left join specialities on users.specialitie_id=specialities.id
        left join user_block on users.id=user_block.block_user_id and user_block.user_id=$id
        left join user_mute on users.id=user_mute.mute_user_id and user_mute.user_id=$id
        left join user_friends on users.id=user_friends.friend_id and user_friends.user_id=$id 
        left join follows as a on users.id=a.user_id and a.follower_id=$id
        where follows.user_id=$id");
        if(count($friends_list)>0)
        {
            foreach($friends_list as $raw)
            {
                if(@$raw->user_details->speciality)
                $speciality=$raw->user_details->speciality;
                else
                $speciality='';
                 
                //dd($raw->user_details);
                $data[]=array(
                    'id'=>$raw->id,
                    'full_name_of_friend'=>$raw->full_name,
                    'email'=>$raw->email,
                    'speciality'=>$raw->specialities,
                    'speciality_icon'=>$raw->icon,
                    'friend_profile_image'=>$raw->profile_image,
                    'mutual_friends'=>0,
                    'muted'=>$raw->muted,
                    'blocked'=>$raw->blocked,
                    'fellow'=>$raw->fellow,
                    'follow'=>$raw->follow,
                );
            }
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1'],'data'=>$data],200, [], JSON_NUMERIC_CHECK); 
        }
        else
        {
            return response()->json(['status'=>FALSE,'msg'=>config('constants')['NO_FRIENDS']],400); 
        }
    }
    public function userFollowing($id)
    {
        
        if(!$id){
            return response()->json(['status'=>FALSE,'msg'=>'user_id Missing'],400);
        }
         
        if(!$id){
            return response()->json(['status'=>FALSE,'msg'=>'user_id Missing'],400);
        }
        $friends_list=DB::select("SELECT users.id,users.email,users.profile_image,users.full_name,specialities.title as specialities,specialities.icon,
        (user_block.block_user_id IS NOT NULL) as blocked,(user_mute.mute_user_id IS NOT NULL) as muted, (user_friends.friend_id IS NOT NULL) as fellow FROM follows 
            left join users on users.id=follows.follower_id
            left join specialities on users.specialitie_id=specialities.id
            left join user_block on users.id=user_block.block_user_id and user_block.user_id=$id
            left join user_mute on users.id=user_mute.mute_user_id and user_mute.user_id=$id
            left join user_friends on users.id=user_friends.friend_id and user_friends.user_id=$id 
            
            where follows.follower_id=$id");
        if(count($friends_list)>0)
        {
            foreach($friends_list as $raw)
            {
                if(@$raw->user_details->speciality)
                $speciality=$raw->user_details->speciality;
                else
                $speciality='';
                 
               //dd($raw->user_details);
                $data[]=array(
                    'id'=>$raw->id,
                    'full_name_of_friend'=>$raw->full_name,
                    'email'=>$raw->email,
                    'speciality'=>$raw->specialities,
                    'speciality_icon'=>$raw->icon,
                    'friend_profile_image'=>$raw->profile_image,
                    'mutual_friends'=>0,
                    'muted'=>$raw->muted,
                    'blocked'=>$raw->blocked,
                    'fellow'=>$raw->fellow,
                    'follow'=>1,
                );
            }
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1'],'data'=>$data],200, [], JSON_NUMERIC_CHECK); 
        }
        else
        {
            return response()->json(['status'=>FALSE,'msg'=>config('constants')['NO_FRIENDS']],400); 
        }
    }

    public function userBlock(Request $request)
    {
        $res = $this->validation($request,'user_id,block_user_id');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        if(User::where('id',$request->user_id)->count()>0 && User::where('id',$request->block_user_id)->count()>0)
        {
            if(User_block::where('user_id',$request->user_id)->where('block_user_id',$request->block_user_id)->count()>0)
            {
                return response()->json(['status'=>FALSE,'msg'=>config('constants')['ALREADY_BLOCK']],400);   
            }
            else
            {

                if(User_block::insertGetId($request->all()) > 0){
                return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]);   
            }
            else
            {
                return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);   
            }
        }
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400);
    }
    public function userUnBlock(Request $request)
    {
        $res = $this->validation($request,'user_id,block_user_id');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        
        $delete=User_block::where('user_id',$request->user_id)->where('block_user_id',$request->block_user_id)->first();
        if($delete){ $delete->delete(); }
        if($delete)
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]); 
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400); 
        
    }
    public function blockList($id)
    {
        
        if(!$id){
        return response()->json(['status'=>FALSE,'msg'=>'user_id Missing'],400);
        }  
        if(User::where('id',$id)->count()>0)
        {   $list= User_block::where('user_id',$id)->with(array('user_info'=>function($query){
            $query->select('id','full_name','profile_image');
        }))->get();
        
          // dd($list);
            if(count($list)>0){
                foreach($list as $raw){
                    $data[]=$raw->user_info;
                }
                
                return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1'],'data'=>$data],200, [], JSON_NUMERIC_CHECK);   
            }
            else
            {
                return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);   
            }
        }
        
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400);
    }
    public function userMute(Request $request)
    {
        $res = $this->validation($request,'user_id,mute_user_id');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        if(User::where('id',$request->user_id)->count()>0 && User::where('id',$request->mute_user_id)->count()>0)
        {
            if(User_mute::where('user_id',$request->user_id)->where('mute_user_id',$request->mute_user_id)->count()>0)
            {
                return response()->json(['status'=>FALSE,'msg'=>config('constants')['ALREADY_BLOCK']],400);   
            }
            else
            {

                if(User_mute::insertGetId($request->all()) > 0){
                return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]);   
            }
            else
            {
                return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);   
            }
        }
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400);
    }
    public function userUnMute(Request $request)
    {
        $res = $this->validation($request,'user_id,mute_user_id');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        
        $delete=User_mute::where('user_id',$request->user_id)->where('mute_user_id',$request->mute_user_id)->first();
        if($delete){ $delete->delete(); }
        if($delete)
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]); 
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400); 
        
    }

    public function suggested($id)
    {
        $user_friends=User_friend::select('friend_id')->where('user_id',$id)->get();
        
        if(count($user_friends)>0){
            foreach($user_friends as $raw)
            {
                $friends[]=$raw->friend_id;
            }

        $friends=implode(', ',$friends);
        $suggested_list=DB::select("SELECT users.id,users.email,users.profile_image,users.full_name,specialities.title as specialities,specialities.icon,
        (user_block.block_user_id IS NOT NULL) as blocked,(user_mute.mute_user_id IS NOT NULL) as muted, (a.user_id IS NOT NULL) as follow ,
        (a.user_id IS NOT NULL) as follow,(select count(friend_id) from user_friends where user_id=$id and friend_id in
            (select friend_id from user_friends where user_id=users.id and status='Accepted') ) as mutual_friends FROM user_friends 
        left join users on users.id=user_friends.friend_id
        left join specialities on users.specialitie_id=specialities.id
        left join user_block on users.id=user_block.block_user_id and user_block.user_id=$id
        left join user_mute on users.id=user_mute.mute_user_id and user_mute.user_id=$id
        left join follows as a on users.id=a.user_id and a.follower_id=$id
        where user_friends.user_id in ($friends) and users.id not in ($friends) and users.id !=$id Group By users.id");

        if(count($suggested_list)>0)
        {
            foreach($suggested_list as $raw)
            {
                if(@$raw->user_details->speciality)
                $speciality=$raw->user_details->speciality;
                else
                $speciality='';
                 
               //dd($raw->user_details);
                $data[]=array(
                    'id'=>$raw->id,
                    'full_name_of_friend'=>$raw->full_name,
                    'email'=>$raw->email,
                    'speciality'=>$raw->specialities,
                    'speciality_icon'=>$raw->icon,
                    'friend_profile_image'=>$raw->profile_image,
                    'mutual_friends'=>$raw->mutual_friends,
                    'muted'=>$raw->muted,
                    'blocked'=>$raw->blocked,
                    'fellow'=>0,
                    'follow'=>$raw->follow,
                );
            }
            //array_push($suggested_list,$a);
            //$suggested_list['fellow']=0;
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1'],'data'=>$data],200, [], JSON_NUMERIC_CHECK); 
        }
        else
        {
            return response()->json(['status'=>FALSE,'msg'=>'friend list empty'],400); 
        }
        }
        else
        {
            return response()->json(['status'=>FALSE,'msg'=>'friend list empty'],400); 
        }

    }
    public function unFriend(Request $request)
    {
        $res = $this->validation($request,'user_id,friend_id');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }

        
        if(User_friend::where('user_id',$request->user_id)->where('friend_id',$request->friend_id)->delete())
        {
            User_friend::where('user_id',$request->friend_id)->where('friend_id',$request->user_id)->delete();
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]);
        }
        else
        {return response()->json(['status'=>FALSE,'msg'=>'users are not friends'],400);}

    }
    public function viewFriendsprofile(Request $request)
    {
        $res = $this->validation($request,'user_id,friend_id');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        $user_profile=User::where('id',$request->friend_id)->with('user_intern_infos','user_qualification','current_work_place','field_of_interest')->first();
            if($user_profile)
            {

              $title= $user_profile->user_title;

              $intern_info=array();
              $user_qualification=array();
              $current_work_place=array();
              $user_website=array();
              $user_mobile=array();
              $field_of_interest=array();
              $university_name='';
              $speciality_title='';
              $current_work_place_name='';
              foreach($user_profile->user_intern_infos as $raw_intern)
              {
                $university_name=$raw_intern->university_name;
                $intern_info[]=array( 
                "id"=>$raw_intern->id,
                "user_id"=>(int)$raw_intern->user_id,
                "university_id"=>(int)$raw_intern->university_id,
                "intern_id"=>(int)$raw_intern->intern_id,
                "practice_number"=>$raw_intern->practice_number,
                "country_id"=>(int)$raw_intern->country_id,
                "university_name"=>$raw_intern->university_name,
                "intern_name"=>$raw_intern->intern_name,
                "created_at"=>date('Y-m-d',strtotime($raw_intern->created_at)),
                "updated_at"=>date('Y-m-d',strtotime($raw_intern->updated_at))
                );
              }
              foreach($user_profile->user_qualification  as $raw_qualification)
              {
                if((int)$raw_qualification->default_qualification==1){  
                $speciality_title=$raw_qualification->speciality[0]->title;
                }
                  $q=array();
                  $s=array();
                  $q[]=array(
                        "id"=>(int)@$raw_qualification->qualification[0]->id,
                        "title"=>$raw_qualification->qualification[0]->title,
                        "code"=>$raw_qualification->qualification[0]->code,
                        "created_at"=>date('Y-m-d',strtotime($raw_qualification->qualification[0]->created_at)),
                        "updated_at"=>date('Y-m-d',strtotime($raw_qualification->qualification[0]->updated_at))
                           );
                  $s[]=array(
                    "id"=>(int)@$raw_qualification->speciality[0]->id,
                    "title"=>$raw_qualification->speciality[0]->title,
                    "icon"=>$raw_qualification->speciality[0]->icon,
                    "created_at"=>date('Y-m-d',strtotime($raw_qualification->speciality[0]->created_at)),
                    "updated_at"=>date('Y-m-d',strtotime($raw_qualification->speciality[0]->updated_at))
                  );          
                $user_qualification[]=array(
                    "id"=>(int)$raw_qualification->id,
                    "user_id"=>(int)$raw_qualification->user_id,
                    "qualification_id"=>(int)$raw_qualification->qualification_id,
                    "speciality_id"=>(int)$raw_qualification->speciality_id,
                    "default_qualification"=>(int)$raw_qualification->default_qualification,
                    "country_id"=>(int)$raw_qualification->country_id,
                    "registration_number"=>$raw_qualification->registration_number,
                    "certificate"=>$raw_qualification->certificate,
                    "created_at"=>date('Y-m-d',strtotime($raw_qualification->created_at)),
                    "updated_at"=>date('Y-m-d',strtotime($raw_qualification->updated_at)),
                    "qualification"=>$q,
                    "speciality"=>$s,
                    );
              }
              
              foreach($user_profile->current_work_place as $raw_work)
              {
                $current_work_place_name=$raw_work->current_work_place_name;
                $current_work_place[]=array(
                    "id"=>(int)$raw_work->id,
                    "user_id"=>(int)$raw_work->user_id,
                    "position"=>$raw_work->position,
                    "current_work_place_name"=>$raw_work->current_work_place_name,
                    "country_id"=>(int)$raw_work->country_id,
                    "practice_no"=>$raw_work->practice_no,
                    "from_date"=>$raw_work->from_date,
                    "to_date"=>$raw_work->to_date,
                    "status"=>(int)$raw_work->status,
                    "created_at"=>date('Y-m-d',strtotime($raw_work->created_at)),
                    "updated_at"=>date('Y-m-d',strtotime($raw_work->updated_at))
                  );
              }
              
              
              foreach($user_profile->field_of_interest as $r_field_of_interest)
              {
                  
                  $f=array(
                    "id"=>$r_field_of_interest->field->id,
                    "title"=>$r_field_of_interest->field->title,
                    "icon"=>$r_field_of_interest->field->icon,
                    "created_at"=>date('Y-m-d',strtotime($r_field_of_interest->field->created_at)),
                    "updated_at"=>date('Y-m-d',strtotime($r_field_of_interest->field->updated_at))
                  );

                $field_of_interest[]=array(
                "id"=>$r_field_of_interest->id,
                "user_id"=>(int)$r_field_of_interest->user_id,
                "field_id"=>(int)$r_field_of_interest->field_id,
                "updated_at"=>date('Y-m-d',strtotime($r_field_of_interest->updated_at)),
                "created_at"=>date('Y-m-d',strtotime($r_field_of_interest->created_at)),
                "field"=>$f
                  );
              }
              
              if((int)$user_profile->qualification_category==1)
              $cat_title=$university_name;
              else if ((int)$user_profile->qualification_category==2)
              $cat_title=$speciality_title;
              else if ((int)$user_profile->qualification_category==3)
              $cat_title=$current_work_place_name;
              else
              $cat_title='';

              //get user category info
              
              $user_info=array(
                "id"=>(int)$user_profile->id,
                "qualification_category"=>(int)$user_profile->qualification_category,
                'qualification_title'=>$cat_title,
                "title"=>(int)$user_profile->title,
                "first_name"=>$user_profile->first_name,
                "last_name"=>$user_profile->last_name,
                "full_name"=>$user_profile->full_name,
                "dob"=>$user_profile->dob,
                "email"=>$user_profile->email,
                "profile_image"=>$user_profile->profile_image,
                "cover_image"=>$user_profile->cover_image,
                "mobile"=>$user_profile->mobile,
                "country_id"=>(int)$user_profile->country_id,
                "about_me"=>$user_profile->about_me,
                "created_at"=>date('Y-m-d',strtotime($user_profile->created_at)),
                "updated_at"=>date('Y-m-d',strtotime($user_profile->updated_at)),
                "field_of_interest"=>$field_of_interest
              );  
              $friends_list=DB::select("select users.title_name as title,users.first_name,users.last_name, user_friends.friend_id from user_friends 
              join users on users.id=user_friends.friend_id
              where user_friends.user_id='$request->user_id' and user_friends.status='Accepted' and user_friends.friend_id in
              (select friend_id from user_friends where user_friends.user_id='$request->friend_id' and user_friends.status='Accepted')
              ");
           $data=array('user_info'=>$user_info,'mutual_friends'=>$friends_list);
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1'],'data'=>$data],200, [], JSON_NUMERIC_CHECK); 
            
            }else{
            return response()->json(['status'=>FALSE,'msg'=>config('constants')['INVALID_USER']],400);
            }
       

    }

/*---------------------------------*/
/*events----------*/    
    function randomString(){
        $pool = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        return substr(str_shuffle(str_repeat($pool, 8)), 0, 8);
    }
   
    public function user_groups($id)
    {
        if(!$id){
            return response()->json(['status'=>FALSE,'msg'=>'user_id is missing'],400);
            }
            $result=Group::where('user_id',$id)->get();
        if(count($result)>0){
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1'],'data'=>$result]); 
        }
        else
        {
            return response()->json(['status'=>FALSE,'msg'=>config('constants')['NO_GROUP_FOUD']],400); 
        }

    }
    public function createEvent(Request $request)
    {
        $res = $this->validation($request,'user_id,event_image,title_of_event,event_type,event_tags,event_start,event_end,rsvp_datetime,location_detail,event_description,event_host');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        if($request->event_type==1)
        {
            if($request->user_id!=$request->event_host)
            {
                return response()->json(['status'=>FALSE,'msg'=>'user_id & event_host not matched'],400); 
            }
        }
        if($request->event_type==2)
        {
            $request->merge(['guest_can_invite'=>'N']);
        }
        if($request->event_type==3)
        {
            if(!Group::where('owner_id',$request->user_id)->where('id',$request->event_host)->first())
            {
                return response()->json(['status'=>FALSE,'msg'=>'you are not admin of this group'],400); 
            }
        }

        $request->merge(['unique_id'=>$this->generateRandomString()]);
        $co_host=json_decode($request->co_host);
        $uplods=json_decode($request->uplods);
        $event_id=Event::insertGetId($request->except(['co_host','uplods']));
        if($event_id)
        {
            /////enter uplods///////////////
            if($uplods){
                foreach($uplods as $raw)
                {
                       $files[]=array('event_id'=>$event_id,
                                      'upload'=>$raw->upload);
                }    
                Event_attachment::insert($files);
                }

            ////enter co host/////
            if($co_host){
            foreach($co_host as $raw_co_host)
            {
                   $raw_co_host_array[]=array('event_id'=>$event_id,
                                              'co_host_id'=>$raw_co_host->user_id
                                             );
            }    
            Event_co_hosts::insert($raw_co_host_array);
            }
            //$data=$this->eventDetails($event_id,$request->event_type);
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['EVENT_CREATED']]);
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
    }
    public function editEvent(Request $request)
    {   
        //DB::enableQueryLog();
        //dd(DB::getQueryLog());
        $res = $this->validation($request,'id,user_id,event_image,title_of_event,event_type,event_tags,event_start,event_end,rsvp_datetime,location_detail,event_description,event_host');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        if($request->event_type==1)
        {
            if($request->user_id!=$request->event_host)
            {
                return response()->json(['status'=>FALSE,'msg'=>'user_id & event_host not matched'],400); 
            }
        }
        if($request->event_type==2)
        {
            $request->merge(['guest_can_invite'=>'N']);
        }
        if($request->event_type==3)
        {
            if(!Event::where('user_id',$request->user_id)->first())
            {
                return response()->json(['status'=>FALSE,'msg'=>'you are not admin of this group'],400); 
            }
        }

        //$request->merge(['unique_id'=>$this->generateRandomString()]);
        $co_host=json_decode($request->co_host);
        $uplods=json_decode($request->uplods);
        $event_id=Event::where('id',$request->id)->update($request->except(['co_host','uplods']));
        if($event_id)
        {
            /////enter uplods///////////////
            if($uplods){
                Event_attachment::where('event_id',$request->id)->delete();

                foreach($uplods as $raw)
                {
                       $files[]=array('event_id'=>$request->id,
                                      'upload'=>$raw->upload);
                }    
                Event_attachment::insert($files);
                }
            ////enter co host/////
            if($co_host){
            //////////////delete old co host///////////
            Event_co_hosts::where('event_id',$request->id)->delete();
            ///////insert new co host//////////////////
            foreach($co_host as $raw_co_host)
            {
                   $raw_co_host_array[]=array('event_id'=>$request->id,
                                              'co_host_id'=>$raw_co_host->user_id
                                             );
            }    
            Event_co_hosts::insert($raw_co_host_array);
            }
            $join_user=DB::select("SELECT GROUP_CONCAT(users.id) as users_id,events.title_of_event,GROUP_CONCAT(users.device_token) as device_token  FROM `event_joined_users`
            LEFT JOIN events on events.id=event_joined_users.event_id
            LEFT JOIN users on users.id=`event_joined_users`.joined_user_id 
            where event_joined_users.event_id='$request->id' and event_joined_users.response_type='1' ");
           //dd($join_user[0]->title_of_event);
           
           if($join_user)
           {
            /***********Add Notification**********/   
            $postUserFullName=User::select('full_name')->where('id',$request->user_id)->first(); 
            $usersId=explode(',',$join_user[0]->users_id);
                foreach($usersId as $raw){
                $notification[]=array(
                'user_id'=>$raw,
                'type'=>'Event_detail',
                'post_id'=>$request->id,
                'msg'=>@$postUserFullName->full_name.' update event info'
                 );
                 }
            Notification::insert($notification);

            /***********Send Push Notification**********/    
            $user=explode(',',$join_user[0]->device_token);
            
            $msg=array('title'=>$join_user[0]->title_of_event,'body'=>@$postUserFullName->full_name.' update event info-'.$join_user[0]->title_of_event,'sound'=>'default');
            $data=array('url'=>'','type'=>'Event_info_update','id'=>$request->id);
            $fields=array('registration_ids'=>$user,'data'=>$data,'notification'=>$msg,'priority'=>'high','content_available'=>true);
            $this->send_messages_android($fields);
            //dd($fields);
           }
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['EVENT_UPDATED']]);
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
    }
    public function eventDetails(Request $request)
    {
        if(!$request->id){
            return response()->json(['status'=>FALSE,'msg'=>'event_id is missing'],400);
            }
            if(!$request->user_id){
                return response()->json(['status'=>FALSE,'msg'=>'user_id is missing'],400);
                }
            $total_invited_persons=0;
            $total_going_persons=1;
            $total_maybe_persons=0;
            $response_type=0;

        $user_events=Event::where('id',$request->id)->with('event_admin','tag')->first();
        if(!$user_events)
        return response()->json(['status'=>FALSE,'msg'=>'Invalid Event Id'],400);


        $Event_joined_user=array();
        $COhost=array();
        $user_type='guest';
        
        if($user_events->user_id==$request->user_id)
        {
            $user_type='host';
        }
        //////event co host///////
        $co_host=Event_co_hosts::where('event_id',$request->id)->with(array('co_host'=>function($query){
            $query->select('id','full_name','profile_image');
        }))->get();
        /////event invited user
        $Event_joined_user=Event_joined_user::where('event_id',$request->id)->with(array('joined_user'=>function($query){
            $query->select('id','full_name','profile_image');
        }))->get();
        
        
        if(count($co_host)>0)
        {
            foreach($co_host as $raw)
            {
              if(@$raw->co_host->id){  
              $COhost[]=array('id'=>$raw->co_host->id,'user_name'=>$raw->co_host->full_name);
              if(@$raw->co_host->id==$request->user_id)
              {
                  $user_type='cohost';
              }
            }
            }
        }
        //dd($COhost);
        if(count($Event_joined_user)>0)
        {
            foreach($Event_joined_user as $raw)
            {
              if($raw->joined_user_id==$request->user_id)
              {
                $response_type=$raw->response_type;
              }  
              
              
              $total_invited_persons=$total_invited_persons+1;  
              if($raw->response_type=='1')
              { $total_going_persons=$total_going_persons+1; }

              if($raw->response_type=='3')
              {
                 $total_maybe_persons =$total_maybe_persons+1;
              }

              
            }
        }
       
        if($user_events)
        {
            $uplods=Event_attachment::where('event_id',$request->id)->get();  
            $data=array(
                'user_type'=>$user_type,
               'id'=>$user_events->id,
               'user_id'=>$user_events->event_admin->id,
               'user_full_name'=>$user_events->event_admin->full_name,
               'event_image'=>$user_events->event_image,
               'title_of_event'=>$user_events->title_of_event,
               'event_type'=>$user_events->event_type,
               'event_tags'=>$user_events->tag,
               'event_start'=>$user_events->event_start,
               'event_end'=>$user_events->event_end,
               'rsvp_datetime'=>$user_events->rsvp_datetime,
               'location_detail'=>$user_events->location_detail,
               'event_description'=>$user_events->event_description,
               'sponsors'=>$user_events->sponsors,
               'ticket_url'=>$user_events->ticket_url,
               'approved_by_admin'=>$user_events->approved_by_admin,
               'guest_can_invite'=>$user_events->guest_can_invite,
               'cteated_at'=>$user_events->cteated_at,
               'co_host'=>$COhost,
               'upload'=>$uplods,
               'total_going_persons'=>$total_going_persons,
               'total_maybe_persons'=>$total_maybe_persons,
               'total_invited_persons'=>$total_invited_persons,
               'response_type'=>(int)$response_type
            );
                                
           
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1'],'data'=>$data],200, [], JSON_NUMERIC_CHECK); 
            
        }
        else
        {
            return response()->json(['status'=>FALSE,'msg'=>config('constants')['EVENT_EMPTY']],400); 
        }
    }
    public function deleteEvent(Request $request)
    {
        $res = $this->validation($request,'event_id,user_id');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        
        $event_details=Event::where('user_id',$request->user_id)->where('id',$request->event_id)->first();
        if($event_details){ $event_details->delete(); }
        if($event_details)
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]); 
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400); 
        
    }
    public function eventInviteFriends(Request $request)
    {
        $res = $this->validation($request,'user_id,event_id');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        
        ///check event////////////
        $event_data=Event::where('id',$request->event_id)->first();
        if($event_data)
        {

            $host[]=$event_data->event_host;
            
            $co_host=Event_co_hosts::where('event_id',$request->event_id)->with(array('co_host'=>function($query){
                $query->select('id','full_name','profile_image');
            }))->get();
            if(count($co_host)>0)
            {
            foreach($co_host as $raw)
            {
              $host[]=$raw->co_host->id;
            }
            }
            
            $host1=implode(', ',$host);
            //dd($host1);
         ///check not public event
            if($event_data->guest_can_invite=='N') 
            {
            //check if user is in host or cohost
            if(Event::where('id',$request->event_id)->where('event_host',$request->user_id)->first())
            {
             ////ok
            }
            else if(Event_co_hosts::where('event_id',$request->event_id)->where('co_host_id',$request->user_id)->first())
            {
             ////ok   
            }
            else
            {
                return response()->json(['status'=>FALSE,'msg'=>'Sorry, guest invite disabled by event admin'],400); 
            }
            }
            else
            {
            
            
            if(Event::where('id',$request->event_id)->where('event_host',$request->user_id)->first())
            {
             ////ok
            }
            else if(Event_co_hosts::where('event_id',$request->event_id)->where('co_host_id',$request->user_id)->first())
            {
             ////ok   
            }
            else if(Event_joined_user::where('event_id',$request->event_id)->where('joined_user_id',$request->user_id)->whereIn('joined_by',array($host1))->first())
            {
             /////ok
            }
            else
            {
                return response()->json(['status'=>FALSE,'msg'=>'Sorry, guest of guest invite disabled by event admin'],400); 
            }   
            
            } 
         

            $friends_list=json_decode($request->friends_list);
            if(!$friends_list)
            {
            return response()->json(['status'=>FALSE,'msg'=>'Friend list Empty'],400);
            }
            $data=array();
            $already=array();
            foreach($friends_list as $raw)
            {
                //check user already invited or not
                if(!Event_joined_user::where('joined_user_id',$raw->user_id)->where('event_id',$request->event_id)->first())
                {

                $data[]=array('event_id'=>$request->event_id,
                                'joined_user_id'=>$raw->user_id,
                                'joined_by'=>$request->user_id
                                );
                    ///notification
                    $user_info=User::select('full_name')->where('id',$request->user_id)->first();
                    $notification[]=array(
                        'user_id'=>$raw->user_id,
                        'type'=>'Event_join_request',
                        'msg'=>@$user_info->full_name.' Invite you in a Event'
                    );
                    
                    $friend_info=User::select('device_token')->where('id',$raw->user_id)->first();
                    if($friend_info->device_token){
                    $msg=array('title'=>@$user_info->full_name.' Invite you in a Event','body'=>@$user_info->full_name.' Invite you in a Event','sound'=>'default');
                    $data1=array('url'=>'','type'=>'Event_join_request','id'=>$request->event_id);
                    $fields=array('to'=>$friend_info->device_token,'data'=>$data1,'notification'=>$msg,'priority'=>'high','content_available'=>true);
                    $this->send_messages_android($fields);
                    }
                
                    Notification::insert($notification);
            }
            else
            {
                
                $already[]=array('user_id'=>$raw->user_id);
            }
        }    
        if(count($data)>0){
        Event_joined_user::insert($data);
        }
        $responce=array('added'=>$data,'already_joined'=>$already);
        return response()->json(['status'=>TRUE,'msg'=>'','data'=>$responce],200);


        }
        else
        {
            return response()->json(['status'=>FALSE,'msg'=>'Event not found'],400);
        }

    }
    public function EventsHosted_old($id)
    {
        $data=array();
        if(!$id){
            return response()->json(['status'=>FALSE,'msg'=>'user_id is missing'],400);
            }
            $end=date('Y-m-d H:i:s');
            //DB::enableQueryLog();
            $user_events = Event::select('events.*',
             DB::raw('DATE_FORMAT(event_start, "%d %b %Y") as start_date'),
             DB::raw('DATE_FORMAT(event_start, "%H:%i") as start_time'),
             DB::raw('DATE_FORMAT(event_end, "%d %b %Y") as end_date'),
             DB::raw('DATE_FORMAT(event_end, "%H:%i") as end_time'))->where('event_end','>=',$end)->where('user_id',$id)->with('totalgoing')->get();
             //dd(DB::getQueryLog());
        //$user_events=Event::where('user_id',$id)->where('event_end','>=',$end)->get();
        //dd($user_events);
         foreach($user_events as $raw)
         {
             $data[]=array(
                 'id'=>$raw->id,
                 'unique_id'=>$raw->unique_id,
                 'user_id'=>$raw->user_id,
                 'event_image'=>$raw->event_image,
                 'title_of_event'=>$raw->title_of_event,
                 'event_type'=>$raw->event_type,
                 'event_host'=>$raw->event_host,
                 'event_tags'=>$raw->event_tags,
                 'event_start'=>$raw->event_start,
                 'event_end'=>$raw->event_end,
                 'rsvp_datetime'=>$raw->rsvp_datetime,
                 'location_detail'=>$raw->location_detail,
                 'event_lat'=>$raw->event_lat,
                 'event_long'=>$raw->event_long,
                 'event_description'=>$raw->event_description,
                 'sponsors'=>$raw->sponsors,
                 'ticket_url'=>$raw->ticket_url,
                 'approved_by_admin'=>$raw->approved_by_admin,
                 'guest_can_invite'=>$raw->guest_can_invite,
                 'cteated_at'=>$raw->cteated_at,
                 'updated_at'=>$raw->updated_at,
                 'start_date'=>$raw->start_date,
                 'start_time'=>$raw->start_time,
                 'end_date'=>$raw->end_date,
                 'end_time'=>$raw->end_time,
                 'totalgoing'=>@$raw->totalgoing()->count(),
             );

         }


         $ds=array();
        $cohost_events=DB::select("SELECT events.*,DATE_FORMAT(events.event_start, '%d %b %Y') as `start_date`, 
        DATE_FORMAT(events.event_start,'%H:%i') as `start_time`,DATE_FORMAT(events.event_end, '%d %b %Y') as `end_date`, 
        DATE_FORMAT(events.event_end,'%H:%i') as `end_time`,
            users.full_name AS `invited_by_name`
              FROM `event_co_hosts` 
            left join `events` on `event_co_hosts`.`event_id`=`events`.`id`
            left join `users` on `events`.`user_id`=`users`.`id`
            
            where `events`.`event_end` >='$end' and `event_co_hosts`.`co_host_id`=$id " );
        if(count($cohost_events)>0)
        {
            foreach($cohost_events as $raw)
            {
                $ds[]=array(
                'id'=>$raw->id,
                'unique_id'=>$raw->unique_id,
                'user_id'=>$raw->user_id,
                'event_image'=>$raw->event_image,
                'title_of_event'=>$raw->title_of_event,
                'event_type'=>$raw->event_type,
                'event_host'=>$raw->event_host,
                'event_tags'=>$raw->event_tags,
                'event_start'=>$raw->event_start,
                'event_end'=>$raw->event_end,
                'rsvp_datetime'=>$raw->rsvp_datetime,
                'location_detail'=>$raw->location_detail,
                'event_lat'=>$raw->event_lat,
                'event_long'=>$raw->event_long,
                'event_description'=>$raw->event_description,
                'sponsors'=>$raw->sponsors,
                'ticket_url'=>$raw->ticket_url,
                'approved_by_admin'=>$raw->approved_by_admin,
                'guest_can_invite'=>$raw->guest_can_invite,
                'cteated_at'=>$raw->cteated_at,
                'updated_at'=>$raw->updated_at,
                'start_date'=>$raw->start_date,
                'start_time'=>$raw->start_time,
                'end_date'=>$raw->end_date,
                'end_time'=>$raw->end_time,
                'invited_by_name'=>$raw->invited_by_name,
                'totalgoing'=>0
                );
            }
        }
            
        //dd($cohost_events);
        $data=array('hosted_events'=>$data,'co_hosted_events'=>$ds);
        if(count($user_events)>0 || count($ds)>0)
        {
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1'],'data'=>$data],200, [], JSON_NUMERIC_CHECK); 
        }
        else
        {
            return response()->json(['status'=>FALSE,'msg'=>config('constants')['EVENT_EMPTY']],400); 
        }
    }
    public function EventsHosted($id)
    {
        $data=array();
        if(!$id){
            return response()->json(['status'=>FALSE,'msg'=>'user_id is missing'],400);
            }
            $end=date('Y-m-d H:i:s');
            //DB::enableQueryLog();
            $user_events = Event::select('events.*',
             DB::raw('DATE_FORMAT(event_start, "%d %b %Y") as start_date'),
             DB::raw('DATE_FORMAT(event_start, "%H:%i") as start_time'),
             DB::raw('DATE_FORMAT(event_end, "%d %b %Y") as end_date'),
             DB::raw('DATE_FORMAT(event_end, "%H:%i") as end_time'))->where('event_end','>=',$end)->where('user_id',$id)->with('totalgoing')->get();
             //dd(DB::getQueryLog());
        //$user_events=Event::where('user_id',$id)->where('event_end','>=',$end)->get();
        //dd($user_events);
         foreach($user_events as $raw)
         {
             $data[]=array(
                 'id'=>$raw->id,
                 'unique_id'=>$raw->unique_id,
                 'user_id'=>$raw->user_id,
                 'event_image'=>$raw->event_image,
                 'title_of_event'=>$raw->title_of_event,
                 'event_type'=>$raw->event_type,
                 'event_host'=>$raw->event_host,
                 'event_tags'=>$raw->event_tags,
                 'event_start'=>$raw->event_start,
                 'event_end'=>$raw->event_end,
                 'rsvp_datetime'=>$raw->rsvp_datetime,
                 'location_detail'=>$raw->location_detail,
                 'event_lat'=>$raw->event_lat,
                 'event_long'=>$raw->event_long,
                 'event_description'=>$raw->event_description,
                 'sponsors'=>$raw->sponsors,
                 'ticket_url'=>$raw->ticket_url,
                 'approved_by_admin'=>$raw->approved_by_admin,
                 'guest_can_invite'=>$raw->guest_can_invite,
                 'cteated_at'=>$raw->cteated_at,
                 'updated_at'=>$raw->updated_at,
                 'start_date'=>$raw->start_date,
                 'start_time'=>$raw->start_time,
                 'end_date'=>$raw->end_date,
                 'end_time'=>$raw->end_time,
                 'totalgoing'=>@$raw->totalgoing()->count(),
             );

         }



        $cohost_events=DB::select("SELECT (select count(*) from event_joined_users where event_id = events.id AND response_type= 1) as totalgoing, events.*,DATE_FORMAT(events.event_start, '%d %b %Y') as `start_date`, 
        DATE_FORMAT(events.event_start,'%H:%i') as `start_time`,DATE_FORMAT(events.event_end, '%d %b %Y') as `end_date`, 
        DATE_FORMAT(events.event_end,'%H:%i') as `end_time`,
            users.full_name AS `invited_by_name`
              FROM `event_co_hosts` 
            left join `events` on `event_co_hosts`.`event_id`=`events`.`id`
            left join `users` on `events`.`user_id`=`users`.`id`
            -- left join `event_joined_users` on `events`.`id`=`event_joined_users`.`event_id` and `event_joined_users`.`response_type`='1'
            where `events`.`event_end` >='$end' and `event_co_hosts`.`co_host_id`=$id group by `events`.`id`");
        //dd($cohost_events);
        $data=array('hosted_events'=>$data,'co_hosted_events'=>$cohost_events);
        if(count($user_events)>0 || count($cohost_events)>0)
        {
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1'],'data'=>$data],200, [], JSON_NUMERIC_CHECK); 
        }
        else
        {
            return response()->json(['status'=>FALSE,'msg'=>config('constants')['EVENT_EMPTY']],400); 
        }
    }
    public function EventHome($id)
    { 
        if(!$id){
            return response()->json(['status'=>FALSE,'msg'=>'user_id is missing'],400);
            }
            $end=date('Y-m-d H:i:s');

            $user_events=DB::select("SELECT events.id,events.title_of_event,events.location_detail,events.event_image,DATE_FORMAT(events.event_start, '%d %b %Y') as `start_date`, 
            DATE_FORMAT(events.event_start,'%H:%i') as `start_time`,DATE_FORMAT(events.event_end, '%d %b %Y') as `end_date`, 
            DATE_FORMAT(events.event_end,'%H:%i') as `end_time`,
            users.full_name AS `invited_by_name`, `event_joined_users`.`response_type`, (select count(id) from event_joined_users where event_id=events.id ) as totalgoing  FROM `event_joined_users` 
            left join `events` on `event_joined_users`.`event_id`=`events`.`id`
            left join `users` on `event_joined_users`.`joined_by`=`users`.`id`
            where `events`.`event_end` >='$end' and `event_joined_users`.`joined_user_id`=$id order by `events`.`event_start` asc");
    
        if(count($user_events)>0)
        {
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1'],'data'=>$user_events],200, [], JSON_NUMERIC_CHECK); 
        }
        else
        {
            return response()->json(['status'=>FALSE,'msg'=>config('constants')['EVENT_EMPTY']],400); 
        }
    }
    public function eventSearch(Request $request)
    {
        $search='';
        $location='';
        $tag='';
        $date='';
            if($request->search_string!='')
            {
                $search= " and events.title_of_event like '%$request->search_string%'";
            } 
            if($request->location!='')
            {
                $location= " and events.location_detail ='$request->location'";
            } 
            if($request->tag!='')
            {
                $tag= " and events.event_tags ='$request->tag'";
            } 
            if($request->start_date_time!='' && $request->end_date_time!='')
            {
                $date= " and events.event_start>='$request->start_date_time' and events.event_end<='$request->end_date_time' ";
            }
            else if ($request->start_date_time!='' && $request->end_date_time=='')  
            {
                $date= " and events.event_start>='$request->start_date_time'";
            } 
            else if ($request->start_date_time=='' && $request->end_date_time!='')
            {
                $date= " and events.event_end<='$request->end_date_time' ";
            }
            
            $end=date('Y-m-d H:i:s');
            $search_result=DB::select("SELECT events.id,events.title_of_event,events.event_image,events.location_detail,events.event_start,events.event_end,
            users.full_name AS `invited_by_name` , `event_joined_users`.`response_type`
            ,DATE_FORMAT(events.event_start, '%d %b %Y') as `start_date`, 
            DATE_FORMAT(events.event_start,'%H:%i') as `start_time`,DATE_FORMAT(events.event_end, '%d %b %Y') as `end_date`, 
            DATE_FORMAT(events.event_end,'%H:%i') as `end_time`,(select count(id) from event_joined_users where event_id=events.id ) as totalgoing
             FROM `event_joined_users` 
            left join `events` on `event_joined_users`.`event_id`=`events`.`id`
            left join `users` on `event_joined_users`.`joined_by`=`users`.`id`
            where `event_joined_users`.`joined_user_id`=$request->user_id $search $location $tag $date");
    
        if(count($search_result)>0)  
        {
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1'],'data'=>$search_result],200, [], JSON_NUMERIC_CHECK); 
        }
        else
        {
            return response()->json(['status'=>FALSE,'msg'=>config('constants')['EVENT_EMPTY']],400); 
        }
        //dd($search_result);

    }
     
/*-------------------------------*/    
    
    public function createPublicEvent(Request $request)
    {
        $res = $this->validation($request,'user_id,event_image,title_of_event,event_type,event_host,event_tags,event_datetime,event_description');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        $event_location=json_decode($request->event_location);
        $co_host=json_decode($request->co_host);
       // dd($co_host);
        
        foreach($event_location as $raw)
        {
              $event_loc=explode('-',$raw->location_lat_long);
              $request->merge(['location_detail' => $raw->location_detail,'event_lat'=> $event_loc[0],'event_long'=> $event_loc[1]]);
        }
        $request->merge(['approved_by_admin' => $request->post_or_event_approved_by_admin]);
       // dd($co_host);
        $event_id=Event::insertGetId($request->except(['event_location','co_host','post_or_event_approved_by_admin']));
        if($event_id)
        {
            foreach($co_host as $raw_co_host)
            {
                   $raw_co_host_array[]=array('event_id'=>$event_id,
                                              'joined_user_id'=>$raw_co_host->user_id
                                             );
            }    
            Event_joined_user::insert($raw_co_host_array);
            $data=$this->eventDetails($event_id,'1');
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['EVENT_CREATED'],'data'=>$data]);
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
    }
    public function createGroupEvent(Request $request)
    {
        $res = $this->validation($request,'user_id,event_image,title_of_event,event_type,event_host,event_tags,event_datetime,event_description,guest_can_invite');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        $event_location=json_decode($request->event_location);
        $co_host=json_decode($request->co_host);
       // dd($co_host);
        
        foreach($event_location as $raw)
        {
              $event_loc=explode('-',$raw->location_lat_long);
              $request->merge(['location_detail' => $raw->location_detail,'event_lat'=> $event_loc[0],'event_long'=> $event_loc[1]]);
        }
        $event_id=Event::insertGetId($request->except(['event_location','co_host']));
        if($event_id)
        {
            foreach($co_host as $raw_co_host)
            {
                   $raw_co_host_array[]=array('event_id'=>$event_id,
                                              'joined_user_id'=>$raw_co_host->user_id
                                             );
            }    
            Event_joined_user::insert($raw_co_host_array);
            $data=$this->eventDetails($event_id,'1');
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['EVENT_CREATED'],'data'=>$data]);
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
    }
    public function eventJoinResponse(Request $request)
    {
        $res = $this->validation($request,'event_id,user_id,response_type');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        $check_event=Event_joined_user::where('event_id',$request->event_id)->where('joined_user_id',$request->user_id)->first();
        if($check_event)
        {
            $responce=Event_joined_user::where('event_id',$request->event_id)->where('joined_user_id',$request->user_id)->update($request->except(['event_id','user_id']));
            if($responce)
            {
                $user_info=User::select('full_name')->where('id',$request->user_id)->first();
                $friend_info=Event::where('id',$request->event_id)->with('event_admin')->first();
                if($friend_info->event_admin->device_token){
                $msg=array('title'=>@$user_info->full_name.' update event join status','body'=>@$user_info->full_name.' update event join status','sound'=>'default');
                $data=array('url'=>'','type'=>'Event_join_response','id'=>$request->event_id);
                $fields=array('to'=>$friend_info->event_admin->device_token,'data'=>$data,'notification'=>$msg,'priority'=>'high','content_available'=>true);
                $this->send_messages_android($fields);
                }
                Notification::insert(['user_id'=>$friend_info->event_admin->id,'type'=>'Event_join_response','post_id'=>$request->event_id,'msg'=>@$user_info->full_name.' update event join status']);
                return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]);} 
            else
            {return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400); }
        }
        else
        {
            return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400);  
        }
    }
/*********************Events Posts*********************/    
    public function eventAddPost(Request $request)
    {   

        $res = $this->validation($request,'user_id,event_id,type');
        if($res){
            return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        
        //////group details
        $event_datails=Event::where('id',$request->event_id)->first();
        
        if($event_datails)
        {
            $post_id = EventPost::insertGetId([
                'user_id'=>$request->user_id,
                'event_id'=>$request->event_id,
                'type'=>$request->type,
                'location'=>$request->location,
                'post_text'=>$request->post_text
            ]);
        if($request->type=='media')
        {
            if(!$request->media && $post_id>0){
                return response()->json(['status'=>FALSE,'msg'=>'Media is missing'],400);
            } else {
                $media=json_decode($request->media);
                foreach($media as $raw)
                {
                    $media_array[]=array(
                        'event_id'=>$request->event_id,
                        'media_type'=>$raw->type,
                        'event_media'=>$raw->media,
                        'event_post_id'=>$post_id
                    );
                }
                EventMedia::insert($media_array);
            }
        }
        if($post_id){
            $join_user=DB::select("SELECT GROUP_CONCAT(users.id) as users_id,events.title_of_event,GROUP_CONCAT(users.device_token) as device_token  FROM `event_joined_users`
            LEFT JOIN events on events.id=event_joined_users.event_id
            LEFT JOIN users on users.id=`event_joined_users`.joined_user_id 
            where event_joined_users.event_id='$request->event_id' and event_joined_users.response_type='1' ");
           //dd($join_user[0]->title_of_event);
           
           if($join_user){
            /***********Add Notification**********/   
            $postUserFullName=User::select('full_name')->where('id',$request->user_id)->first(); 
            $usersId=explode(',',$join_user[0]->users_id);
                foreach($usersId as $raw){
                $notification[]=array(
                'user_id'=>$raw,
                'type'=>'Event_post',
                'post_id'=>$post_id,
                'msg'=>@$postUserFullName->full_name.' made a post in Event-'.$join_user[0]->title_of_event
            );
            }
            Notification::insert($notification);

            /***********Send Push Notification**********/    
            $user=explode(',',$join_user[0]->device_token);
            
            $msg=array('title'=>$join_user[0]->title_of_event,'body'=>@$postUserFullName->full_name.' made a post in Event-'.$join_user[0]->title_of_event,'sound'=>'default');
            $data=array('url'=>'','type'=>'Event_post','post_type'=>'Event_post','id'=>$post_id);
            $fields=array('registration_ids'=>$user,'data'=>$data,'notification'=>$msg,'priority'=>'high','content_available'=>true);
            $this->send_messages_android($fields);

           }
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['POST_ADD_SUCCESS']]); 
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
        
        }
        else
        {
            return response()->json(['status'=>FALSE,'msg'=>'invalid event id'],400);   
        }
    }
    public function eventPosts(Request $request)
    {
        
        $user_array=array();
        $user_id=$request->user_id;
        //DB::enableQueryLog();dd(DB::getQueryLog());
        $event_posts = EventPost::where('event_id',$request->event_id)->withCount('like_count','comment_count','share_count')->with(['is_like'=> function ($query) use ($user_id) {
            $query->where('user_id', $user_id);
        },'user_info','event_post_media'])->orderBy('created_at', 'desc')->paginate(10, ['*'], '', $request->page);
        //dd(DB::getQueryLog());
        //dd($group_posts);
        if(count($event_posts)>0) {
            foreach($event_posts as $raw)
            { 
            
                if (count($raw->is_like)>0)
                $is_liked=1;
                else
                $is_liked=0;

                $media=array();
                if($raw->type=='media')
                {
                    foreach($raw->event_post_media as $media_raw)
                    {
                        $media[]=array('media_type'=>$media_raw->media_type,'media_url'=>$media_raw->event_media);  
                    }
                }
                
                
                $user_array[]=array(
                    "post_id"=>$raw->id,
                    "post_type"=>'Event_post',
                    "user_id"=>$raw->user_id,
                    "post_by"=>@$raw->user_info->full_name,
                    "profile_image"=>@$raw->user_info->profile_image,
                    "date_of_post"=>date('d-m-Y',strtotime($raw->created_at)),
                    "time_of_post"=>date('h:i:s A',strtotime($raw->created_at)),
                    "location_of_post"=>$raw->location,
                    "text_message"=>$raw->post_text,
                    "media"=>$media,
                    "total_number_of_likes"=>$raw->like_count_count,
                    "total_number_of_comments"=>$raw->comment_count_count,
                    "total_number_of_share"=>$raw->share_count_count,
                    "is_liked"=>$is_liked
                    ); 
                
            }
        }
        if(count($user_array)>0)
        return response()->json(['status'=>TRUE,"msg"=>config('constants')['SUCCESS'],'data'=>$user_array],200, [], JSON_NUMERIC_CHECK);
        else
        return response()->json(['status'=>TRUE,"msg"=>config('constants')['NOT_FOUND'],'data'=>[]]);
    }
    public function eventPostAllLike($id)
    {
        $post=EventPost::where('id',$id)->first();  
        if($post) {
        $user_id=$post->user_id;

        $all_like=Event_meta::where('post_id',$id)->where('type','Like')->with(['like_user_info','is_friend' => function ($q) use($user_id) {
            $q->where('user_id','=',$user_id);
            }
            ])->get();
        //dd($all_like);
        if(count($all_like)>0)
        {
            foreach($all_like as $raw)
            {
                    if($raw->is_friend)
                    $is_friend=1;
                    else
                    $is_friend=0;
                    $data[]=array(
                    "user_id"=>$raw->like_user_info->id,
                    "full_name"=>$raw->like_user_info->full_name,
                    "profile_pic"=>$raw->like_user_info->profile_image,
                    "color"=>$raw->like_user_info->user_title,
                    "is_friend"=>$is_friend );
            }
            return response()->json(['status'=>TRUE,"msg"=>config('constants')['SUCCESS'],'data'=>$data],200, [], JSON_NUMERIC_CHECK);
        }
        return response()->json(['status'=>FALSE,"msg"=>config('constants')['NO_LIKE_POST']],400);
        }
        else
        {
            return response()->json(['status'=>FALSE,"msg"=>'Invalid Post Id'],400);
        }
    }
    public function eventPostLikeDislike(Request $request)
    {   //dd(GroupPost::where('id',$request->post_id)->first());
        $res = $this->validation($request,'loggedin_user_id,post_user_id,post_id,current_status');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        $group=EventPost::select('id')->where('id',$request->post_id)->first(['id']);
        
        if(@$group->id){
        if($request->current_status==0)
        {
        $like=Event_meta::where('user_id',$request->loggedin_user_id)->where('post_id',$request->post_id)->where('type','Like')->first();
        if($like){ $like->delete(); }
        }
        else
        {
            $user_info=User::select('full_name')->where('id',$request->loggedin_user_id)->first();
            $friend_info=User::select('device_token')->where('id',$request->post_user_id)->first();
            Notification::insert(['user_id'=>$request->post_user_id,'type'=>'Event_post','post_id'=>$request->post_id,'msg'=>@$user_info->full_name.' Like Your Post']);
            if($friend_info->device_token){
                $msg=array('title'=>'New Like On Post','body'=>@$user_info->full_name.' Like Your Post','sound'=>'default');
                $data=array('url'=>'','type'=>'post_like','post_type'=>'Event_post','id'=>$request->post_id);
                $fields=array('to'=>$friend_info->device_token,'data'=>$data,'notification'=>$msg,'priority'=>'high','content_available'=>true);
                $this->send_messages_android($fields);
                }
        $like=Event_meta::updateOrCreate(['type'=>'Like','post_id'=>$request->post_id,'user_id'=>$request->loggedin_user_id],['type'=>'Like','post_user_id'=>$request->post_user_id,'user_id'=>$request->loggedin_user_id,'post_id'=>$request->post_id]);
        }
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]); 
        }
        else
        {
        return response()->json(['status'=>FALSE,'msg'=>'Invalid Post Id']);    
        }
    }
    public function eventPostComments(Request $request)
    {
        $user_id=$request->user_id;
        $comments=Event_meta::where('type','Comment')->where('post_id',$request->post_id)->with(['is_like'=> function ($query) use ($user_id) {
            $query->where('user_id', $user_id);
        },'comment_like','comment_reply','comment_user_info'])->get();
        
        if(count($comments)>0)
        {
            foreach($comments as $raw)
            {       if($raw->is_like)
                    $is_like=1;
                    else
                    $is_like=0;
                    $data[]=array(
                    "user_id"=>$raw->like_user_info->id,
                    "profile_pic"=>$raw->like_user_info->profile_image,
                    "user_full_name"=>$raw->like_user_info->full_name,
                    "comment_id"=>$raw->id,
                    "comment_text"=>$raw->comment_text,
                    "is_like"=>$is_like,
                    "like_count"=>count($raw->comment_like),
                    'comment_reply'=>$raw->comment_reply
                );
            }
            return response()->json(['status'=>TRUE,"msg"=>config('constants')['SUCCESS'],'data'=>$data],200, [], JSON_NUMERIC_CHECK);
        }
        return response()->json(['status'=>FALSE,"msg"=>config('constants')['NO_POST_COMMENTS']],400);
    }
    public function eventPostAddComment(Request $request)
    {
        $res = $this->validation($request,'loggedin_user_id,post_user_id,post_id,comment_text');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        $request->merge(['user_id'=>$request->loggedin_user_id,'type'=>'Comment']);
        $comment=Event_meta::insert($request->except(['loggedin_user_id','post_type']));
        if($comment)
        {
            $user_info=User::select('full_name')->where('id',$request->loggedin_user_id)->first();
            $friend_info=User::select('device_token')->where('id',$request->post_user_id)->first();
            Notification::insert(['user_id'=>$request->post_user_id,'type'=>'Event_post','post_id'=>$request->post_id,'msg'=>@$user_info->full_name.' Comment on Your Post']);
            if($friend_info->device_token){
                $msg=array('title'=>'New Comment On Post','body'=>@$user_info->full_name.' Comment on Your Post','sound'=>'default');
                $data=array('url'=>'','type'=>'post_comment','post_type'=>'Event_post','id'=>$request->post_id);
                $fields=array('to'=>$friend_info->device_token,'data'=>$data,'notification'=>$msg,'priority'=>'high','content_available'=>true);
                $this->send_messages_android($fields);
                }
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]); 
        }
        else
        {
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
        } 
        
    }
    public function eventPostEditComment(Request $request)
    {
        $res = $this->validation($request,'comment_id,comment_edited_text');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        $request->merge(['comment_text'=>$request->comment_edited_text]);
        $comment=Event_meta::where('id',$request->comment_id)->update($request->except(['comment_edited_text','comment_id','post_type']));
        if($comment)
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]); 
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400); 
        
    }
    public function eventPostDeleteComment(Request $request)
    {
        $res = $this->validation($request,'comment_id,loggedin_user_id');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        
        $comment=Event_meta::where('user_id',$request->loggedin_user_id)->where('id',$request->comment_id)->first();
        if($comment){ $comment->delete(); }
        if($comment)
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]); 
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400); 
        
    }
    public function eventCommentLikeDislike(Request $request)
    {
        $res = $this->validation($request,'user_id,comment_id');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        if($request->current_status=='0')
        {
        $like=Event_comment_meta::where('user_id',$request->user_id)->where('comment_id',$request->comment_id)->where('type','Like')->first();
        if($like){ $like->delete(); }
        }
        else
        {
            $post_id=Event_meta::select('post_user_id','post_id')->where('id',$request->comment_id)->first();
            $user_info=User::select('full_name')->where('id',$request->user_id)->first();
            $friend_info=User::select('device_token')->where('id',$post_id->post_user_id)->first();
            Notification::insert(['user_id'=>$post_id->post_user_id,'comment_id'=>$request->comment_id,'type'=>'Event_post','post_id'=>@$post_id->post_id,'msg'=>@$user_info->full_name.' Like your Comment']);
            if($friend_info->device_token){
                $msg=array('title'=>'New Comment Like','body'=>@$user_info->full_name.' Like your Comment','sound'=>'default');
                $data=array('url'=>'','type'=>'post_comment_like','post_type'=>'Event_post','id'=>@$post_id->post_id);
                $fields=array('to'=>$friend_info->device_token,'data'=>$data,'notification'=>$msg,'priority'=>'high','content_available'=>true);
                $this->send_messages_android($fields);
                }
        $like=Event_comment_meta::updateOrCreate(['type'=>'Like','comment_id'=>$request->comment_id,'user_id'=>$request->user_id],['type'=>'Like','comment_id'=>$request->comment_id,'user_id'=>$request->user_id]);
        }
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]);
    }
    public function eventCommentReply(Request $request)
    {
        $res = $this->validation($request,'user_id,comment_id,text');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
            
        Event_comment_meta::insert(['type'=>'Reply','comment_id'=>$request->comment_id,'user_id'=>$request->user_id,'text'=>$request->text]);
        //Comment_meta::insert(['type'=>'Reply','comment_id'=>$request->comment_id,'user_id'=>$request->user_id,'text'=>$request->text]);
        $post_id=Event_meta::select('post_user_id','post_id')->where('id',$request->comment_id)->first();
       
        if($post_id){
        $user_info=User::select('full_name')->where('id',$request->user_id)->first();
        $friend_info=User::select('device_token')->where('id',$post_id->post_user_id)->first();
        Notification::insert(['user_id'=>$post_id->post_user_id,'type'=>'Event_post','post_id'=>@$post_id->post_id, 'comment_id'=>$request->comment_id,'msg'=>@$user_info->full_name.' Reply on your Comment']);
        if($friend_info->device_token){
            $msg=array('title'=>'New Comment Reply','body'=>@$user_info->full_name.' Reply on your Comment','sound'=>'default');
            $data=array('url'=>'','type'=>'post_comment_reply','post_type'=>'Event_post','id'=>@$post_id->post_id,'comment_id'=>$request->comment_id,'name'=>@$user_info->full_name,'reply'=>$request->text);
            $fields=array('to'=>$friend_info->device_token,'data'=>$data,'notification'=>$msg,'priority'=>'high','content_available'=>true);
            $this->send_messages_android($fields);
            }
        }
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]);
    }
    public function eventDeletePost(Request $request)
    {
        $res = $this->validation($request,'post_id,user_id');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        
        $post_details=EventPost::where('user_id',$request->user_id)->where('id',$request->post_id)->first();
        if($post_details){ $post_details->delete(); }
        if($post_details)
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]); 
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400); 
        
    }
    public function eventPostShare(Request $request)
    {
        $res = $this->validation($request,'post_id,user_id,post_user_id');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        $request->merge(['type'=>'Share']);
        //$share=Post::insert($request->all());
        $share=Event_meta::insert($request->except(['post_type']));
        if($share)
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]); 
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400); 
        
    }
    public function eventMedia($id)
    {
       if(Event::where('id',$id)->first())
       {
         $data=EventMedia::select('event_post_id','event_media','media_type')->where('event_id',$id)->get();
         return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1'],'data'=>$data],200, [], JSON_NUMERIC_CHECK);
       }
       else
       {
        return response()->json(['status'=>FALSE,'msg'=>'Invalid Event ID'],400);
       }
    }
/**************************************************** */
    
    public function searchUser(Request $request)
    {
        $res = $this->validation($request,'search_string');
        if($res){
            return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        
        $check_exist=User::select('id','full_name','profile_image')->where('full_name','LIKE',"%$request->search_string%")->orWhere('email',$request->search_string)->with('speciality')->get();
        //dd($check_exist);
        if(count($check_exist)>0)
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1'],'data'=>$check_exist]);
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400);
    }
    ////////// Master country  API/////////////////////
    public function country(){
        return $this->core->success($this->core->country());
    }
     ////////// Master level API/////////////////////
    public function level(){
        return $this->core->success($this->core->level());
    }
     ////////// Master speciality API/////////////////////
    public function speciality(){
        return $this->core->success($this->core->speciality());
    }
 
    ////////// Master sub speciality API/////////////////////
    public function subSpeciality(){
        return $this->core->success($this->core->sub_speciality());
    }
     ////////// Master qualification listing API/////////////////////
    public function qualification(){
        return $this->core->success($this->core->qualification());
    }
     ////////// Master university listing API/////////////////////
    public function university(){
        return $this->core->success($this->core->university());
    }
     ////////// Master intership Listing API/////////////////////
    public function intership($id){
        if ( $id=='' ) {
            return response()->json(['status'=>FALSE,'msg'=>'Please Send university ID '],400);            
        }
        return $this->core->success($this->core->intership($id));
    }

/********** Start Groups Functions **********/
    public function group_friends(Request $request)
    {
        $res = $this->validation($request,'user_id,group_id');
        if($res){
            return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }

        $friends_list=DB::select("SELECT users.id,users.email,users.profile_image,users.full_name,specialities.title as specialities,specialities.icon,
        (user_block.block_user_id IS NOT NULL) as blocked,(user_mute.mute_user_id IS NOT NULL) as muted, (group_members.user_id IS NOT NULL) as Joined, (groups_co_admin.user_id IS NOT NULL) as moderator  FROM user_friends 
        left join users on users.id=user_friends.friend_id
        left join specialities on users.specialitie_id=specialities.id
        left join user_block on users.id=user_block.block_user_id and user_block.user_id=$request->user_id
        left join user_mute on users.id=user_mute.mute_user_id and user_mute.user_id=$request->user_id
        left join group_members on users.id=group_members.user_id and group_members.group_id=$request->group_id
        left join groups_co_admin on users.id=groups_co_admin.user_id and groups_co_admin.group_id=$request->group_id
        where user_friends.user_id=$request->user_id and user_friends.status='Accepted'");
        if(count($friends_list)>0)
        {
        foreach($friends_list as $raw)
        {
            if(@$raw->user_details->speciality)
            $speciality=$raw->user_details->speciality;
            else
            $speciality='';
            //dd($raw->user_details);
            $data[]=array(
                'id'=>$raw->id,
                'full_name_of_friend'=>$raw->full_name,
                'email'=>$raw->email,
                'speciality'=>$raw->specialities,
                'speciality_icon'=>$raw->icon,
                'friend_profile_image'=>$raw->profile_image,
                'mutual_friends'=>0,
                'muted'=>$raw->muted,
                'blocked'=>$raw->blocked,
                'joined'=>$raw->Joined,
                'moderator'=>$raw->moderator
            );
        }
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1'],'data'=>$data]); 
        }
         else
        {
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['NO_FRIENDS']],400); 
         }
    }
    public function addGroup(Request $request)
    {   
        $res = $this->validation($request,'user_id,title,membership_approval,posting_permission,post_approval,group_privacy,notification_setting,push_notification');
        if($res){
            return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        
        $lastGroupId = Group::insertGetId($request->all());
        if($lastGroupId){
             GroupMember::insert(['group_id'=>$lastGroupId,'user_id'=>$request->user_id,'role'=>'Admin','status'=>'Joined']);
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['GROUP_ADD'],'data'=>$lastGroupId]);
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
    }
    public function editGroup(Request $request)
    {   
        $res = $this->validation($request,'id,user_id,title,membership_approval,posting_permission,post_approval,group_privacy,notification_setting,push_notification');
        if($res){
            return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        
        $lastGroupId = Group::where('id',$request->id)->update($request->except(['id']));
        if($lastGroupId){
            $join_user=DB::select("SELECT GROUP_CONCAT(users.id) as users_id,groups.title,GROUP_CONCAT(users.device_token) as device_token  
            FROM `group_members`
            LEFT JOIN groups on groups.id=group_members.group_id
            LEFT JOIN users on users.id=group_members.user_id 
            where group_members.group_id='$request->id' and group_members.status='Joined' and group_members.role!='Admin' ");
           //dd($join_user[0]->title_of_event);
           
           if($join_user)
           {
            /***********Add Notification**********/   
            $postUserFullName=User::select('full_name')->where('id',$request->user_id)->first(); 
            $usersId=explode(',',$join_user[0]->users_id);
                foreach($usersId as $raw){
                $notification[]=array(
                'user_id'=>$raw,
                'type'=>'Group_detail',
                'post_id'=>$request->id,
                'msg'=>@$postUserFullName->full_name.' update group info'
                 );
                 }
            Notification::insert($notification);

            /***********Send Push Notification**********/    
            $user=explode(',',$join_user[0]->device_token);
            
            $msg=array('title'=>$join_user[0]->title,'body'=>@$postUserFullName->full_name.' update group info -'.$join_user[0]->title,'sound'=>'default');
            $data=array('url'=>'','type'=>'Group_info_update','id'=>$request->id);
            $fields=array('registration_ids'=>$user,'data'=>$data,'notification'=>$msg,'priority'=>'high','content_available'=>true);
            $this->send_messages_android($fields);
            //dd($fields);
           }
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['GROUP_UPDATED']]);
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
    }
    public function addGroupMember(Request $request){
        $res = $this->validation($request,'user_id,group_id,friends_list');
        if($res){
            return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        //////group details
        $group_datails=Group::where('id',$request->group_id)->first();
        ///group co admin list
        $coAdmin=GroupMember::select('user_id')->where('group_id',$request->group_id)->where('role','Moderator')->get();
        $coAdmin=$coAdmin->toArray();
        
        $coAdmin=array_column($coAdmin,'user_id');


        if($group_datails){
        if($group_datails->group_privacy!='public') 
        {    
            if (!in_array((int)$request->user_id,$coAdmin) && $request->user_id!=$group_datails->user_id ){

                return response()->json(['status'=>FALSE,'msg'=>'This Group is '.$group_datails->group_privacy.' And You are not admin or co admin of this group']);
             }
        }   
        
        $members=[];
        $friends_list=json_decode($request->friends_list);
         ///get already members
        $GroupMember=GroupMember::select('user_id')->where('group_id',$request->group_id)->get();
        $GroupMember=$GroupMember->toArray();
        
        $GroupMember=array_column($GroupMember,'user_id');
        foreach ($friends_list as $key => $value) {
            if (!in_array((int)$value->user_id,$GroupMember)){
                 ///notification
                 $user_info=User::select('full_name')->where('id',$request->user_id)->first();
                 $notification[]=array(
                    'user_id'=>$value->user_id,
                    'type'=>'Group_join_request',
                    'msg'=>@$user_info->full_name.' Invite you in a Group'
                );
                
                $friend_info=User::select('device_token')->where('id',$value->user_id)->first();
                if($friend_info->device_token){
                $msg=array('title'=>@$user_info->full_name.' Invite you in a Group','body'=>@$user_info->full_name.' Invite you in a Group','sound'=>'default');
                $data1=array('url'=>'','type'=>'Group_invite_request','id'=>$request->group_id);
                $fields=array('to'=>$friend_info->device_token,'data'=>$data1,'notification'=>$msg,'priority'=>'high','content_available'=>true);
                $this->send_messages_android($fields);
                }
                Notification::insert($notification);
              $members[]= ['group_id'=>$request->group_id,'user_id'=>$value->user_id,'status'=>'Joined'];
            }
                }
        if(count($members)>0){
        $rec = GroupMember::insert($members);
        
        if($rec)
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['GROUP_MEMBER_ADD']]);
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
        }
        else
        {
            return response()->json(['status'=>FALSE,'msg'=>'Users are already members of group']);
        }
        }
        else
        {
            return response()->json(['status'=>FALSE,'msg'=>'group not found']);
        }
    }
    public function groupDetails(Request $request)
    {   
        $res = $this->validation($request,'user_id,group_id');
        if($res){
            return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        $group_members=array();
        $group_post=array();
        $group_moderators=array();
        $user_role='guest';

        $result=Group::whereId($request->group_id)->with('group_member','group_admin','group_moderators')->with(['group_post' => function($q) {
            $q->take(5);
        }])->first();
        //dd($result->group_moderators[0]->member_details);
        if($result)
        {
          foreach($result->group_member as $raw)
          {
            if($request->user_id==$raw->member_details->id)  
            {
                $user_role='group_member'; 
            }
            $group_members[]=array(
                'user_id'=>$raw->member_details->id,
                'name'=>$raw->member_details->full_name,
                'image'=>$raw->member_details->profile_image
            );
          }
          foreach($result->group_post as $raw)
          {
              
            $post_media=array();
            foreach($raw->group_post_media as $media)
            {
                $post_media[]=array('media_id'=>$media->id,'url'=>$media->group_media,'media_type'=>$media->media_type);
            }  
            $group_post[]=array(
                'post_id'=>$raw->id,
                'user_id'=>$raw->user_info->id,
                'posted_by'=>$raw->user_info->full_name,
                'image'=>$raw->user_info->profile_image,
                'type'=>$raw->type,
                'location'=>$raw->location,
                'post_text'=>$raw->post_text,
                'created_at'=>date($raw->created_at),
                'post_media'=>$post_media
         
            );
          }
             $group_admin=array(
                 'id'=>@$result->group_admin->id,
                 'profile_image'=>@$result->group_admin->profile_image,
                 'name'=>@$result->group_admin->full_name
             );
             if($request->user_id==$result->group_admin->id)  
            {
                $user_role='group_admin'; 
            }
          foreach($result->group_moderators as $raw)
          {
            if($request->user_id==@$raw->member_details->id)  
            {
                $user_role='group_moderators'; 
            }
            $group_moderators[]=array(
                'user_id'=>$raw->member_details->id,
                'name'=>$raw->member_details->full_name,
                'image'=>$raw->member_details->profile_image
            );
          }
          $data[]=array(
              'id'=>$result->id,
              'user_id'=>$result->user_id,
              'title'=>$result->title,
              'description'=>$result->description,
              'image'=>$result->image,
              'group_privacy'=>$result->group_privacy,
              'notification_setting'=>$result->notification_setting,
              'push_notification'=>$result->push_notification,
              'member_request_notification'=>$result->member_request_notification,
              'membership_approval'=>$result->membership_approval,
              'posting_permission'=>$result->posting_permission,
              'post_approval'=>$result->post_approval,
              'role'=>$user_role,
              'group_members'=>$group_members,
              'group_admin'=>$group_admin,
              'group_moderators'=>$group_moderators,
              'group_post'=>$group_post,

          );
          
          return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS'],'data'=>$data],200, [], JSON_NUMERIC_CHECK);
        }
        else
        {
            return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400);
        }
    }
   
    public function exitGroupUser(Request $request)
    {
        if(GroupMember::where('user_id',$request->user_id)->where('group_id',$request->group_id)->delete())
        {
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]);
        }
        else
        {
        return response()->json(['status'=>FALSE,'msg'=>'User id found in group']);
        }
    }
    public function deleteGroup(Request $request)
    {
        $res = $this->validation($request,'user_id,group_id');
        if($res){
            return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        $group_datails=Group::where('id',$request->group_id)->first();
        if($group_datails){
        if($group_datails->user_id!=$request->user_id)     
        {
            return response()->json(['status'=>FALSE,'msg'=>'You are not group admin, You can not delete this Group']);
        }  

        if(Group::where('id',$group_datails->id)->delete())
        {
        //todo:delete group relation table data//////    
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]);
        }

        }
        else
        {
        return response()->json(['status'=>FALSE,'msg'=>'Group not found']);
        }

    }
    public function joinGroup(Request $request){
        $res = $this->validation($request,'user_id,group_id');
        if($res){
            return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        //////group details
        $group_datails=Group::where('id',$request->group_id)->first();
        if($group_datails){
        if($group_datails->group_privacy!='public') 
        {    
            return response()->json(['status'=>FALSE,'msg'=>'This Group is '.$group_datails->group_privacy.' group you can not join this group']);
        }   
        
        ///get already members
        $GroupMember=GroupMember::select('user_id','status')->where('group_id',$request->group_id)->where('user_id',$request->user_id)->first();
        if (!$GroupMember){
        $rec = GroupMember::insert(['group_id'=>$request->group_id,'user_id'=>$request->user_id]);
        if($rec)
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['GROUP_MEMBER_ADD']]);
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
        }
        else
        {
            return response()->json(['status'=>FALSE,'msg'=>'Users is already requested and status is '.$GroupMember->status]);
        }
        }
        else
        {
            return response()->json(['status'=>FALSE,'msg'=>'group not found']);
        }
    }

    public function groupRequestList(Request $request)
    {
        $res = $this->validation($request,'user_id,group_id');
        if($res){
            return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        //////group details
        $group_datails=Group::where('id',$request->group_id)->first();
        if($group_datails)
        {
            $GroupMember=GroupMember::where('group_id',$request->group_id)->where('status','Pending')->with(array('member_details'=>function($query){
                $query->select('id','full_name','profile_image','email');
            }))->get();
            if(count($GroupMember)>0)
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1'],'data'=>$GroupMember],200, [], JSON_NUMERIC_CHECK);
            else
            return response()->json(['status'=>FALSE,'msg'=>'No request found']);
        }
        else
        {
            return response()->json(['status'=>FALSE,'msg'=>'group not found']);
        }

    }
    public function groupRequestUpdate(Request $request)
    {
        $res = $this->validation($request,'approved_by_id,user_id,group_id,status');
        if($res){
            return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        $group_datails=Group::where('id',$request->group_id)->first();
        if($group_datails)
        {
          if($request->status=='Joined'){
          if(GroupMember::where('group_id',$request->group_id)->where('user_id',$request->user_id)->update(['status'=>$request->status]))
          return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]);
          else
          return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']]);
          
          }
          else
          {
            if(GroupMember::where('user_id',$request->user_id)->where('group_id',$request->group_id)->delete())
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]);
            else
            return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']]);
          }
        }
        else
        {
            return response()->json(['status'=>FALSE,'msg'=>'group not found']);
        }

    }
    public function groupMembersList(Request $request)
    {
        $res = $this->validation($request,'user_id,group_id');
        if($res){
            return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        //////group details
        $group_datails=Group::where('id',$request->group_id)->first();
        if($group_datails)
        {
            $GroupMember=GroupMember::where('group_id',$request->group_id)->where('status','Joined')->with('member_details:id,full_name,profile_image,email')->get();//,'blocked','muted'
            //dd($GroupMember);
            if(count($GroupMember)>0)
            {
                foreach($GroupMember as $raw)
                {   
                    // if($raw->blocked) $blocked=1; else $blocked=0;
                    // if($raw->muted) $muted=1; else $muted=0;
                    if(!$raw->blocked && !$raw->muted){
                    $data[]=array(
                        'role'=>$raw->role,
                        'group_id'=>$raw->group_id,
                        'user_id'=>$raw->user_id,
                        'full_name'=>$raw->member_details->full_name,
                        'profile_image'=>$raw->member_details->profile_image,
                        'email'=>$raw->member_details->email,
                        // 'blocked'=>$blocked,
                        // 'muted'=>$muted
                    );
                }
                }
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1'],'data'=>$data],200, [], JSON_NUMERIC_CHECK);
            }else{
            return response()->json(['status'=>FALSE,'msg'=>'No Members found']);
            }
        }
        else
        {
            return response()->json(['status'=>FALSE,'msg'=>'group not found']);
        } 
    }
    public function addModerators(Request $request){
        $res = $this->validation($request,'user_id,group_id,moderator_id');
        if($res){
            return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        //////group details
        $group_datails=Group::where('id',$request->group_id)->first();
        ///group co admin list
        $coAdmin=GroupMember::select('user_id')->where('group_id',$request->group_id)->where('role','Moderator')->get();
        $coAdmin=$coAdmin->toArray();
        $coAdmin=array_column($coAdmin,'user_id');
        if($group_datails){
           
            if (!in_array((int)$request->user_id,$coAdmin) && $request->user_id!=$group_datails->user_id ){

                return response()->json(['status'=>FALSE,'msg'=>'You are not admin or co admin of this group']);
             }
        ///check already Group_co_admin
        $check=GroupMember::where('user_id',$request->moderator_id)->where('group_id',$request->group_id)->first();
        if(!$check)
        {
            
        $rec = GroupMember::insert(['group_id'=>$request->group_id,'user_id'=>$request->moderator_id,'role'=>'Moderator','status'=>'Joined']);
        if($rec)
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['GROUP_MODERATOR_ADD']]);
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
        }
        else
        {
            if($check->role!='Moderator')
            {
                GroupMember::where('user_id',$request->moderator_id)->where('group_id',$request->group_id)->update(['role'=>'Moderator']);
                return response()->json(['status'=>TRUE,'msg'=>config('constants')['GROUP_MODERATOR_ADD']]);
            }
            else{
            return response()->json(['status'=>FALSE,'msg'=>'Users are already moderator of group']);
            }
        }
        }
        else
        {
            return response()->json(['status'=>FALSE,'msg'=>'group not found']);
        }
    }
    public function removeModerators(Request $request){
        $res = $this->validation($request,'user_id,group_id,moderator_id');
        if($res){
            return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        //////group details
        $group_datails=Group::where('id',$request->group_id)->first();
        ///group co admin list
        $coAdmin=GroupMember::select('user_id')->where('group_id',$request->group_id)->where('role','Moderator')->get();
        $coAdmin=$coAdmin->toArray();
        $coAdmin=array_column($coAdmin,'user_id');
        if($group_datails){
           
            if (!in_array((int)$request->user_id,$coAdmin) && $request->user_id!=$group_datails->user_id ){

                return response()->json(['status'=>FALSE,'msg'=>'You are not admin or co admin of this group'],400);
             }
             if(GroupMember::where('user_id',$request->moderator_id)->where('role','Moderator')->first())
             {
                GroupMember::where('user_id',$request->moderator_id)->where('role','Moderator')->update(['role'=>'Members']);
                return response()->json(['status'=>FALSE,'msg'=>config('constants')['SUCCESS1']]);
             }
             
             else
             { return response()->json(['status'=>FALSE,'msg'=>'User is not Moderator of this group'],400);}

        }
        else
        {
            return response()->json(['status'=>FALSE,'msg'=>'group not found']);
        }
    }
    public function reportGroupPost(Request $request)
    {
        $res = $this->validation($request,'group_id,post_id,report_by_id,post_user_id');
        if($res){
            return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }

        $response=Group_report::insert($request->all());
        if($response)
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['GROUP_REPORT_SUCCESS']]);
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
    }
    public function reportGroupList($id)
    {
        
        if(!$id){
            return response()->json(['status'=>FALSE,'msg'=>'group id is missing'],400);
        }

        $response=Group_report::where('group_id',$id)->
        with('posted_by:id,first_name,last_name,profile_image','reported_by:id,first_name,last_name,profile_image'
        ,'post')->get();
        if($response)
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1'],'data'=>$response],200, [], JSON_NUMERIC_CHECK);
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400);
    }
    public function makeGroupAdmin(Request $request)
    {
        $res = $this->validation($request,'group_id,user_id,make_admin_user_id');
        if($res){
            return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }

        $token=User::select('device_token')->where('id',$request->make_admin_user_id)->first(); 
        $group=Group::where('id',$request->group_id)->first(); 
        if($request->user_id!=$group->user_id)
        {
            return response()->json(['status'=>FALSE,'msg'=>'You are not group admin']);
        }
        if($token->device_token){
        $title='Become a Group Admin Request';
        $body='Become a Group Admin Request for ('.$group->title.')';
        $msg=array('title'=>$title,'body'=>$body,'sound'=>'default');
        $data=array('url'=>'','type'=>'Make_admin','id'=>$request->group_id);
        $fields=array('to'=>$token->device_token,'data'=>$data,'notification'=>$msg,'priority'=>'high','content_available'=>true);
         $this->send_messages_android($fields);
        }
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]);
    }
    public function responseMakeGroupAdmin(Request $request)
    {
        $res = $this->validation($request,'group_id,user_id,response');
        if($res){
            return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }

        if($request->response==1)
        {
            $group=Group::where('id',$request->group_id)->first(); 
            
            ///////update group admin
            Group::where('id',$request->group_id)->update(['user_id'=>$request->user_id]);
            GroupMember::where('group_id',$request->group_id)->where('user_id',$request->user_id)->update(['role'=>'Admin']);
            /////remove old admin
            GroupMember::where('group_id',$request->group_id)->where('user_id',$group->user_id)->update(['role'=>'Members']);
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]);

        }
        else
        {
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]);
        }

    }
    public function groupBlock(Request $request)
    {
        $res = $this->validation($request,'group_id,user_id');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        
            if(Group_blocked::where('user_id',$request->user_id)->where('group_id',$request->group_id)->count()>0)
            {
                return response()->json(['status'=>FALSE,'msg'=>config('constants')['ALREADY_BLOCK']],400);   
            }
            else
            {

                if(Group_blocked::insertGetId($request->all()) > 0){
                return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]);   
            }
            else
            {
                return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);   
            }
        }
        
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400);
    }
    public function groupUnBlock(Request $request)
    {
        $res = $this->validation($request,'user_id,group_id');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        
        $delete=Group_blocked::where('user_id',$request->user_id)->where('group_id',$request->group_id)->first();
        if($delete){ $delete->delete(); }
        if($delete)
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]); 
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400); 
        
    }
    public function groupblockList($id)
    {
        
        if(!$id){
        return response()->json(['status'=>FALSE,'msg'=>'group_id Missing'],400);
        }  
                 
            $list= Group_blocked::where('group_id',$id)->with(array('user_info'=>function($query){
            $query->select('id','full_name','profile_image');
        }))->get();
        
          // dd($list);
            if(count($list)>0){
                foreach($list as $raw){
                    $data[]=$raw->user_info;
                }
                
                return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1'],'data'=>$data],200, [], JSON_NUMERIC_CHECK);   
            }
            else
            {
                return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400); 
            }
        
        
    }
    public function groupMute(Request $request)
    {
        $res = $this->validation($request,'user_id,group_id');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        
            if(Group_mute::where('user_id',$request->user_id)->where('group_id',$request->group_id)->count()>0)
            {
                return response()->json(['status'=>FALSE,'msg'=>config('constants')['ALREADY_MUTED']],400);   
            }
            else
            {

                if(Group_mute::insertGetId($request->all()) > 0){
                return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]);   
            }
            else
            {
                return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);   
            }
       
            }
        
    }
    public function groupUnMute(Request $request)
    {
        $res = $this->validation($request,'user_id,group_id');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        
        $delete=Group_mute::where('user_id',$request->user_id)->where('group_id',$request->group_id)->first();
        if($delete){ $delete->delete(); }
        if($delete)
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]); 
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400); 
        
    }
    public function groupMuteList($id)
    {
        
        if(!$id){
        return response()->json(['status'=>FALSE,'msg'=>'group_id Missing'],400);
        }  
                 
            $list= Group_mute::where('group_id',$id)->with(array('user_info'=>function($query){
            $query->select('id','full_name','profile_image');
        }))->get();
        
          // dd($list);
            if(count($list)>0){
                foreach($list as $raw){
                    $data[]=$raw->user_info;
                }
                
                return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1'],'data'=>$data],200, [], JSON_NUMERIC_CHECK);   
            }
            else
            {
                return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400); 
            }
        
        
    }
    public function GroupMedia($id)
    {
       if(Group::where('id',$id)->first())
       {
         $data=GroupMedia::select('group_post_id','group_media','media_type')->where('group_id',$id)->get();
         return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1'],'data'=>$data],200, [], JSON_NUMERIC_CHECK);
       }
       else
       {
        return response()->json(['status'=>FALSE,'msg'=>'Invalid Group ID'],400);
       }
    }
    
/********Group post******/
    public function groupSearch(Request $request)
    {
        $data=Group::where('title','Like','%'.$request->search_string.'%')->where('group_privacy','public')->get();
        if(count($data)>0)
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1'],'data'=>$data]); 
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']]); 
    }
    public function myGroups($id)
    {
      if($id)
      {
        $data=GroupMember::where('user_id',$id)->where('status','Joined')->with('group_details')->get(); 
        if(count($data)>0)
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1'],'data'=>$data]); 
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']]); 
      }
      
    }
    public function addGroupPost(Request $request)
    {   

        $res = $this->validation($request,'user_id,group_id,post_type');
        if($res){
            return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        
        //////group details
        $group_datails=Group::where('id',$request->group_id)->first();
        
        if($group_datails)
        {
        /////check posting_permission /////   
        if($group_datails->posting_permission==1){
                        
            ////check user is group member or not
            $coAdmin=GroupMember::select('user_id')->where('group_id',$request->group_id)->where('status','Joined')->get();
            $coAdmin=$coAdmin->toArray();
            $coAdmin=array_column($coAdmin,'user_id');
            if (!in_array((int)$request->user_id,$coAdmin)){
                return response()->json(['status'=>FALSE,'msg'=>'You are not member of this group']);
            }
            
        $post_id = GroupPost::insertGetId([
                'user_id'=>$request->user_id,
                'group_id'=>$request->group_id,
                'type'=>$request->post_type,
                'location'=>$request->location,
                'post_text'=>$request->post_text
            ]);
        if($request->post_type=='media')
        {
            if(!$request->media && $post_id>0){
                return response()->json(['status'=>FALSE,'msg'=>'Media is missing'],400);
            } else {
                $media=json_decode($request->media);
                foreach($media as $raw)
                {
                    $media_array[]=array(
                        'group_id'=>$request->group_id,
                        'media_type'=>$raw->type,
                        'group_media'=>$raw->media,
                        'group_post_id'=>$post_id
                    );
                }
                GroupMedia::insert($media_array);
            }
        }
        if($post_id){
            
            $join_user=DB::select("SELECT GROUP_CONCAT(users.id) as users_id ,groups.title,GROUP_CONCAT(users.device_token) as device_token  FROM `group_members`
            LEFT JOIN groups on groups.id=group_members.group_id
            LEFT JOIN users on users.id=`group_members`.user_id 
            where group_members.group_id='$request->group_id' and group_members.status='Joined'");
           //dd($join_user[0]->title_of_event);
           if($join_user){
            $postUserFullName=User::select('full_name')->where('id',$request->user_id)->first(); 
            $usersId=explode(',',$join_user[0]->users_id);
                foreach($usersId as $raw){
                $notification[]=array(
                'user_id'=>$raw,
                'type'=>'Group_post',
                'post_id'=>$post_id,
                'msg'=>@$postUserFullName->full_name.' made a post in Group-'.$join_user[0]->title
            );
            }
            Notification::insert($notification);
            
            $user=explode(',',$join_user[0]->device_token);
            
            $msg=array('title'=>$join_user[0]->title,'body'=>@$postUserFullName->full_name.' made a post in Group-'.$join_user[0]->title,'sound'=>'default');
            $data=array('url'=>'','type'=>'Event_post','post_type'=>'Group_post','id'=>$post_id);
            $fields=array('registration_ids'=>$user,'data'=>$data,'notification'=>$msg,'priority'=>'high','content_available'=>true);
            $this->send_messages_android($fields);
           }

            return response()->json(['status'=>TRUE,'msg'=>config('constants')['POST_ADD_SUCCESS']]); 
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
        }
        else
        {
            return response()->json(['status'=>FALSE,'msg'=>'Only Admin can post in this Group'],400);   
        }
        }
        else
        {
            return response()->json(['status'=>FALSE,'msg'=>'invalid group id'],400);   
        }
    }
    public function groupPosts(Request $request)
    {
        
        $user_array=array();
        $user_id=$request->user_id;
        //DB::enableQueryLog();dd(DB::getQueryLog());
        $group_posts = GroupPost::where('group_id',$request->group_id)->withCount('like_count','comment_count','share_count')->with(['is_like'=> function ($query) use ($user_id) {
            $query->where('user_id', $user_id);
        },'user_info','group_post_media'])->orderBy('created_at', 'desc')->paginate(10, ['*'], '', $request->page);
        //dd(DB::getQueryLog());
        //dd($group_posts);
        if(count($group_posts)>0) {
            foreach($group_posts as $raw)
            { 
               
                if (count($raw->is_like)>0)
                $is_liked=1;
                else
                $is_liked=0;

                $media=array();
                if($raw->type=='media')
                {
                    foreach($raw->group_post_media as $media_raw)
                    {
                        $media[]=array('media_type'=>$media_raw->media_type,'media_url'=>$media_raw->group_media);  
                    }
                }
                
                
                $user_array[]=array(
                    "post_id"=>$raw->id,
                    "post_type"=>'Group_post',
                    "user_id"=>$raw->user_id,
                    "post_by"=>@$raw->user_info->full_name,
                    "profile_image"=>@$raw->user_info->profile_image,
                    "date_of_post"=>date('d-m-Y',strtotime($raw->created_at)),
                    "time_of_post"=>date('h:i:s A',strtotime($raw->created_at)),
                    "location_of_post"=>$raw->location,
                    "text_message"=>$raw->post_text,
                    "media"=>$media,
                    "total_number_of_likes"=>$raw->like_count_count,
                    "total_number_of_comments"=>$raw->comment_count_count,
                    "total_number_of_share"=>$raw->share_count_count,
                    "is_liked"=>$is_liked
                    ); 
                
            }
        }
        if(count($user_array)>0)
        return response()->json(['status'=>TRUE,"msg"=>config('constants')['SUCCESS'],'data'=>$user_array],200, [], JSON_NUMERIC_CHECK);
        else
        return response()->json(['status'=>TRUE,"msg"=>config('constants')['NOT_FOUND'],'data'=>[]]);
    }
    public function groupPostAllLike(Request $request)
    {
        $post=GroupPost::where('id',$request->post_id)->first();  
        if($post) {
        $user_id=$post->user_id;

        $all_like=Group_meta::where('post_id',$request->post_id)->where('type','Like')->with(['like_user_info','is_friend' => function ($q) use($user_id) {
            $q->where('user_id','=',$user_id);
            }
            ])->get();
        //dd($all_like);
        if(count($all_like)>0)
        {
            foreach($all_like as $raw)
            {
                    if($raw->is_friend)
                    $is_friend=1;
                    else
                    $is_friend=0;
                    $data[]=array(
                    "user_id"=>$raw->like_user_info->id,
                    "full_name"=>$raw->like_user_info->full_name,
                    "profile_pic"=>$raw->like_user_info->profile_image,
                    "color"=>$raw->like_user_info->user_title,
                    "is_friend"=>$is_friend );
            }
            return response()->json(['status'=>TRUE,"msg"=>config('constants')['SUCCESS'],'data'=>$data],200, [], JSON_NUMERIC_CHECK);
        }
        return response()->json(['status'=>FALSE,"msg"=>config('constants')['NO_LIKE_POST']],400);
        }
        else
        {
            return response()->json(['status'=>FALSE,"msg"=>'Invalid Post Id'],400);
        }
    }
    public function groupPostLikeDislike(Request $request)
    {   
        $res = $this->validation($request,'loggedin_user_id,post_user_id,post_id,current_status');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        $group=GroupPost::select('id')->where('id',$request->post_id)->first(['id']);
        
        if(@$group->id){
        if($request->current_status==0)
        {
         $like=Group_meta::where('user_id',$request->loggedin_user_id)->where('post_id',$request->post_id)->where('type','Like')->first();
         if($like){ $like->delete(); }
        }
        else
        {
            $user_info=User::select('full_name')->where('id',$request->loggedin_user_id)->first();
            $friend_info=User::select('device_token')->where('id',$request->post_user_id)->first();
            Notification::insert(['user_id'=>$request->post_user_id,'type'=>'Group_post','post_id'=>$request->post_id,'msg'=>@$user_info->full_name.' Like Your Post']);
            if($friend_info->device_token){
                $msg=array('title'=>'New Like On Post','body'=>@$user_info->full_name.' Like Your Post','sound'=>'default');
                $data=array('url'=>'','type'=>'post_like','post_type'=>'Group_post','id'=>$request->post_id);
                $fields=array('to'=>$friend_info->device_token,'data'=>$data,'notification'=>$msg,'priority'=>'high','content_available'=>true);
                $this->send_messages_android($fields);
                }
         $like=Group_meta::updateOrCreate(['type'=>'Like','post_id'=>$request->post_id,'user_id'=>$request->loggedin_user_id],['type'=>'Like','post_user_id'=>$request->post_user_id,'user_id'=>$request->loggedin_user_id,'post_id'=>$request->post_id]);
        }
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]); 
        }
        else
        {
        return response()->json(['status'=>FALSE,'msg'=>'Invalid Post Id']);    
        }
    }
    public function groupPostComments(Request $request)
    {
        $user_id=$request->user_id;
        $comments=Group_meta::where('type','Comment')->where('post_id',$request->post_id)->with(['is_like'=> function ($query) use ($user_id) {
            $query->where('user_id', $user_id);
        },'comment_like','comment_reply','comment_user_info'])->get();
        
        if(count($comments)>0)
        {
            foreach($comments as $raw)
            {       if($raw->is_like)
                    $is_like=1;
                    else
                    $is_like=0;
                    $data[]=array(
                    "user_id"=>$raw->like_user_info->id,
                    "profile_pic"=>$raw->like_user_info->profile_image,
                    "user_full_name"=>$raw->like_user_info->full_name,
                    "comment_id"=>$raw->id,
                    "comment_text"=>$raw->comment_text,
                    "is_like"=>$is_like,
                    "like_count"=>count($raw->comment_like),
                    'comment_reply'=>$raw->comment_reply
                );
            }
            return response()->json(['status'=>TRUE,"msg"=>config('constants')['SUCCESS'],'data'=>$data],200, [], JSON_NUMERIC_CHECK);
        }
        return response()->json(['status'=>FALSE,"msg"=>config('constants')['NO_POST_COMMENTS']],400);
    }
    public function groupPostAddComment(Request $request)
    {
        $res = $this->validation($request,'loggedin_user_id,post_user_id,post_id,comment_text');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        $request->merge(['user_id'=>$request->loggedin_user_id,'type'=>'Comment']);
        $comment=Group_meta::insert($request->except(['loggedin_user_id','post_type']));
        if($comment){
            $user_info=User::select('full_name')->where('id',$request->loggedin_user_id)->first();
            $friend_info=User::select('device_token')->where('id',$request->post_user_id)->first();
            Notification::insert(['user_id'=>$request->post_user_id,'type'=>'Group_post','post_id'=>$request->post_id,'msg'=>@$user_info->full_name.' Comment on Your Post']);
            if($friend_info->device_token){
                $msg=array('title'=>'New Comment On Post','body'=>@$user_info->full_name.' Comment on Your Post','sound'=>'default');
                $data=array('url'=>'','type'=>'post_comment','post_type'=>'Group_post','id'=>$request->post_id);
                $fields=array('to'=>$friend_info->device_token,'data'=>$data,'notification'=>$msg,'priority'=>'high','content_available'=>true);
                $this->send_messages_android($fields);
                }
                return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]); 
        }
        else
        {return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400); }
        
        
    }
    public function groupPostEditComment(Request $request)
    {
        $res = $this->validation($request,'comment_id,comment_edited_text');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        $request->merge(['comment_text'=>$request->comment_edited_text]);
        $comment=Group_meta::where('id',$request->comment_id)->update($request->except(['comment_edited_text','comment_id','post_type']));
        if($comment)
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]); 
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400); 
        
    }
    public function groupPostDeleteComment(Request $request)
    {
        $res = $this->validation($request,'comment_id,loggedin_user_id');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        
        $comment=Group_meta::where('user_id',$request->loggedin_user_id)->where('id',$request->comment_id)->first();
        if($comment){ $comment->delete(); }
        if($comment)
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]); 
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400); 
        
    }
    public function groupCommentLikeDislike(Request $request)
    {
        $res = $this->validation($request,'user_id,comment_id');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        if($request->current_status=='0')
        {
         $like=Group_comment_meta::where('user_id',$request->user_id)->where('comment_id',$request->comment_id)->where('type','Like')->first();
         if($like){ $like->delete(); }
        }
        else
        {
            $post_id=Group_meta::select('post_user_id','post_id')->where('id',$request->comment_id)->first();
            $user_info=User::select('full_name')->where('id',$request->user_id)->first();
            $friend_info=User::select('device_token')->where('id',$post_id->post_user_id)->first();
            Notification::insert(['user_id'=>$post_id->post_user_id,'comment_id'=>$request->comment_id,'type'=>'Group_post','post_id'=>@$post_id->post_id,'msg'=>@$user_info->full_name.' Like your Comment']);
            if($friend_info->device_token){
                $msg=array('title'=>'New Comment Like','body'=>@$user_info->full_name.' Like your Comment','sound'=>'default');
                $data=array('url'=>'','type'=>'post_comment_like','post_type'=>'Group_post','id'=>@$post_id->post_id);
                $fields=array('to'=>$friend_info->device_token,'data'=>$data,'notification'=>$msg,'priority'=>'high','content_available'=>true);
                $this->send_messages_android($fields);
                }
         $like=Group_comment_meta::updateOrCreate(['type'=>'Like','comment_id'=>$request->comment_id,'user_id'=>$request->user_id],['type'=>'Like','comment_id'=>$request->comment_id,'user_id'=>$request->user_id]);
        }
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]);
    }
    public function groupCommentReply(Request $request)
    {
        $res = $this->validation($request,'user_id,comment_id,text');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
            
        Group_comment_meta::insert(['type'=>'Reply','comment_id'=>$request->comment_id,'user_id'=>$request->user_id,'text'=>$request->text]);
        $post_id=Group_meta::select('post_user_id','post_id')->where('id',$request->comment_id)->first();
        $user_info=User::select('full_name')->where('id',$request->user_id)->first();
        $friend_info=User::select('device_token')->where('id',$post_id->post_user_id)->first();
        Notification::insert(['user_id'=>$post_id->post_user_id,'type'=>'Group_post','post_id'=>@$post_id->post_id, 'comment_id'=>$request->comment_id,'msg'=>@$user_info->full_name.' Reply on your Comment']);
        if($friend_info->device_token){
            $msg=array('title'=>'New Comment Reply','body'=>@$user_info->full_name.' Reply on your Comment','sound'=>'default');
            $data=array('url'=>'','type'=>'post_comment_reply','post_type'=>'Group_post','id'=>@$post_id->post_id,'comment_id'=>$request->comment_id,'name'=>@$user_info->full_name,'reply'=>$request->text);
            $fields=array('to'=>$friend_info->device_token,'data'=>$data,'notification'=>$msg,'priority'=>'high','content_available'=>true);
            $this->send_messages_android($fields);
            }
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]);
    }
    public function groupDeletePost(Request $request)
    {
        $res = $this->validation($request,'post_id,user_id');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        
        $post_details=GroupPost::where('user_id',$request->user_id)->where('id',$request->post_id)->first();
        if($post_details){ $post_details->delete(); }
        if($post_details)
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]); 
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400); 
        
    }
    public function groupPostShare(Request $request)
    {
        $res = $this->validation($request,'post_id,user_id,post_user_id');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        $request->merge(['type'=>'Share']);
        //$share=Post::insert($request->all());
        $share=Group_meta::insert($request->except(['post_type']));
        if($share)
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]); 
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400); 
        
    }
/*----------------*/    
/********** End Groups Functions **********/

/***********community function Start ********/

    public function addCommunity(Request $request)
    {   
        $res = $this->validation($request,'name');
        if($res){
            return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        //Check group already exist
        if(Group::where('name',$request->name)->where('type','2')->count()){
            return response()->json(['status'=>FALSE,'msg'=>config('constants')['COMMUNITY_ALREADY_EXIST']],400);
         }
         $request->merge(['type' => '2']);
        //Insert Group Details
        $lastGroupId = Group::insertGetId($request->all());
        if($lastGroupId){
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['COMMUNITY_ADD']]);
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
    }

    public function addCommunityMember(Request $request)
    {
        $res = $this->validation($request,'community_id,user_ids');
        if($res){
            return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        
        $array = explode(',', $request->user_ids);
        $members = [];
        ///////delete old members if any/////////////
        GroupMember::whereIn('user_id',$array)->where('group_id',$request->community_id)->delete();
        ////////////////////////////////////////////
        foreach ($array as $key => $value) {
            if($request->$value==''){
                $members[]= ['group_id'=>$request->community_id,'user_id'=>$value];
            }
        }
        $rec = GroupMember::insert($members);
        if($rec){
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['COMMUNITY_MEMBER_ADD']]);
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
    }

    public function getCommunityById($id)
    {   
        $group_members=array();
        $group_post=array();
        $result=Group::whereId($id)->with('group_member','group_post')->first();
        //dd($result);
        if(count($result)>0)
        {
          foreach($result->group_member as $raw)
          {
            $group_members[]=array(
                'user_id'=>$raw->member_details->id,
                'name'=>$raw->member_details->full_name,
                'image'=>$raw->member_details->profile_image
            );
          }
          foreach($result->group_post as $raw)
          {
              
            $post_media=array();
            foreach($raw->group_post_media as $media)
            {
                $post_media[]=array('media_id'=>$media->id,'url'=>$media->group_media,'media_type'=>$media->media_type);
            }  
            $group_post[]=array(
                'post_id'=>$raw->id,
                'type'=>$raw->type,
                'location'=>$raw->location,
                'post_text'=>$raw->post_text,
                'created_at'=>$raw->created_at,
                'post_media'=>$post_media
         
            );
          }
             
          $data[]=array(
              'id'=>$result->id,
              'community_name'=>$result->name,
              'image'=>$result->image,
              'privacy'=>$result->privacy,
              'description'=>$result->description,
              'tags'=>$result->tags,
              'membership_approval'=>$result->membership_approval,
              'posting_permission'=>$result->posting_permission,
              'post_approval'=>$result->post_approval,
              'group_category_id'=>$result->group_category_id,
              'community_members'=>$group_members,
              'community_post'=>$group_post
          );
          
          return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS'],'data'=>$data]);
        }
        else
        {
            return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400);
        }  
    }
    public function addCommunityPost(Request $request)
    {   
        $post_id = GroupPost::insertGetId([
                'user_id'=>$request->user_id,
                'group_id'=>$request->community_id,
                'type'=>$request->post_type,
                'location'=>$request->location,
                'post_text'=>$request->post_text
            ]);
        if($request->post_type=='media')
        {
            if(!$request->media && $post_id>0){
                return response()->json(['status'=>FALSE,'msg'=>'Media is missing'],400);
            } else {
                $media=json_decode($request->media);
                foreach($media as $raw)
                {
                    $media_array[]=array(
                        'group_id'=>$request->community_id,
                        'media_type'=>$raw->type,
                        'group_media'=>$raw->media,
                        'group_post_id'=>$post_id
                    );
                }
                GroupMedia::insert($media_array);
            }
        }
        if($post_id){
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['POST_ADD_SUCCESS']]); 
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
       
    }
/*************community function End**** */

/********** Start Jobs Functions **********/

    public function addJob(Request $request)
    {   
        $res = $this->validation($request,'user_id,category_id,title,description,location,company_name,exp_min,exp_max,employement_type,seniority_level,notify_me');
        if($res){
            return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        if($request->notify_me==2 && $request->email=='')
        {
            return response()->json(['status'=>FALSE,'msg'=>'Notify Email is Required'],400);
        }

        $education =json_decode($request->job_education);
        if(!$education)
        {
            return response()->json(['status'=>FALSE,'msg'=>'Education Required'],400);
        }
        
        $today = date('Y-m-d H:i:s');
        //$request->merge(['start_date'=>$today, 'end_date'=>date('Y-m-d H:i:s',strtotime($today ."+ 30 days"))]);
        $lastId = JobPost::insertGetId($request->except(['job_education']));
        if($lastId){
            if($request->job_education){
                
                foreach($education as $raw)
                {
                    $edu_array[]=[
                        'job_id'=>$lastId,
                        'degree_id'=>$raw->degree_id,
                    ];
                }
                JobEducation::insert($edu_array);
            }
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['JOB_ADD_SUCCESS']]);
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
    }
    public function updateJob(Request $request)
    {   
        $res = $this->validation($request,'id,user_id,category_id,title,description,location,company_name,exp_min,exp_max,employement_type,seniority_level,notify_me');
        if($res){
            return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        if($request->notify_me==2 && $request->email=='')
        {
            return response()->json(['status'=>FALSE,'msg'=>'Notify Email is Required'],400);
        }
   

        $education =json_decode($request->job_education);
        if(JobPost::where('id',$request->id)->first()){
        $response=JobPost::where('id',$request->id)->update($request->except(['id','job_education']));
        
            if($request->job_education){
                JobEducation::where('job_id',$request->id)->delete();
                $education =json_decode($request->job_education);
                foreach($education as $raw)
                {
                    $edu_array[]=[
                        'job_id'=>$request->id,
                        'degree_id'=>$raw->degree_id,
                    ];
                }
                JobEducation::insert($edu_array);
            }

            $saved_user=DB::select("SELECT GROUP_CONCAT(users.id) as users_id,job_posts.company_name,GROUP_CONCAT(users.device_token) as device_token  
            FROM `job_posts`
            RIGHT JOIN job_saved on job_saved.job_id=job_posts.id
            LEFT JOIN users on users.id=job_saved.user_id 
            where job_posts.id=$request->id");
                    
           if($saved_user){
            /***********Add Notification**********/   
            $usersId=explode(',',$saved_user[0]->users_id);
                foreach($usersId as $raw){
                $notification[]=array(
                'user_id'=>$raw,
                'type'=>'Job_detail',
                'post_id'=>$request->id,
                'msg'=>@$saved_user[0]->company_name.' has updated the job info'
                );
                }
                Notification::insert($notification);

            /***********Send Push Notification**********/    
            $user=explode(',',$saved_user[0]->device_token);
            
            $msg=array('title'=>$saved_user[0]->company_name,'body'=>$saved_user[0]->company_name.' has updated the job info','sound'=>'default');
            $data=array('url'=>'','type'=>'Job_info_update','post_type'=>'Job_detail','id'=>$request->id);
            $fields=array('registration_ids'=>$user,'data'=>$data,'notification'=>$msg,'priority'=>'high','content_available'=>true);
            $this->send_messages_android($fields);
            }
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['JOB_UPDATE_SUCCESS']]);
        }
        else
        {
            return response()->json(['status'=>FALSE,'msg'=>'Job Not Found'],400);   
        }
       
    }
    public function getAllJobByUser(Request $request)
    {
        $jobs = JobPost::select('job_posts.*',DB::raw('DATE_FORMAT(job_posts.created_at, "%d-%b-%Y") as posted_on'))->with('job_category','job_view_count','job_employment_type')->where('user_id',$request->user_id)->get();
        
        if(count($jobs)>0)
        {
         foreach($jobs as $raw)
         {
             $data[]=array(
                 'id'=>$raw->id,
                 'title'=>$raw->title,
                 'description'=>$raw->description,
                 'location'=>$raw->location,
                 'image'=>$raw->image,
                 'company_name'=>$raw->company_name,
                 'job_functions'=>$raw->job_functions,
                 'status'=>(int)$raw->status,
                 'posted_on'=>$raw->posted_on,
                 'employement_type'=>$raw->job_employment_type,
                 'job_category'=>$raw->job_category,
                 'total_view'=>@$raw->job_view_count->count(),
             );
         }
         return response()->json(['status'=>TRUE,'msg'=>'','data'=>$data],200, [], JSON_NUMERIC_CHECK);
        }
        else
        {return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400);}
    }

    public function getJobById(Request $request)
    {
        $job_details = JobPost::with('owner:id,full_name,profile_image','job_category','job_education','job_employment_type','job_seniority_level')->where('id',$request->id)->first();
        if($job_details)
        {
            ///job View Counter Work
            $check=Job_count::where('user_id',$request->user_id)->where('job_id',$request->id)->first();
            if(!$check)
            {
             $response=Job_count::insertGetId(['job_id'=>$request->id,'user_id'=>$request->user_id]);
            }
        return response()->json(['status'=>TRUE,'msg'=>'','data'=>$job_details],200, [], JSON_NUMERIC_CHECK);}
        else
        {return response()->json(['status'=>FALSE,'msg'=>config('constants')['INVALID_JOB_ID']],400);}

    }

    public function pauseJob($job_id)
    {
        if(JobPost::where('id',$job_id)->update(['status'=>0])){
            return response()->json(['status'=>TRUE,'msg'=>'Job Pause Successfully.']);
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
    }

    public function unPauseJob($job_id)
    {
        if(JobPost::where('id',$job_id)->update(['status'=>1])){
            return response()->json(['status'=>TRUE,'msg'=>'Job Un-Pause Successfully.']);
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
    }

    public function deleteJob($job_id)
    {
        if(JobPost::where('id',$job_id)->delete()){
            return response()->json(['status'=>TRUE,'msg'=>'Job Delete Successfully.']);
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
    }
    public function applyJob(Request $request)
    {
        
        $res = $this->validation($request,'user_id,job_id,email,contact_no,resume');
        if($res){
            return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        ////check if already applyed/////////
        if(!JobApplyUser::where('user_id',$request->user_id)->where('job_id',$request->job_id)->first()){
        $lastId = JobApplyUser::insertGetId(['job_id'=>$request->job_id,'user_id'=>$request->user_id]);
        if($lastId){
            
            $user_info=User::select('full_name')->where('id',$request->user_id)->first();
            $job_info=JobPost::where('id',$request->job_id)->with('owner')->first();
             
            if($request->user_id!=$job_info->user_id){
            Notification::insert(['user_id'=>$job_info->owner->id,'type'=>'Job_detail','post_id'=>$request->job_id,'msg'=>@$user_info->full_name.' has apply on your job']);
            if(@$job_info->owner->device_token){
                $msg=array('title'=>'New apply on job ','body'=>@$user_info->full_name.' has apply on your job ','sound'=>'default');
                $data=array('url'=>'','type'=>'Job_apply','post_type'=>'Job_detail','id'=>$request->job_id);
                $fields=array('to'=>$job_info->owner->device_token,'data'=>$data,'notification'=>$msg,'priority'=>'high','content_available'=>true);
                $this->send_messages_android($fields);
                }
            }
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['JOB_APPLY_SUCCESS']]);
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
        }
        else
        {
        return response()->json(['status'=>FALSE,'msg'=>'You have already applyed for this Job'],400);
        } 
    }
    public function jobSearch(Request $request)
    {
        $date = date('Y-m-d');
        $date1 = str_replace('-', '/', $date);
        
        $id=$request->user_id;
        if(!$id){
            return response()->json(['status'=>FALSE,'msg'=>'user_id Required'],400);
        }
        
        //DB::enableQueryLog();
            $query = JobPost::with(['owner','job_employment_type','job_category','job_saved' => function ($q) use($id) {
                $q->where('user_id','=',$id);
                }
                ]);
                $query->where('status','1');
            if($request->search_string!='')
            {
                $query->where('title','like','%'.$request->search_string.'%');
            }
            if($request->location!='')
            {
                $query->where('location',$request->location);
            }
            if($request->short_type!='')
            {
                if($request->short_type==0)
                { $query->orderBy('id', 'ASC'); }
                if($request->short_type==1)
                { $query->orderBy('id', 'DESC'); }
                
            }
            else
            {
                $query->orderBy('id','DESC');
            }
            if($request->category_id!='')
            {
                $query->where('category_id',$request->category_id);
            }
            if($request->employement_type!='')
            {
                $query->where('employement_type',$request->employement_type);
            }
                
    
            $result= $query->get();
            //dd(DB::getQueryLog());
            if(count($result)>0)
            {
                foreach($result as $raw)
                {
                    $date=date('Y-m-d H:i:s',strtotime($raw->created_at));
                    $now = time(); // or your date as well
                    $your_date = strtotime($raw->created_at);
                    $datediff = $now - $your_date;
    
                    $day_ago= round($datediff / (60 * 60 * 24));
                    if($day_ago==0)
                    $day_ago='New';
                    else
                    $day_ago=$day_ago.' Days';
    
                    if(@$raw->job_saved[0]['user_id']==$request->user_id)
                    $is_saved=1;
                    else
                    $is_saved=0;

                    $data[]=array(
                        'id'=>$raw->id,
                        'posted_by'=>$raw->owner->full_name,
                        'posted_by_image'=>$raw->owner->profile_image,
                        'title'=>$raw->title,
                        'location'=>$raw->location,
                        'company_name'=>@$raw->company_name,
                        'description'=>$raw->description,
                        'days_ago'=>$day_ago,
                        'image'=>$raw->image,
                        'is_saved'=>$is_saved,
                        'posted_on'=>date('d-M-Y',strtotime($raw->created_at)),
                        'employment_type'=>$raw->job_employment_type,
                        'job_category'=>$raw->job_category,

                       );
                }
                return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS'],'data'=>$data]);
            }
            else
           { return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400);}
                
    }

    public function jobSaved($id)
    {
     $jobSaved=Job_saved::where('user_id',$id)->with('job_info')->get();
     $data=array();
     if(count($jobSaved)>0)
     {
         foreach($jobSaved as $raw){
            if($raw->job_info){ 
            $date=date('Y-m-d H:i:s',strtotime($raw->created_at));
            $now = time(); // or your date as well
            $your_date = strtotime($raw->created_at);
            $datediff = $now - $your_date;

            $day_ago= round($datediff / (60 * 60 * 24));
            if($day_ago==0)
            $day_ago='Today';
            else
            $day_ago=$day_ago.' Days';


            $data[]=array(
            'id'=>$raw->id,
            'job_id'=>@$raw->job_info->id,
            'title'=>@$raw->job_info->title,
            'company_name'=>@$raw->job_info->company_name,
            'image'=>@$raw->job_info->image,
            'location'=>@$raw->job_info->location,
            'day_ago'=>$day_ago,
            'posted_on'=>date('d-M-Y',strtotime($raw->created_at)),
            'employment_type'=>$raw->job_info->job_employment_type,
            'job_category'=>$raw->job_info->job_category,
            );
        }
         }
         if(count($data)>0)
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS'],'data'=>$data]);
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400);
     }
     
     else
     {return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400);}

    }
    public function jobRemoveSaved(Request $request)
    {
        $res = $this->validation($request,'user_id,id');
        if($res){
            return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
    
        if(Job_saved::where('user_id',$request->user_id)->where('job_id',$request->id)->delete())
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]);
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
    }
    public function addSavedJob(Request $request)
    {
    
        $res = $this->validation($request,'user_id,job_id');
        if($res){
            return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
                  
        if(!Job_saved::where('job_id',$request->job_id)->where('user_id',$request->user_id)->first()){
        $JobPost=JobPost::where('id',$request->job_id)->first();
        if($JobPost){
        $response=Job_saved::insertGetId(['job_post_user_id'=>$JobPost->user_id,'job_id'=>$request->job_id,'user_id'=>$request->user_id]);
        if($response)
        {return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]);}
        else
        {return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);}
        }
        else
        {
            return response()->json(['status'=>FALSE,'msg'=>'Invalid Job ID'],400);
        }
        }
        else
        {
            return response()->json(['status'=>FALSE,'msg'=>'Job is Already Saved'],400);   
        }
    }

    public function jobListing(Request $request)
    {
        $date = date('Y-m-d');
        $date1 = str_replace('-', '/', $date);
        
        $id=$request->user_id;
        if(!$id){
            return response()->json(['status'=>FALSE,'msg'=>'user_id Required'],400);
        }
        
        //DB::enableQueryLog();
            $query = JobPost::with(['owner','job_employment_type','job_category','job_saved' => function ($q) use($id) {
                $q->where('user_id','=',$id);
                }
                ])->orderBy('created_at', 'desc')->paginate(5, ['*'], '', $request->page);;
                if(count($query)>0)
            {
                foreach($query as $raw)
                {
                    $date=date('Y-m-d H:i:s',strtotime($raw->created_at));
                    $now = time(); // or your date as well
                    $your_date = strtotime($raw->created_at);
                    $datediff = $now - $your_date;
    
                    $day_ago= round($datediff / (60 * 60 * 24));
                    if($day_ago==0)
                    $day_ago='New';
                    else
                    $day_ago=$day_ago.' Days';
    
                    if(@$raw->job_saved[0]['user_id']==$request->user_id)
                    $is_saved=1;
                    else
                    $is_saved=0;

                    $data[]=array(
                        'id'=>$raw->id,
                        'posted_by'=>$raw->owner->full_name,
                        'posted_by_image'=>$raw->owner->profile_image,
                        'title'=>$raw->title,
                        'location'=>$raw->location,
                        'company_name'=>@$raw->company_name,
                        'description'=>$raw->description,
                        'days_ago'=>$day_ago,
                        'image'=>$raw->image,
                        'is_saved'=>$is_saved,
                        'posted_on'=>date('d-M-Y',strtotime($raw->created_at)),
                        'employment_type'=>$raw->job_employment_type,
                        'job_category'=>$raw->job_category,

                       );
                }
                return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS'],'data'=>$data]);
            }
            else
           { return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400);}
    }

/********** End Jobs Functions **********/

/******************* Market Place Function  **************************/
    public function marketPlaceSaveRecent(Request $request)
    {
        $marketplace=Marketplace_saved::where('user_id',$request->user_id)->with('product_single_image','user_info','marketplace_info')->get();
        //dd($marketplace);
        if(count($marketplace)>0){
            foreach($marketplace as $raw){
                $date=date('Y-m-d H:i:s',strtotime($raw->created_at));
                $now = time(); // or your date as well
                $your_date = strtotime($raw->created_at);
                $datediff = $now - $your_date;

                $day_ago= round($datediff / (60 * 60 * 24));
                if($day_ago==0)
                $day_ago='Today';
                else
                $day_ago=$day_ago.' Days';
            $data[]=array(
                'id'=>$raw->id,
                'title'=>$raw->marketplace_info->product_title,
            'product_id'=>$raw->marketplace_id,
            'product_image'=>@$raw->product_single_image->url,
            'user_name'=>@$raw->user_info->full_name,
            'location'=>@$raw->marketplace_info->full_address,
            'price'=>@$raw->marketplace_info->price,
            'day_ago'=>$day_ago,
            );
            }
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS'],'data'=>$data]);
        }
        
        else
        {return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400);}

    }
    public function marketplaceRemoveSaved(Request $request)
    {
        $res = $this->validation($request,'user_id,id');
        if($res){
            return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }

        if(Marketplace_saved::where('user_id',$request->user_id)->where('marketplace_id',$request->id)->delete())
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]);
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
    }
    public function addSavedProduct(Request $request)
    {

        $res = $this->validation($request,'user_id,marketplace_id');
        if($res){
            return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }

        if(!Marketplace_saved::where('marketplace_id',$request->marketplace_id)->where('user_id',$request->user_id)->first()){
        $marketplace=Marketplace::where('id',$request->marketplace_id)->first();
        if($marketplace){
        $response=Marketplace_saved::insertGetId(['marketplace_user_id'=>$marketplace->user_id,'marketplace_id'=>$request->marketplace_id,'user_id'=>$request->user_id]);
        if($response)
        {return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]);}
        else
        {return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);}
        }
        else
        {
            return response()->json(['status'=>FALSE,'msg'=>'Invalid marketplace_id'],400);
        }
        }
        else
        {
            return response()->json(['status'=>FALSE,'msg'=>'Product Already Saved'],400);
        }
    }
    public function addMarketplaceProduct(Request $request)
    {

        $res = $this->validation($request,'user_id,product_title,market_place_category_id,price,location,description,full_address,country_code,mobile_number,email');
        if($res){
            return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }

        $response=Marketplace::insertGetId($request->except(['image']));
        if($response)
        {
            $images=json_decode($request->image);
            if($images)
            {
                foreach($images as $raw){
                Marketplace_gallery::insertGetId(['product_id'=>$response,'url'=>$raw->img]);
                }
            }

            return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]);}
        else
        {return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);}
    }
    public function editMarketplaceProduct(Request $request)
    {

        $res = $this->validation($request,'id,product_title,market_place_category_id,price,location,description,full_address,country_code,mobile_number,email');
        if($res){
            return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }

        $response=Marketplace::where('id',$request->id)->update($request->except(['image']));
        if($response)
        {
            $images=json_decode($request->image);
            if($images)
            {
                ///delete old images
                if(Marketplace_gallery::where('product_id',$request->id)->delete())
                {

                }
                foreach($images as $raw){
                Marketplace_gallery::insertGetId(['product_id'=>$response,'url'=>$raw->img]);
                }
            }

            return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]);}
        else
        {return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);}
    }
    public function deleteMarketplaceProduct(Request $request)
    {

        $res = $this->validation($request,'user_id,product_id');
        if($res){
            return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }

        if(Marketplace::where('user_id',$request->user_id)->where('id',$request->product_id)->delete())
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]);
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
    }
    public function marketPlaceCategory()
    {
        $category=Marketplace_category::all();
        if(count($category)>0)
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS'],'data'=>$category]);
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400);

    }
    public function marketPlace()
    {
        $marketplace=Marketplace::with('product_single_image')->get();
        if(count($marketplace)>0)
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS'],'data'=>$marketplace]);
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400);

    }
    public function myMarketPlace($id)
    {
        $marketplace=Marketplace::where('user_id',$id)->with('product_single_image','product_view_count')->get();
        if(count($marketplace)>0)
        {
            foreach($marketplace as $raw)
            {
                $data[]=array(
                    'id'=>$raw->id,
                    'product_title'=>$raw->product_title,
                    'price'=>$raw->price,
                    'location'=>$raw->location,
                    'description'=>$raw->description,
                    'full_address'=>$raw->full_address,
                     'mobile_no'=>$raw->country_code.'-'.$raw->mobile_number,
                    'total_views'=>@$raw->product_view_count->count(),
                    'image'=>@$raw->product_single_image->url
                );
            } 
            
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS'],'data'=>$data]);}
        else
        {return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400);}

    }
    public function marketPlaceSearch(Request $request)
    {
        
            $id=$request->user_id;
            if(!$id){
                return response()->json(['status'=>FALSE,'msg'=>'user_id Required'],400);
            }
            //DB::enableQueryLog();dd(DB::getQueryLog());
            $query = Marketplace::with(['product_single_image','user_info',
            'marketplace_saved' => function ($q) use($id) {
                $q->where('user_id','=',$id);
                }
                ]);
        
            if($request->search_string!='')
            {
                $query->where('product_title','like','%'.$request->search_string.'%');
            }
            if($request->location!='')
            {
                $query->where('location',$request->location);
            }
            if($request->short_type!='')
            {
                if($request->short_type==0)
                { $query->orderBy('id', 'ASC'); }
                if($request->short_type==1)
                { $query->orderBy('price', 'ASC'); }
                if($request->short_type==2)
                { $query->orderBy('price', 'DESC'); }
            }
            else
            {
                $query->orderBy('id','DESC');
            }
            if($request->category_id!='' && $request->category_id>0)
            {
                $query->where('market_place_category_id',$request->category_id);
            }
            $price_range=json_decode($request->price_range);
            if($price_range)
            {
                if($price_range->end!='30')
                {
                    $query->where('price','>=',(int)$price_range->start);
                    $query->where('price','<=',(int)$price_range->end);
                }
                
            }

            $result= $query->get();
        //dd($result);
            //dd(DB::getQueryLog());
            if(count($result)>0)
            {
                foreach($result as $raw)
                {   
                    $date=date('Y-m-d H:i:s',strtotime($raw->created_at));
                    $now = time(); // or your date as well
                    $your_date = strtotime($raw->created_at);
                    $datediff = $now - $your_date;

                    $day_ago= round($datediff / (60 * 60 * 24));
                    if($day_ago==0)
                    $day_ago='Today';
                    else
                    $day_ago=$day_ago.' Days';
                    
                    //dd($raw->marketplace_saved[0]['id']);
                    if(@$raw->marketplace_saved[0]['user_id']==$request->user_id)
                    $is_saved=1;
                    else
                    $is_saved=0;

                    $data[]=array(
                        'id'=>$raw->id,
                        'product_title'=>$raw->product_title,
                        'price'=>$raw->price,
                        'user_name'=>@$raw->user_info->full_name,
                        'days_ago'=>$day_ago,
                        'location'=>$raw->full_address,
                        'image'=>@$raw->product_single_image->url,
                        'is_saved'=>$is_saved,
                        
                        
                
                );
                }
                return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS'],'data'=>$data]);
            }
            else
        { return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400);}
    
    }
    public function marketPlaceProductDetails(Request $request)
    {
        
        
        $marketplace=Marketplace::where('id',$request->id)->with('product_image','user_info_with')->first();
        

        if($marketplace)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
        {
            //add view counter///
        $check=Marketplace_count::where('user_id',$request->user_id)->where('marketplace_id',$request->id)->first();
        if(!$check)
        {
        $response=Marketplace_count::insertGetId(['marketplace_id'=>$request->id,'user_id'=>$request->user_id]);
        }
            foreach(@$marketplace->user_info_with->user_sub_speciality as $raw)
            {
                //dd($raw->sub_speciality[0]['id']);
            $sub[]=array('id'=>@$raw->sub_speciality[0]['id'],'icon'=>@$raw->sub_speciality[0]['icon'],'title'=>@$raw->sub_speciality[0]['title']);
            }
                    $date=date('Y-m-d H:i:s',strtotime($marketplace->created_at));
                    $now = time(); // or your date as well
                    $your_date = strtotime($marketplace->created_at);
                    $datediff = $now - $your_date;

                    $day_ago= round($datediff / (60 * 60 * 24));
                    if($day_ago==0)
                    $day_ago='Today';
                    else
                    $day_ago=$day_ago.' Days';
            $data=array(
                'id'=>$marketplace->id,
                'product_title'=>$marketplace->product_title,
                'market_place_category_id'=>(int)$marketplace->market_place_category_id,
                'price'=>$marketplace->price,
                'location'=>$marketplace->location,
                'full_address'=>$marketplace->full_address,
                'description'=>$marketplace->description,
                'email'=>$marketplace->email,
                'mobile_no'=>$marketplace->country_code.'-'.$marketplace->mobile_number,
                'user_name'=>@$marketplace->user_info_with->full_name,
                'user_image'=>@$marketplace->user_info_with->profile_image,
                'specialitie'=>@$sub,
                'product_image'=>@$marketplace->product_image,
                'date'=>$date,
                'day_ago'=>$day_ago
            
            );
            
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS'],'data'=>$data]);}
        else
        {return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400);}

    }

/******************************************************************* */

/************************Search function*********************** */
   public function globleSearch(Request $request)
   {

    $res = $this->validation($request,'user_id,search_string');
    if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
    }

    if(!isset($request->page))
    $request->merge(['page' => 1]);
    
    if(!isset($request->search_type))
    {   //DB::enableQueryLog();
        $user=User::select('id','full_name','profile_image','country_id')->with('address')
        ->where('full_name','like','%'.$request->search_string.'%')
        ->paginate(5, ['*'], '', $request->page);
        $user = $user->getCollection();
       // dd(DB::getQueryLog());
        $group=Group::select('id','title','image')->where('title','like','%'.$request->search_string.'%')
        ->where('group_privacy','public')
        ->withCount('group_member')->paginate(5, ['*'], '', $request->page);
        $group = $group->getCollection();

        $event=Event::select('id','title_of_event','event_image','event_start','event_end')
        ->where('title_of_event','like','%'.$request->search_string.'%')
        ->where('event_end','>',date('Y-m-d H:i:s'))->where('event_type','Public')
        ->paginate(5, ['*'], '', $request->page);
        $event = $event->getCollection();

       
        $marketplace=Marketplace::select('id','product_title','price','user_id')
        ->where('product_title','like','%'.$request->search_string.'%')
        ->with('product_single_image','user_info:id,full_name')->paginate(5, ['*'], '', $request->page);
        $marketplace = $marketplace->getCollection();

        
        $job=JobPost::select('id','title','image','company_name')
        ->where('title','like','%'.$request->search_string.'%')->where('status',1)->paginate(5, ['*'], '', $request->page);
        $job = $job->getCollection();

        
        
    $data=array('user'=>$user,'group'=>$group,'event'=>$event,'marketplace'=>$marketplace,'job'=>$job);
    return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS'],'data'=>$data]);
    }
    else
    {   
       
        $data=array();
        if($request->search_type=='user')
        {
            $data=User::select('id','full_name','profile_image','country_id')->with('address')
            ->where('full_name','like','%'.$request->search_string.'%')
            ->paginate(5, ['*'], '', $request->page);
            $data = $data->getCollection();
        }
        else if($request->search_type=='group')
        {
            $data=Group::select('id','title','image')->where('title','like','%'.$request->search_string.'%')
        ->where('group_privacy','public')
        ->withCount('group_member')->paginate(5, ['*'], '', $request->page);
        $data = $data->getCollection();
        }
        else if($request->search_type=='event')
        {
            $data=Event::select('id','title_of_event','event_image','event_start','event_end')
        ->where('title_of_event','like','%'.$request->search_string.'%')
        ->where('event_end','>',date('Y-m-d H:i:s'))->where('event_type','Public')
        ->paginate(5, ['*'], '', $request->page);
        $data = $data->getCollection();
        }
        else if($request->search_type=='marketplace')
        {
            $data=Marketplace::select('id','product_title')
            ->where('product_title','like','%'.$request->search_string.'%')
            ->with('product_single_image')->paginate(5, ['*'], '', $request->page);
            $data = $data->getCollection();
        }
        else
        {
            return response()->json(['status'=>False,'msg'=>'Invalid Search Type Value']);
        }

        if(count($data)>0)
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS'],'data'=>$data]);
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']]);

    }

    
   
    }
/************************Search end*********************** */

/**********************Librery function ***************************/

    public function librerySearch(Request $request)
    {

        $search_string='';
        $category_id='';
        $access_type='';
        $item_type='';
        $language='';
        $publication='';
        $journal='';
        $isbn='';
        $short_by='';
        if($request->search_string)
        {
            $search_string="title LIKE '%$request->search_string%'";
        }
        if($request->category_id!='')
        {
            $category_id=" and category_id='$request->category_id'";
        }
        if($request->access_type!='')
        {
            $access_type=" and access_type='$request->access_type'";
        }
        if($request->item_type!='')
        {
            
            $array=explode(',', $request->item_type);
            $array = implode("','",$array);
            $item_type=" and item_type IN ('$array')";

        }
        if($request->language!='')
        {
            $language=" and language='$request->language'";
        }
        if($request->publication_date_form!='' && $request->publication_date_to!='')
        {
            $publication=" and publication_from >='$request->publication_date_form' and publication_to <= '$request->publication_date_to'";
        }
        if($request->journal!='')
        {
            $journal=" and journal='$request->journal'";
        }
        if($request->isbn!='')
        {
            $isbn=" and isbn='$request->isbn'";
        }
        if($request->short_by!='')
        {
            if($request->short_by=='1')
            $short_by=" order by read_count DESC";
            if($request->short_by=='2')
            $short_by=" order by created_at DESC";
            if($request->short_by=='3')
            $short_by=" order by created_at ASC";
        }
        $search = DB::select("select * from libreries where $search_string $category_id $access_type $item_type $language $publication $journal $isbn $short_by");
    //dd("select * from libreries where $search_string $category_id $access_type $item_type $language $publication $journal $isbn $short_by");
        if(count($search)>0)
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1'],'data'=>$search]);
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400);
    }


 ///////// get user profile by email and mobile API //////////////////
    public function getUserProfile(Request $request){
        $res = $this->validation($request,'user_key');
        if($res){
            return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        $user = User::where('mobile',$request->user_key)->orWhere('email',$request->user_key)->first();
        if($user){
            $email_verified=$mobile_verified=$user->profile_completed=$profile_completed=true;
                if($user->email_verified==0)
                    $email_verified=false;
                if($user->mobile_verified==0)
                    $mobile_verified=false;
                if($user->profile_completed==0)
                    $profile_completed=false;

                $data=array(
                    'id'=>$user->id,
                    'full_name'=>$user->full_name,
                    'dob'=>$user->dob,
                    'email'=>$user->email,
                    'user_name'=>$user->user_name,
                    'login_type'=>$user->login_type,
                    'profile_image'=>$user->profile_image,
                    'cover_image'=>$user->cover_image,
                    'mobile'=>$user->mobile,
                    'device_type'=>$user->device_type,
                    'device_token'=>$user->device_token,
                    'status'=>$user->status,
                    'country_id'=>$user->country_id,
                    'level_id'=>$user->level_id,
                    'specialitie_id'=>$user->specialitie_id,
                    'profession_id'=>$user->profession_id,
                    'profession_skill'=>$user->profession_skill,
                    'about_me'=>$user->about_me,
                    'email_verified'=>$email_verified,
                    'mobile_verified'=>$mobile_verified,
                    'profile_completed'=>$profile_completed,
                    'company_registration_number'=>$user->company_registration_number,
                    'remember_token'=>$user->remember_token
                    
                );
                return response()->json(['status'=>TRUE,'msg'=>'','data'=>$data]);
        } else {
            return response()->json(['status'=>FALSE,'msg'=>config('constants')['INVALID_USERNAME_OR_EMAIL']],400);
        }
    }

    ///////// send user otp in email API //////////////////
    public function sendResetPasswordOTP(Request $request){
        $res = $this->validation($request,'user_id');
        if($res){
            return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        $uData = User::whereId($request->user_id)->first();
        if($uData){
            $otp=$this->generateRandomString();
            if(User::whereId($request->user_id)->update(['otp'=>$otp])){
                $mailData['params']=['email'=>$uData->email,'user_id'=>$uData->id,'user_name'=>$uData->full_name,'otp'=>$otp,'subject'=>'MeddiFellow Reset Password OTP','template'=>'reset_password_otp'];
                $this->core->SendEmail($mailData);
                return response()->json(['status'=>TRUE,'msg'=>config('constants')['OTP_EMAIL_SEND']]);
            }
        } 
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['INVALID_USER']],400);

    }
     ///////// update user new password API //////////////////
    public function updateNewPassword(Request $request){
        $res = $this->validation($request,'otp,password');
        if($res){
            return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        $uData = User::whereId($request->user_id)->where('otp',$request->otp)->first();
        if($uData){
            User::whereId($request->user_id)->update(['password'=>Hash::make($request->password)]);
             $mailData['params']=['email'=>$uData->email,'user_id'=>$uData->id,'user_name'=>$uData->full_name,'subject'=>'MeddiFellow Password Update','template'=>'update_password_success'];
            $this->core->SendEmail($mailData);
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['PASSWORD_UPDATED']]);
        } 
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['INVALID_OTP']],400);
    }

    ////////// Is Email Unique API/////////////////////
    public function isEmailUnique(Request $request){
        if(User::where('email',$request->email)->count()){
           return response()->json(['status'=>FALSE,'msg'=>config('constants')['EMAIL_ALREADY_EXIST']]);
        }else{
           return response()->json(['status'=>TRUE,'msg'=>config('constants')['EMAIL_AVILABLE']],400);
        }
    }
    
    public function userTitle()
    {
     $category=Title::all();
     if(count($category)>0)
     return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS'],'data'=>$category]);
     else
     return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400);

    }

    ///////////////////////////////////////General Functions//////////////////////////////////////////////////
    private function generateRandomString($length = 5){
        $characters = '12345678998765432145789129862275634236578686523546'.time();
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    private function getUnusedRandomFileName($extention) {
        return md5(microtime()) .'.'. $extention;
    }
    public function mc_encrypt($string) {
        if(!$string){return false;}
        $encrypt_method = "AES-256-CBC";
        $key = hash('sha256', 'hVTCI4j9H7PlQz0F');
        $iv = substr(hash('sha256', 'goforsys@#123'), 0, 16);
        $output = trim($this->safe_b64encode(openssl_encrypt($string, $encrypt_method, $key, 0, $iv)));    
        return $output; 
     }
     public function mc_decrypt($string) {
         if(!$string){return false;}
         $encrypt_method = "AES-256-CBC";
         $key = hash('sha256', 'hVTCI4j9H7PlQz0F');
         $iv = substr(hash('sha256', 'goforsys@#123'), 0, 16);    
         $output = trim(openssl_decrypt($this->safe_b64decode($string), $encrypt_method, $key, 0, $iv));
         return $output; 
     }
     public function safe_b64encode($string) {
        $data = base64_encode($string);
        $data = str_replace(array('+','/','='),array('-','_',''),$data);
        return $data;
    }
    public function safe_b64decode($string) {
        $data = str_replace(array('-','_'),array('+','/'),$string);
        $mod4 = strlen($data) % 4;
        if ($mod4) {
            $data .= substr('====', $mod4);
        }
        return base64_decode($data);
    }
    //Relative Date Function


}
