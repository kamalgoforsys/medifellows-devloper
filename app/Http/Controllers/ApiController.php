<?php

namespace App\Http\Controllers;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Validator; 
use App\User,App\Country,App\User_contact_detail,App\User_qualification,App\User_intern_info,App\User_workspace,App\User_certificate_and_registration,App\User_post,App\Follows,App\User_media,App\User_album,App\Post_meta,App\Event_joined_user,App\Event,App\User_friend,App\Marketplace_category,App\Marketplace,App\Librery,App\User_additional_info,App\Company,App\User_block;
use DB;
use App\Group,App\GroupMember,App\GroupPost,App\GroupMedia;
use App\JobPost,App\JobApplyUser,App\JobEducation;
class ApiController extends Controller
{
    private $core = 0;
    private $msg_array=NULL;
    private $img_path = '';
    public function __construct()
    {
        $this->img_path = public_path('/uploads/avatar/');
        $this->core = app(\App\Http\Controllers\CoreController::class);
    }
    ///////comon Validation function///////////////
    public function validation($request,$fields){
        $array = explode(',', $fields);
        $error = [];
        foreach ($array as $key => $value) {
         if($request->$value==''){
            $error[]= $value.' is Required';
         }
        }
        return $error;
       }
    ////////// privacyPolicy API/////////////////////
    public function privacyPolicy(){
        $pp=DB::table('privacy_policys')->get();
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['NO_ERROR'],'content'=>$pp[0]->text],'200');
    }
     ////////// check Email Exist API/////////////////////
    public function checkExist(Request $request){
        if(User::where('email',$request->email)->count()){
           return response()->json(['status'=>TRUE,'msg'=>config('constants')['EMAIL_ALREADY_EXIST']]);
        }else{
           return response()->json(['status'=>FALSE,'msg'=>config('constants')['EMAIL_AVILABLE']],400);
        }
    }

    public function checkEmailVerify($id)
    {
        if(!$id){
            return response()->json(['status'=>FALSE,'msg'=>'ID is Missing'],400);
         }
         
        $user=User::where('id',$id)->first();
        if(count($user)>0){
            if($user->email_verified==0)
            return response()->json(['status'=>FALSE,'msg'=>config('constants')['EMAIL_NOT_VERIFIED']],400);
            else
            return response()->json(['status'=>TRUE,'msg'=>'Email Verified']);
            
         }
         else
         {
            return response()->json(['status'=>FALSE,'msg'=>'User_id Not Found'],400);   
         }
    }
     ////////// check company Exist API/////////////////////
     public function checkCompany(Request $request){
        if(Company::where('company_reg_no',$request->company_reg_no)->count()){
           return response()->json(['status'=>TRUE,'msg'=>config('constants')['VALID_COMPANY']]);
        }else{
           return response()->json(['status'=>FALSE,'msg'=>config('constants')['INVALID_COMPANY']],400);
        }
    }

    public function userBlock(Request $request)
    {
        $res = $this->validation($request,'user_id,block_user_id');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        if(User::where('id',$request->user_id)->count()>0 && User::where('id',$request->block_user_id)->count()>0)
        {
            if(User_block::where('user_id',$request->user_id)->where('block_user_id',$request->block_user_id)->count()>0)
            {
                return response()->json(['status'=>FALSE,'msg'=>config('constants')['ALREADY_BLOCK']],400);   
            }
            else
            {

                if(User_block::insertGetId($request->all()) > 0){
                return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]);   
            }
            else
            {
                return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);   
            }
        }
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400);
    }
    public function userUnBlock(Request $request)
    {
        $res = $this->validation($request,'user_id,block_user_id');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        
        $delete=User_block::where('user_id',$request->user_id)->where('block_user_id',$request->block_user_id)->first();
        if($delete){ $delete->delete(); }
        if($delete)
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]); 
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400); 
        
    }
    public function blockList($id)
    {
        
        if(!$id){
        return response()->json(['status'=>FALSE,'msg'=>'user_id Missing'],400);
        }  
        if(User::where('id',$id)->count()>0)
        {   $list= User_block::where('user_id',$id)->with(array('user_info'=>function($query){
            $query->select('id','full_name','profile_image');
        }))->get();
        
          // dd($list);
            if(count($list)>0){
                foreach($list as $raw){
                    $data[]=$raw->user_info;
                }
                
                return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1'],'data'=>$data]);   
            }
            else
            {
                return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);   
            }
        }
        
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400);
    }
    
    
    ////////// login API/////////////////////
    public function login(Request $request){
     $res = $this->validation($request,'username,password,device_type,type');
     if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
     }
        $user = User::where('user_name',$request->username)->orWhere('email',$request->username)->first();
        if($user){
            if(password_verify($request->password,$user->password)){
                if($user->email_verified==0)
                $email_verified=false;
                else 
                $email_verified=true;
                
                if($user->mobile_verified==0)
                $mobile_verified=false;
                else 
                $mobile_verified=true;

                if($user->profile_completed==0)
                $profile_completed=false;
                else 
                $profile_completed=true;

                $data=array(
                    'id'=>$user->id,
                    'full_name'=>$user->full_name,
                    'dob'=>$user->dob,
                    'email'=>$user->email,
                    'user_name'=>$user->user_name,
                    'login_type'=>$user->login_type,
                    'profile_image'=>$user->profile_image,
                    'cover_image'=>$user->cover_image,
                    'mobile'=>$user->mobile,
                    'device_type'=>$user->device_type,
                    'device_token'=>$user->device_token,
                    'status'=>$user->status,
                    'country_id'=>$user->country_id,
                    'level_id'=>$user->level_id,
                    'specialitie_id'=>$user->specialitie_id,
                    'profession_id'=>$user->profession_id,
                    'profession_skill'=>$user->profession_skill,
                    'about_me'=>$user->about_me,
                    'email_verified'=>$email_verified,
                    'mobile_verified'=>$mobile_verified,
                    'profile_completed'=>$profile_completed,
                    'company_registration_number'=>$user->company_registration_number,
                    'remember_token'=>$user->remember_token
                    
                );
                if($user->email_verified==0)
                {
                    return response()->json(['status'=>TRUE,'msg'=>config('constants')['EMAIL_NOT_VERIFIED'],'data'=>$data]);       
                }
                if($user->profile_completed==0)
                {
                    return response()->json(['status'=>TRUE,'msg'=>config('constants')['PROFILE_NOT_COMPLETED'],'data'=>$data]);       
                }
                //dd($user->id);
                $user->remember_token=$this->mc_encrypt($user->id);
                if($request->device_type!='' && ($request->device_type=='Android' || $request->device_type=='IOS'))
                {
                $user->device_type = $request->device_type;
                if($request->device_token!='')
                {$user->device_token = $request->device_token;}
                
                }
                $user->save();
                return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS_LOGIN'],'data'=>$data]);
            }else
                return response()->json(['status'=>FALSE,'msg'=>config('constants')['INVALID_PASS']],400);    
        }else
            return response()->json(['status'=>FALSE,'msg'=>config('constants')['INVALID_USERNAME_OR_EMAIL']],400);
    }
     ////////// registration API/////////////////////
    public function registration(Request $request)
    {   
        ////////////////////////input validations/////////////////////////////////////
        $res = $this->validation($request,'full_name,country_id,dob,email,password,device_type');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        ///////////////////////Check email already exist//////////////////////////////
        if(User::where('email',$request->email)->count()){
            return response()->json(['status'=>FALSE,'msg'=>config('constants')['EMAIL_ALREADY_TAKEN']],400);
        }
        ////////////////////////insert user details///////////////////////////////////
        
        $request->merge(['password'=>Hash::make($request->password)]);
        $user = User::insertGetId($request->all());
        if($user){
            
            $remember_token=$this->mc_encrypt($user);
            $data=array();
            $data=User::where('id',$user)->update(['remember_token'=>$remember_token]);
            $uData =User::find($user);
            $mailData['params']=['email'=>$uData->email,'user_id'=>$uData->id,'user_name'=>$uData->full_name,'subject'=>'E-mail Confirmation','template'=>'verify_email'];
            $this->core->SendEmail($mailData);
            
            if($uData->email_verified==0)
            $email_verified=false;
            else 
            $email_verified=true;
            
            if($uData->mobile_verified==0)
            $mobile_verified=false;
            else 
            $mobile_verified=true;

            if($uData->profile_completed==0)
            $profile_completed=false;
            else 
            $profile_completed=true;

            $data=array(
                'id'=>$uData->id,
                'full_name'=>$uData->full_name,
                'dob'=>$uData->dob,
                'email'=>$uData->email,
                'user_name'=>$uData->user_name,
                'login_type'=>$uData->login_type,
                'profile_image'=>$uData->profile_image,
                'cover_image'=>$uData->cover_image,
                'mobile'=>$uData->mobile,
                'device_type'=>$uData->device_type,
                'device_token'=>$uData->device_token,
                'status'=>$uData->status,
                'country_id'=>$uData->country_id,
                'level_id'=>$uData->level_id,
                'specialitie_id'=>$uData->specialitie_id,
                'profession_id'=>$uData->profession_id,
                'profession_skill'=>$uData->profession_skill,
                'about_me'=>$uData->about_me,
                'email_verified'=>$email_verified,
                'mobile_verified'=>$mobile_verified,
                'profile_completed'=>$profile_completed,
                'company_registration_number'=>$uData->company_registration_number,
                'remember_token'=>$uData->remember_token
                
            );
            
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['REG_SUCCESS'],'data'=>$data]);
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
    }
    public function changePassword(Request $request)
    {
        $res = $this->validation($request,'user_id,current_password,new_password,confirm_password');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        $user = User::where('id',$request->user_id)->first();
        if(count($user) > 0){
            if(password_verify($request->current_password,$user->password)){

                $request->merge(['password'=>Hash::make($request->new_password)]);
                $data=User::where('id',$request->user_id)->update($request->except(['user_id','current_password','new_password','confirm_password']));
                return response()->json(['status'=>TRUE,'msg'=>config('constants')['PASSWORD_UPDATED']]);   
            }
            else
            {
                return response()->json(['status'=>FALSE,'msg'=>config('constants')['CURRENT_PASSWORD_NOT_MATCH']],400);   
            }
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400);
    }
    public function addMobileNumber(Request $request)
    {
        $res = $this->validation($request,'user_id,code,mobile_number');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        ///////check Mobile no already exist or not//////////////////////
        if(User::where('mobile',$request->code.''.$request->mobile_number)->count()){
            return response()->json(['status'=>FALSE,'msg'=>config('constants')['MOBILE_ALREADY_TAKEN']],400);
        }
        /////////////////////////////////////////////////////////////////
        
        $request->merge(['mobile'=>$request->code.''.$request->mobile_number,'otp'=>rand ( 10000 , 99999 )]);
        $data=User::where('id',$request->user_id)->update($request->except(['user_id','code','mobile_number']));
        if($data){
            ///todo///
            //send sms//
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['OTP_SENT']]);   
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);

    }
    public function resendOTP(Request $request)
    {
        $res = $this->validation($request,'user_id');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        $request->merge(['otp'=>rand ( 10000 , 99999 )]);
        $data=User::where('id',$request->user_id)->update($request->except(['user_id']));
        if($data){
            ///todo///
            //send sms//
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['OTP_SENT']]);   
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
    }
    public function mobileOTPVerified(Request $request)
    {
        $res = $this->validation($request,'user_id','otp');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        $request->merge(['mobile_verified'=>1]);
        $data=User::where('id',$request->user_id)->where('otp',$request->otp)->update($request->except(['user_id','otp']));
        if($data){
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['OTP_VERIFIED_SUCCESS']]);   
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['INVALID_OTP']],400);
    }
    
    public function forgotPassword(Request $request)
    {
        $res = $this->validation($request,'email');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        $user = User::where('mobile',$request->email)->orWhere('email',$request->email)->first();
        if($user){
        $request->merge(['otp'=>rand ( 10000 , 99999 )]);
        $data=User::where('mobile',$request->email)->orWhere('email',$request->email)->update($request->except(['email']));
        if($data){
            $mailData['params']=['email'=>$user->email,'user_id'=>$user->id,'user_name'=>$user->full_name,'otp'=>$request->otp,'subject'=>'Forgot Password Mail','template'=>'forgot_password'];
            $this->core->SendEmail($mailData);
           
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1'],'data'=>$user->id]);   
        }
       }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['EMAIL_NOT_EXIST']],400);
    }
    public function forgotPasswordOTPVerified(Request $request)
    {
        $res = $this->validation($request,'user_id','otp');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        
        $data=User::where('id',$request->user_id)->first();
        if($data){
            if($data->otp==$request->otp)
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['OTP_VERIFIED_SUCCESS'],'data'=>$data->id]);  
            else
            return response()->json(['status'=>FALSE,'msg'=>config('constants')['INVALID_OTP']],400);
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['INVALID_OTP']],400);
    }
    public function forgotPasswordChangePassword(Request $request)
    {
        $res = $this->validation($request,'user_id,new_password,confirm_password');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        $user = User::where('id',$request->user_id)->first();
        if(count($user) > 0){
                $request->merge(['password'=>Hash::make($request->new_password)]);
                $data=User::where('id',$request->user_id)->update($request->except(['user_id','new_password','confirm_password']));
                return response()->json(['status'=>TRUE,'msg'=>config('constants')['PASSWORD_UPDATED']]);   
                   }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400);
    }
    
     ////////// complete Profile API/////////////////////
         
    public function completeProfile(Request $request){
               
        $res = $this->validation($request,'user_id,profile_image,qualification_category');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        if(User::where('id',$request->user_id)->first()){ 
        // qualification_category :: 1=Student 2=Qualified 3=Other
        if($request->qualification_category==1){
            
            $student_intern_info=json_decode($request->student_info);
            if($student_intern_info)
            {
                $student_intern_info_array=array(
                         'user_id'=>$request->user_id,
                         'university_id'=>$student_intern_info->university_id,
                         'intern_id'=>$student_intern_info->intern_id,
                );
            User_intern_info::insert($student_intern_info_array);
            }
            else
            {
                return response()->json(['status'=>FALSE,'msg'=>'Student info Missing'],400);  
            }
        } 
        else if($request->qualification_category==2){
             $professional_info=json_decode($request->professional_info);
             if($professional_info){
              foreach($professional_info->qualification as $raw)
                {
                       $qualification_array[]=array(
                                     'user_id'=>$request->user_id,
                                     'default_qualification'=>$raw->default_qualification,
                                     'speciality_id'=>$raw->speciality_id,
                                     'qualification_id'=>$raw->qualification_id,
                                     'institution_id'=>$raw->institution_id,
                                    );
                }
                $certificate_and_registration_array=array(
                                     'user_id'=>$request->user_id,
                                     'country_id'=>$professional_info->certificate_info->country_id,
                                     'registration_number'=>$professional_info->certificate_info->registration_number,
                                     'certificate'=>$professional_info->certificate_info->certificate,
                                      );
                
                $current_place_of_work_array=array('user_id'=>$request->user_id,
                                 'current_work_place_name'=>$professional_info->work_place_info->current_work_place_name,
                                 'country_id'=>$professional_info->work_place_info->country_id,
                                 'state'=>$professional_info->work_place_info->state,
                                 'zip_code'=>$professional_info->work_place_info->zip_code,
                                );
                
                User_workspace::insert($current_place_of_work_array);
                User_qualification::insert($qualification_array);
                User_certificate_and_registration::insert($certificate_and_registration_array);
                            }else
                            {
                                return response()->json(['status'=>FALSE,'msg'=>'Professional info Missing'],400);      
                            } 
            } else if($request->qualification_category==3) {

            $other_info=json_decode($request->other_info);
            if($other_info){
                $user_additional_info[]=array(
                    'user_id'=>$request->user_id,
                    'position'=>$other_info->position,
                    'workplace'=>$other_info->workplace,
                    'company_reg_no'=>$other_info->company_reg_no,
                    'country_code'=>$other_info->country_code,
                    'phone_number'=>$other_info->phone_number,
                      
            );
           
            User_additional_info::insert($user_additional_info);
        }
        else
        {
            return response()->json(['status'=>FALSE,'msg'=>'Other info Missing'],400);       
        }
                   
        }
        else
        {
            return response()->json(['status'=>FALSE,'msg'=>config('constants')['INVALID_Q_CATEGORY']],400);
        }
                       
        $data=array('profile_image'=>$request->profile_image,
                    'qualification_category'=>$request->qualification_category,
                    'profile_completed'=>1
                 );
        $data=User::where('id',$request->user_id)->update($data);
        if($data){
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['PROFILE_UPDATE']]);
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
        }
        else
        {
        return response()->json(['status'=>FALSE,'msg'=>'User id Not Found'],400);
        }
    }
     ////////// resend Email Verify Link API/////////////////////
     public function resendEmailVerifyLink(Request $request)
     {
         $res = $this->validation($request,'email');
         if($res){
         return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
         }
         $user = User::where('email',$request->email);
         if($user->count()>0){
            $uData = $user->first();
            $mailData['params']=['email'=>$request->email,'user_id'=>$uData->id,'user_name'=>$uData->full_name,'subject'=>'E-mail Confirmation','template'=>'verify_email'];
            $this->core->SendEmail($mailData);
             return response()->json(['status'=>TRUE,'msg'=>config('constants')['VERIFY_LINK_SENT']]);
          }else{
             return response()->json(['status'=>FALSE,'msg'=>config('constants')['EMAIL_NOT_EXIST']],400);
          }
     }
      ////////// Email Verify API/////////////////////
     public function EmailVerify(Request $request)
     {
         $email=base64_decode($request->email);
         if(User::where('email',$email)->count()){
             User::where('email',$email)->update($data=array('email_verified'=>1));
             return response()->json(['status'=>TRUE,'msg'=>'Email Verified Successfully']);
          }else{
             return response()->json(['status'=>FALSE,'msg'=>'This Email id not Registered yet'],400);
          }
     }
     
    ////////// update Profile Pic API/////////////////////
    public function updateProfilePic(Request $request)
    {
        //dd(date('Y-m-d H:i:s'));
        $res = $this->validation($request,'user_id,profile_image');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        $data=User::where('id',$request->user_id)->update($request->except(['user_id']));
        if($data){
            ////// add profile pic add Post ///////
            $user_post_array=array(
                'user_id'=>$request->user_id,
                'type'=>'media',
                'album_id'=>1,
            );
            $post_id=User_post::insertGetId($user_post_array);

            //////// update user media //////
            $user_media_array=array(
                'user_id'=>$request->user_id,
                'type'=>'profile_pic_update',
                'url'=>$request->profile_image,
                'user_post_id'=>$post_id
                
            );
            User_media::insert($user_media_array);
            
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['PROFILE_PIC_UPDATE']]);
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
    }
     ////////// update Cover Pic API/////////////////////
    public function updateCoverPic(Request $request)
    {
        $res = $this->validation($request,'user_id,cover_image');
        if($res){
            return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        $data=User::where('id',$request->user_id)->update($request->except(['user_id']));
        if($data){
        ////// add profile pic add Post ///////
        $user_post_array=array(
            'user_id'=>$request->user_id,
            'type'=>'media',
            'album_id'=>1,
         );
        $post_id=User_post::insertGetId($user_post_array);

        //////// update user media //////
        $user_media_array=array(
            'user_id'=>$request->user_id,
            'type'=>'profile_pic_update',
            'url'=>$request->cover_image,
            'user_post_id'=>$post_id
            
         );
        User_media::insert($user_media_array);
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['PROFILE_COVER_UPDATE']]);
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
    }
    ////////// Update Profile API/////////////////////
    public function updateProfile(Request $request){
               
        $res = $this->validation($request,'id');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        $qualification=json_decode($request->qualification);
        $current_place_of_work=json_decode($request->current_place_of_work);
        $certificate_and_registration=json_decode($request->certificate_and_registration);
        $student_intern_info=json_decode($request->student_intern_info);
        $contact_detail=json_decode($request->contact_detail);
        
        foreach($qualification as $raw)
        {
               $qualification_array[]=array('user_id'=>$request->id,
                             'level_id'=>$raw->level_id,
                             'speciality_id'=>$raw->speciality_id,
                             'qualification_id'=>$raw->qualification_id,
                             'institution_id'=>$raw->institution_id,
                            );
        }
        foreach($current_place_of_work as $raw_work)
        {
               $current_place_of_work_array[]=array('user_id'=>$request->id,
                             'current_work_place_name'=>$raw_work->current_work_place_name,
                             'country_id'=>$raw_work->country_id,
                             'state'=>$raw_work->state,
                             'zip_code'=>$raw_work->zip_code,
                            );
        }
        
        foreach($certificate_and_registration as $raw_certificate)
        {
               $certificate_and_registration_array[]=array('user_id'=>$request->id,
                             'country_id'=>$raw_certificate->country_id,
                             'registration_number'=>$raw_certificate->registration_number,
                             'certificate'=>$raw_certificate->certificate,
                              );
        }
        foreach($student_intern_info as $raw_intern)
        {
               $student_intern_info_array[]=array('user_id'=>$request->id,
                             'university_id'=>$raw_intern->university_id,
                             'intern_id'=>$raw_intern->intern_id,
                             );
        }
        
        foreach($contact_detail[0]->phone_array as $raw)
             {
                $phone_array[]=array('user_id'=>$request->id,
                             'meta_type'=>'Phone',
                             'code'=>$raw->code,
                             'phone_number'=>$raw->phone_number,
                             );
             }
        foreach($contact_detail[0]->email_array as $raw)
             {
                $email_array[]=array('user_id'=>$request->id,
                             'meta_type'=>'Email',
                             'email_id'=>$raw->email_id,
                                    );
             }
        
        User_contact_detail::insert($phone_array);
        User_contact_detail::insert($email_array);
        User_qualification::insert($qualification_array);
        User_intern_info::insert($student_intern_info_array);
        User_workspace::insert($current_place_of_work_array);
        User_certificate_and_registration::insert($certificate_and_registration_array);
        
        $data=array('profile_image'=>$request->profile_image,
                   'company_registration_number'=>$contact_detail[0]->company_registration_number
                 );
        $data=User::where('id',$request->id)->update($data);
        if($data){
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['PROFILE_UPDATE']]);
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
    }

    public function landingPage($id)
    {
        
        if(!$id){
        return response()->json(['status'=>FALSE,'msg'=>'user_id is missing'],400);
        }
        //DB::enableQueryLog();
        $user=User::where('id',$id)->with('speciality','user_sub_speciality','user_education','current_work_place')->get();
        //dd($user[0]->user_sub_speciality);
        if(count($user)>0){
        $sub_speciality=array();
        $education=array();
        foreach($user[0]->user_sub_speciality as $raw)
        {
          $sub_speciality[]=array('icon'=>$raw->sub_speciality[0]->icon,'name'=>$raw->sub_speciality[0]->title);
        }
        foreach($user[0]->user_education as $raw)
        {
          $education[]=array('education_text'=>$raw->qualification[0]->title);
        }
        
        $website='';
        if(count($user[0]->user_website)>0)
        {
            $website=$user[0]->user_website->website;
        }
        $workplace='';
        if(count($user[0]->current_work_place)>0)
        {
            $workplace=$user[0]->current_work_place->current_work_place_name;
        }
        $speciality='';
        if(count($user[0]->speciality)>0)
        {
            $speciality=$user[0]->speciality->title;
        }
        
        $user_array=array(
            "cover_image"=>$user[0]['profile_image'],
            "profile_image"=>$user[0]['cover_image'],
            "user_name"=>$user[0]['full_name'],
            "speciality_name"=>$speciality,
            "sub_speciality"=>$sub_speciality,
            "about_me"=>$user[0]['about_me'],
            "total_number_of_followers"=>Follows::where('user_id',$id)->count(),
            "total_number_of_posts"=>User_post::where('user_id',$id)->count(),
            "total_number_of_follows"=>Follows::where('follower_id',$id)->count(),
            "address"=>$user[0]->address->name,
            "address_icon"=>$user[0]->address->flag,
            "education"=>$education,
            "current_work_place"=>$workplace,
            "website"=>$website,
             );
            return response()->json(['status'=>TRUE,"msg"=>config('constants')['SUCCESS'],'data'=>$user_array]);
            }
            return response()->json(['status'=>FALSE,"msg"=>config('constants')['INVALID_USER']],400); 
        
    }
    public function removeProfilePic($id)
    {
        if(!$id){ return response()->json(['status'=>FALSE,'msg'=>'user_id is missing'],400); }
        $data=User::where('id',$id)->update($data=array('profile_image'=>''));
        if($data){
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['PROFILE_PIC_REMOVE']]);
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
        
    }
    public function addPost(Request $request)
    {
        $res = $this->validation($request,'user_id,location');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        if($request->type=='media')
        {
            if(!$request->media)
            return response()->json(['status'=>FALSE,'msg'=>'Media is missing'],400);
            if(!$request->album_id)
            return response()->json(['status'=>FALSE,'msg'=>'Album id is missing'],400);
        }
        else
        {
            if(!$request->text_message_typed)
            return response()->json(['status'=>FALSE,'msg'=>'Text Message Typed is missing'],400);
        }
        if($request->type!='media')
        {
        $post_id = User_post::insertGetId($request->except(['media','album_id']));
        }
        else
        {
            $post_data=array(
                'user_id'=>$request->user_id,
                'type'=>"media",
                'location'=>$request->location,
                'album_id'=>$request->album_id,
                'text_message_typed'=>$request->text_message_typed
            );
           
            $post_id = User_post::insertGetId($post_data);
            //////// add post media //////
            $media=json_decode($request->media);
            foreach($media as $raw)
            {
                $media_array[]=array(
                    'user_id'=>$request->user_id,
                    'type'=>$raw->type,
                    'url'=>$raw->media,
                    'user_post_id'=>$post_id
                                   );
            }
            User_media::insert($media_array);
        }
             if($post_id){
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['POST_ADD_SUCCESS']]); 
            }
            return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);

    }
    public function userPost($id,$page)
    {   //,'post_like','post_share','post_comment'
        $user_profile_pic=User::where('id',$id)->first(['profile_image']);
        $user_website=User_contact_detail::where('user_id',$id)->where('meta_type','website')->first(['website']);
        $website='';
        if($user_website)
        {
          $website=$user_website->website;  
        }
        //DB::enableQueryLog();
        $user_post = User_post::where('user_id',$id)->with('post_media','like_count','comment_count')->paginate(1, ['*'], '', $page);
        //dd(DB::getQueryLog());
        if(count($user_post)>0) {
        foreach($user_post as $raw)
        {
            $media=array();
            if($raw->type=='media')
            {
                foreach($raw->post_media as $media_raw)
                {
                    $media[]=array('media_type'=>$media_raw->type,'media_url'=>$media_raw->url);  
                }
            }

            $user_array[]=array(
                "post_id"=>$raw->id,
                "profile_image"=>$user_profile_pic->profile_image,
                "date_of_post"=>date('d-m-y',strtotime($raw->created_at)),
                "time_of_post"=>date('h:i:s A',strtotime($raw->created_at)),
                "location_of_post"=>$raw->location,
                "text_message"=>$raw->text_message_typed,
                "media"=>$media,
                "website"=>$website,
                "total_number_of_likes"=>count($raw->like_count),
                "total_number_of_comments"=>count($raw->comment_count),
                "total_number_of_follows"=>0,
                ); 
            
        }
        return response()->json(['status'=>TRUE,"msg"=>config('constants')['SUCCESS'],'data'=>$user_array]);
                         }
                else
                {
                    return response()->json(['status'=>FALSE,"msg"=>config('constants')['NO_POST']],400);  
                }
    }
    public function postAllLike($id)
    {
        $all_like=Post_meta::where('post_id',$id)->where('type','Like')->with('like_user_info')->get();
        //dd($all_like);
        if(count($all_like)>0)
        {
            foreach($all_like as $raw)
            {
                $speciality='';
                if($raw->like_user_info->speciality)
                {$speciality=$raw->like_user_info->speciality->title;}
                $user_level='';
                if($raw->like_user_info->user_level)
                {
                   $user_level= $raw->like_user_info->user_level->title;
                }

                $data[]=array(
                    "user_id"=>$raw->like_user_info->id,
                    "profile_pic"=>$raw->like_user_info->profile_image,
                    "user_full_name"=>$raw->like_user_info->full_name,
                    "level"=>$user_level,
                    "user_speciality"=>$speciality
                );
            }
            return response()->json(['status'=>TRUE,"msg"=>config('constants')['SUCCESS'],'data'=>$data]);
        }
        return response()->json(['status'=>FALSE,"msg"=>config('constants')['NO_LIKE_POST']],400);
    }
    public function postLikeDislike(Request $request)
    {
        $res = $this->validation($request,'loggedin_user_id,post_user_id,post_id,current_status');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        if($request->current_status=='0')
        {
         $like=Post_meta::where('user_id',$request->loggedin_user_id)->where('post_id',$request->post_id)->first();
         if($like){ $like->delete(); }
        }
        else
        {
         $request->merge(['user_id'=>$request->loggedin_user_id,'type'=>'Like']);
         $like=Post_meta::insert($request->except(['loggedin_user_id','current_status']));
        }
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]); 
    }
    public function postComments($id)
    {
        if(!$id)
        {
            return response()->json(['status'=>FALSE,'msg'=>'Post id is Missing'],400);    
        }
        $comments=Post_meta::where('type','Comment')->where('post_id',$id)->with('comment_user_info')->get();
        if(count($comments)>0)
        {
            foreach($comments as $raw)
            {
                $speciality='';
                if($raw->like_user_info->speciality)
                {$speciality=$raw->like_user_info->speciality->title;}
                $user_level='';
                if($raw->like_user_info->user_level)
                {
                   $user_level= $raw->like_user_info->user_level->title;
                }

                $data[]=array(
                    "user_id"=>$raw->like_user_info->id,
                    "profile_pic"=>$raw->like_user_info->profile_image,
                    "user_full_name"=>$raw->like_user_info->full_name,
                    "level"=>$user_level,
                    "user_speciality"=>$speciality,
                    "comment_id"=>$raw->id,
                    "comment_text"=>$raw->comment_text
                );
            }
            return response()->json(['status'=>TRUE,"msg"=>config('constants')['SUCCESS'],'data'=>$data]);
        }
        return response()->json(['status'=>FALSE,"msg"=>config('constants')['NO_POST_COMMENTS']],400);
    }
    public function postAddComment(Request $request)
    {
        $res = $this->validation($request,'loggedin_user_id,post_user_id,post_id,comment_text');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        $request->merge(['user_id'=>$request->loggedin_user_id,'type'=>'Comment']);
        $comment=Post_meta::insert($request->except(['loggedin_user_id']));
        if($comment)
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]); 
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400); 
        
    }
    public function postEditComment(Request $request)
    {
        $res = $this->validation($request,'comment_id,comment_edited_text');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        $request->merge(['comment_text'=>$request->comment_edited_text]);
        $comment=Post_meta::where('id',$request->comment_id)->update($request->except(['comment_edited_text','comment_id']));
        if($comment)
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]); 
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400); 
        
    }
    public function postDeleteComment(Request $request)
    {
        $res = $this->validation($request,'comment_id,loggedin_user_id');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        
        $comment=Post_meta::where('user_id',$request->loggedin_user_id)->where('id',$request->comment_id)->first();
        if($comment){ $comment->delete(); }
        if($comment)
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]); 
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400); 
        
    }
    public function deletePost(Request $request)
    {
        $res = $this->validation($request,'post_id,user_id');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        
        $post_details=User_post::where('user_id',$request->user_id)->where('id',$request->post_id)->first();
        if($post_details){ $post_details->delete(); }
        if($post_details)
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]); 
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400); 
        
    }
    public function postShare(Request $request)
    {
        $res = $this->validation($request,'post_id,user_id,post_user_id');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        $request->merge(['type'=>'Share']);
        $share=Post_meta::insert($request->all());
        if($share)
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]); 
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400); 
        
    }
    public function createAlbum(Request $request)
    {
        $res = $this->validation($request,'user_id,album_name');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        //dd($request->all());
        $album=User_album::insertGetId($request->all());
        if($album)
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]); 
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400); 
        
    }
    public function gallery($id)
    {
        if(!$id){
            return response()->json(['status'=>FALSE,'msg'=>'user_id is missing'],400);
            }
        $gallery=User_media::where('user_id',$id)->get(['id','type','url']);
        if(count($gallery)>0)
        {
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1'],'data'=>$gallery]); 
        }
        else
        {
            return response()->json(['status'=>FALSE,'msg'=>config('constants')['GALLERY_EMPTY']],400); 
        }
    }
    public function uploadsImageVideo($id)
    {
        if(!$id){
            return response()->json(['status'=>FALSE,'msg'=>'user_id is missing'],400);
            }
        $gallery=User_media::where('user_id',$id)->get(['id','type','url']);
        if(count($gallery)>0)
        {
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1'],'data'=>$gallery]); 
        }
        else
        {
            return response()->json(['status'=>FALSE,'msg'=>config('constants')['GALLERY_EMPTY']],400); 
        }
    }
    public function userAlbum($id)
    {
        if(!$id){
            return response()->json(['status'=>FALSE,'msg'=>'user_id is missing'],400);
            }
        $gallery=User_album::where('user_id',$id)->orWhere('user_id',0)->with('user_last_post','thumnail')->get();
        if(count($gallery)>0)
        {
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1'],'data'=>$gallery]); 
        }
        else
        {
            return response()->json(['status'=>FALSE,'msg'=>config('constants')['GALLERY_EMPTY']],400); 
        }
    }
    public function albumDetails(Request $request)
    {
        $media_array=array();
        $res = $this->validation($request,'user_id,album_id');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        $album_name=User_album::where('id',$request->album_id)->first(['album_name']);
        $details=User_post::where('user_id',$request->user_id)->where('album_id',$request->album_id)->with('post_media_details')->get();
        foreach($details as $raw)
        {   
            foreach($raw->post_media_details as $media){
            unset($media['user_id']);
            unset($media['user_post_id']);
            $media_array[]=array($media);
        }
        }
              
        if(count($details)>0)
        {
            $album_details[]=array('album_name'=>$album_name->album_name,'media_array'=>$media_array) ;
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1'],'data'=>$album_details]); 
        }
        else
        {return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400); }
    }
    public function userFriends($id)
    {
        if(!$id){
            return response()->json(['status'=>FALSE,'msg'=>'user_id is missing'],400);
            }
        $friends_list=User_friend::where('user_id',$id)->with(array('user_details'=>function($query){
            $query->select('id','full_name','profile_image');
        }))->get();
        //dd($friends_list);
        if(count($friends_list)>0)
        {
            foreach($friends_list as $raw)
            {
                if(count($raw->user_details->current_work_place)>0)
                $workplace=$raw->user_details->current_work_place->current_work_place_name;
                else
                $workplace='';
                //dd($raw->user_details);
                $data[]=array(
                    'full_name_of_friend'=>$raw->user_details->full_name,
                    'current_work_detail_and_location'=>$workplace,
                    'friend_profile_image'=>$raw->user_details->profile_image
            
            );
            }
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1'],'data'=>$data]); 
        }
        else
        {
            return response()->json(['status'=>FALSE,'msg'=>config('constants')['NO_FRIENDS']],400); 
        }
    }
    public function createPrivateEvent(Request $request)
    {
        $res = $this->validation($request,'user_id,event_image,title_of_event,event_type,event_tags,event_datetime,event_description,guest_can_invite');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        $event_location=json_decode($request->event_location);
        $co_host=json_decode($request->co_host);
       // dd($co_host);
        
        foreach($event_location as $raw)
        {
              $event_loc=explode('-',$raw->location_lat_long);
              $request->merge(['location_detail' => $raw->location_detail,'event_lat'=> $event_loc[0],'event_long'=> $event_loc[1]]);
        }
       // dd($co_host);
        $event_id=Event::insertGetId($request->except(['event_location','co_host']));
        if($event_id)
        {
            foreach($co_host as $raw_co_host)
            {
                   $raw_co_host_array[]=array('event_id'=>$event_id,
                                              'joined_user_id'=>$raw_co_host->user_id
                                             );
            }    
            Event_joined_user::insert($raw_co_host_array);
            $data=$this->eventDetails($event_id,'1');
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['EVENT_CREATED'],'data'=>$data]);
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
    }
    public function userPriveteEvents($id)
    {
        if(!$id){
            return response()->json(['status'=>FALSE,'msg'=>'user_id is missing'],400);
            }
        $user_events=Event::where('user_id',$id)->get();
        
        if(count($user_events)>0)
        {
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1'],'data'=>$user_events]); 
        }
        else
        {
            return response()->json(['status'=>FALSE,'msg'=>config('constants')['EVENT_EMPTY']],400); 
        }
    }
    public function eventDetails($id,$call=null)
    {
        if(!$id){
            return response()->json(['status'=>FALSE,'msg'=>'event_id is missing'],400);
            }

            $total_invited_persons=0;
            $total_going_persons=0;
            $total_maybe_persons=0;

        $user_events=Event::where('id',$id)->with('event_admin')->first();
        $co_host=Event_joined_user::where('event_id',$id)->with(array('co_host'=>function($query){
            $query->select('id','full_name','profile_image');
        }))->get();
        //dd($co_host);
        $coHost=array();
        if(count($co_host)>0)
        {
            foreach($co_host as $raw)
            {
              $total_invited_persons=$total_invited_persons+1;  
              if($raw->response_type=='1')
              { $total_going_persons=$total_going_persons+1; }

              if($raw->response_type=='3')
              {
                 $total_maybe_persons =$total_maybe_persons+1;
              }

              $coHost[]=array('id'=>$raw->co_host->id,'user_name'=>$raw->co_host->full_name);
            }
        }
        
        if(count($user_events)>0)
        {
            $eventlocation=array('location_detail'=>$user_events->location_detail,'location_lat_long'=>$user_events->event_lat.'-'.$user_events->event_long);
            if($user_events->event_type=='Private'){
            $data=array(
               'user_full_name'=>$user_events->event_admin->full_name,
               'event_image'=>$user_events->event_image,
               'title_of_event'=>$user_events->title_of_event,
               'event_type'=>$user_events->event_type,
               'event_tags'=>$user_events->event_tags,
               'event_datetime'=>$user_events->event_datetime,
               'event_location'=>$eventlocation,
               'event_description'=>$user_events->event_description,
               'co_host'=>$coHost,
               'total_going_persons'=>$total_going_persons,
               'total_maybe_persons'=>$total_maybe_persons,
               'total_invited_persons'=>$total_invited_persons
               
            );
            }
            if($user_events->event_type=='Public'){
                $event_host=Event::where('id',$id)->with('eventhost')->first();
            
                $data=array(
                   'user_full_name'=>$user_events->event_admin->full_name,
                   'host_full_name'=>$event_host->eventhost->full_name,
                   'event_image'=>$user_events->event_image,
                   'title_of_event'=>$user_events->title_of_event,
                   'event_type'=>$user_events->event_type,
                   'event_tags'=>$user_events->event_tags,
                   'event_datetime'=>$user_events->event_datetime,
                   'event_location'=>$eventlocation,
                   'event_description'=>$user_events->event_description,
                   'ticket_url'=>$user_events->ticket_url,
                   'approved_by_admin'=>$user_events->approved_by_admin,
                   'co_host'=>$coHost,
                   'total_going_persons'=>$total_going_persons,
                   'total_maybe_persons'=>$total_maybe_persons,
                   'total_invited_persons'=>$total_invited_persons
                   
                );
                }
                if($user_events->event_type=='Group'){
                    $event_host=Event::where('id',$id)->with('eventhost')->first();;
                    $data=array(
                       'user_full_name'=>$user_events->event_admin->full_name,
                       'host_full_name'=>$event_host->eventhost->full_name,
                       'event_image'=>$user_events->event_image,
                       'title_of_event'=>$user_events->title_of_event,
                       'event_type'=>$user_events->event_type,
                       'event_tags'=>$user_events->event_tags,
                       'event_datetime'=>$user_events->event_datetime,
                       'event_location'=>$eventlocation,
                       'event_description'=>$user_events->event_description,
                       'guest_can_invite'=>$user_events->guest_can_invite,
                       'co_host'=>$coHost,
                       'total_going_persons'=>$total_going_persons,
                       'total_maybe_persons'=>$total_maybe_persons,
                       'total_invited_persons'=>$total_invited_persons
                       
                    );
                    }
                    
            if(!$call)
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1'],'data'=>$data]); 
            else
            return $data;
        }
        else
        {
            return response()->json(['status'=>FALSE,'msg'=>config('constants')['EVENT_EMPTY']],400); 
        }
    }
    public function deleteEvent(Request $request)
    {
        $res = $this->validation($request,'event_id,user_id');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }  
        
        $event_details=Event::where('user_id',$request->user_id)->where('id',$request->event_id)->first();
        if($event_details){ $event_details->delete(); }
        if($event_details)
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]); 
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400); 
        
    }
    
    public function createPublicEvent(Request $request)
    {
        $res = $this->validation($request,'user_id,event_image,title_of_event,event_type,event_host,event_tags,event_datetime,event_description');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        $event_location=json_decode($request->event_location);
        $co_host=json_decode($request->co_host);
       // dd($co_host);
        
        foreach($event_location as $raw)
        {
              $event_loc=explode('-',$raw->location_lat_long);
              $request->merge(['location_detail' => $raw->location_detail,'event_lat'=> $event_loc[0],'event_long'=> $event_loc[1]]);
        }
        $request->merge(['approved_by_admin' => $request->post_or_event_approved_by_admin]);
       // dd($co_host);
        $event_id=Event::insertGetId($request->except(['event_location','co_host','post_or_event_approved_by_admin']));
        if($event_id)
        {
            foreach($co_host as $raw_co_host)
            {
                   $raw_co_host_array[]=array('event_id'=>$event_id,
                                              'joined_user_id'=>$raw_co_host->user_id
                                             );
            }    
            Event_joined_user::insert($raw_co_host_array);
            $data=$this->eventDetails($event_id,'1');
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['EVENT_CREATED'],'data'=>$data]);
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
    }
    public function createGroupEvent(Request $request)
    {
        $res = $this->validation($request,'user_id,event_image,title_of_event,event_type,event_host,event_tags,event_datetime,event_description,guest_can_invite');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        $event_location=json_decode($request->event_location);
        $co_host=json_decode($request->co_host);
       // dd($co_host);
        
        foreach($event_location as $raw)
        {
              $event_loc=explode('-',$raw->location_lat_long);
              $request->merge(['location_detail' => $raw->location_detail,'event_lat'=> $event_loc[0],'event_long'=> $event_loc[1]]);
        }
        $event_id=Event::insertGetId($request->except(['event_location','co_host']));
        if($event_id)
        {
            foreach($co_host as $raw_co_host)
            {
                   $raw_co_host_array[]=array('event_id'=>$event_id,
                                              'joined_user_id'=>$raw_co_host->user_id
                                             );
            }    
            Event_joined_user::insert($raw_co_host_array);
            $data=$this->eventDetails($event_id,'1');
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['EVENT_CREATED'],'data'=>$data]);
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
    }
    public function eventJoinResponse(Request $request)
    {
        $res = $this->validation($request,'event_id,user_id,response_type');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        $check_event=Event_joined_user::where('event_id',$request->event_id)->where('joined_user_id',$request->user_id)->first();
        if(count($check_event)>0)
        {
            $responce=Event_joined_user::where('event_id',$request->event_id)->where('joined_user_id',$request->user_id)->update($request->except(['event_id','user_id']));
            if($responce)
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]); 
            else
            return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400); 
        }
        else
        {
            return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400);  
        }
    }
    public function eventSearch(Request $request)
    {
        $res = $this->validation($request,'search_string');
        if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        $search_result=Event::where('title_of_event','LIKE',"%$request->search_string%")->get();
        if(count($search_result)>0)  
        {

        }
        else
        {

        }
        //dd($search_result);

    }
    public function followUser(Request $request)
    {
        $res = $this->validation($request,'user_id,follower_id');
        if($res){
            return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        $check_exist=Follows::where('user_id',$request->user_id)->where('follower_id',$request->follower_id)->first();
        if(!$check_exist)
        {
            Follows::insertGetId($request->all());
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]);
        }
        else
        {return response()->json(['status'=>FALSE,'msg'=>config('constants')['ALREADY_FOLLOWING']],400);}
    }

    public function unFollowUser(Request $request)
    {
        $res = $this->validation($request,'user_id,follower_id');
        if($res){
            return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        $check_exist=Follows::where('user_id',$request->user_id)->where('follower_id',$request->follower_id)->delete();
        if(count($check_exist)>0)
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]);
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400);
    }

    public function userFollower($id)
    {
        
        if(!$id){
            return response()->json(['status'=>FALSE,'msg'=>'user_id Missing'],400);
        }
        $listing=Follows::select('user_id','follower_id')->where('user_id',$id)->with(array('user_follower_details'=>function($query){
            $query->select('id','full_name','profile_image');
        }))->get();
        //dd($listing);
        if(count($listing)>0)
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1'],'data'=>$listing]);
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400);  
    }
    public function userFollowing($id)
    {
        
        if(!$id){
            return response()->json(['status'=>FALSE,'msg'=>'user_id Missing'],400);
        }
        $listing=Follows::select('user_id','follower_id')->where('follower_id',$id)->with(array('user_following_details'=>function($query){
            $query->select('id','full_name','profile_image');
        }))->get();
        //dd($listing);
        if(count($listing)>0)
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1'],'data'=>$listing]);
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400);  
    }
    public function searchUser(Request $request)
    {
        $res = $this->validation($request,'search_string');
        if($res){
            return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        
        $check_exist=User::select('id','full_name','profile_image')->where('full_name','LIKE',"%$request->search_string%")->orWhere('email',$request->search_string)->with('speciality')->get();
        //dd($check_exist);
        if(count($check_exist)>0)
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1'],'data'=>$check_exist]);
        else
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400);
    }
    ////////// Master country  API/////////////////////
    public function country(){
        return $this->core->success($this->core->country());
    }
     ////////// Master level API/////////////////////
    public function level(){
        return $this->core->success($this->core->level());
    }
     ////////// Master speciality API/////////////////////
    public function speciality(){
        return $this->core->success($this->core->speciality());
    }
 
    ////////// Master sub speciality API/////////////////////
    public function subSpeciality(){
        return $this->core->success($this->core->sub_speciality());
    }
     ////////// Master qualification listing API/////////////////////
    public function qualification(){
        return $this->core->success($this->core->qualification());
    }
     ////////// Master university listing API/////////////////////
    public function university(){
        return $this->core->success($this->core->university());
    }
     ////////// Master intership Listing API/////////////////////
    public function intership($id){
        if ( $id=='' ) {
            return response()->json(['status'=>FALSE,'msg'=>'Please Send university ID '],400);            
        }
        return $this->core->success($this->core->intership($id));
    }

/********** Start Groups Functions **********/

    public function addGroup(Request $request)
    {   
        $res = $this->validation($request,'name');
        if($res){
            return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        //Check group already exist
        if(Group::where('name',$request->name)->where('type','1')->count()){
            return response()->json(['status'=>FALSE,'msg'=>config('constants')['GROUP_ALREADY_EXIST']],400);
         }
         $request->merge(['type' => '1']);
        //Insert Group Details
        $lastGroupId = Group::insertGetId($request->all());
        if($lastGroupId){
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['GROUP_ADD']]);
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
    }

    public function addGroupMember(Request $request){
        $res = $this->validation($request,'group_id,user_ids');
        if($res){
            return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        $array = explode(',', $request->user_ids);
        $members = [];
        ///////delete old members if any/////////////
        GroupMember::whereIn('user_id',$array)->where('group_id',$request->group_id)->delete();
        ////////////////////////////////////////////
        foreach ($array as $key => $value) {
            if($request->$value==''){
                $members[]= ['group_id'=>$request->group_id,'user_id'=>$value];
            }
        }


        $rec = GroupMember::insert($members);
        if($rec){
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['GROUP_MEMBER_ADD']]);
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
    }

    public function getGroupById($id)
    {   
        $group_members=array();
        $group_post=array();
        $result=Group::whereId($id)->with('group_member','group_post')->first();
        //dd($result);
        if(count($result)>0)
        {
          foreach($result->group_member as $raw)
          {
              
            $group_members[]=array(
                'user_id'=>$raw->member_details->id,
                'name'=>$raw->member_details->full_name,
                'image'=>$raw->member_details->profile_image
            );
          }
          foreach($result->group_post as $raw)
          {
              
            $post_media=array();
            foreach($raw->group_post_media as $media)
            {
                $post_media[]=array('media_id'=>$media->id,'url'=>$media->group_media,'media_type'=>$media->media_type);
            }  
            $group_post[]=array(
                'post_id'=>$raw->id,
                'type'=>$raw->type,
                'location'=>$raw->location,
                'post_text'=>$raw->post_text,
                'created_at'=>$raw->created_at,
                'post_media'=>$post_media
         
            );
          }
             
          $data[]=array(
              'id'=>$result->id,
              'group_name'=>$result->name,
              'image'=>$result->image,
              'privacy'=>$result->privacy,
              'description'=>$result->description,
              'tags'=>$result->tags,
              'membership_approval'=>$result->membership_approval,
              'posting_permission'=>$result->posting_permission,
              'post_approval'=>$result->post_approval,
              'group_category_id'=>$result->group_category_id,
              'group_members'=>$group_members,
              'group_post'=>$group_post
          );
          
          return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS'],'data'=>$data]);
        }
        else
        {
            return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400);
        }
    }
    public function addGroupPost(Request $request)
    {   
        $post_id = GroupPost::insertGetId([
                'user_id'=>$request->user_id,
                'group_id'=>$request->group_id,
                'type'=>$request->post_type,
                'location'=>$request->location,
                'post_text'=>$request->post_text
            ]);
        if($request->post_type=='media')
        {
            if(!$request->media && $post_id>0){
                return response()->json(['status'=>FALSE,'msg'=>'Media is missing'],400);
            } else {
                $media=json_decode($request->media);
                foreach($media as $raw)
                {
                    $media_array[]=array(
                        'group_id'=>$request->group_id,
                        'media_type'=>$raw->type,
                        'group_media'=>$raw->media,
                        'group_post_id'=>$post_id
                    );
                }
                GroupMedia::insert($media_array);
            }
        }
        if($post_id){
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['POST_ADD_SUCCESS']]); 
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
       
    }
    public function exitGroup(Request $request)
    {
        GroupMember::where('user_id',$request->user_id)->where('group_id',$request->group_id)->delete();
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]);
    }

    
/********** End Groups Functions **********/

/***********community function Start ********/

    public function addCommunity(Request $request)
    {   
        $res = $this->validation($request,'name');
        if($res){
            return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        //Check group already exist
        if(Group::where('name',$request->name)->where('type','2')->count()){
            return response()->json(['status'=>FALSE,'msg'=>config('constants')['COMMUNITY_ALREADY_EXIST']],400);
         }
         $request->merge(['type' => '2']);
        //Insert Group Details
        $lastGroupId = Group::insertGetId($request->all());
        if($lastGroupId){
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['COMMUNITY_ADD']]);
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
    }

    public function addCommunityMember(Request $request)
    {
        $res = $this->validation($request,'community_id,user_ids');
        if($res){
            return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        
        $array = explode(',', $request->user_ids);
        $members = [];
        ///////delete old members if any/////////////
        GroupMember::whereIn('user_id',$array)->where('group_id',$request->community_id)->delete();
        ////////////////////////////////////////////
        foreach ($array as $key => $value) {
            if($request->$value==''){
                $members[]= ['group_id'=>$request->community_id,'user_id'=>$value];
            }
        }
        $rec = GroupMember::insert($members);
        if($rec){
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['COMMUNITY_MEMBER_ADD']]);
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
    }

    public function getCommunityById($id)
    {   
        $group_members=array();
        $group_post=array();
        $result=Group::whereId($id)->with('group_member','group_post')->first();
        //dd($result);
        if(count($result)>0)
        {
          foreach($result->group_member as $raw)
          {
            $group_members[]=array(
                'user_id'=>$raw->member_details->id,
                'name'=>$raw->member_details->full_name,
                'image'=>$raw->member_details->profile_image
            );
          }
          foreach($result->group_post as $raw)
          {
              
            $post_media=array();
            foreach($raw->group_post_media as $media)
            {
                $post_media[]=array('media_id'=>$media->id,'url'=>$media->group_media,'media_type'=>$media->media_type);
            }  
            $group_post[]=array(
                'post_id'=>$raw->id,
                'type'=>$raw->type,
                'location'=>$raw->location,
                'post_text'=>$raw->post_text,
                'created_at'=>$raw->created_at,
                'post_media'=>$post_media
         
            );
          }
             
          $data[]=array(
              'id'=>$result->id,
              'community_name'=>$result->name,
              'image'=>$result->image,
              'privacy'=>$result->privacy,
              'description'=>$result->description,
              'tags'=>$result->tags,
              'membership_approval'=>$result->membership_approval,
              'posting_permission'=>$result->posting_permission,
              'post_approval'=>$result->post_approval,
              'group_category_id'=>$result->group_category_id,
              'community_members'=>$group_members,
              'community_post'=>$group_post
          );
          
          return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS'],'data'=>$data]);
        }
        else
        {
            return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400);
        }  
    }
    public function addCommunityPost(Request $request)
    {   
        $post_id = GroupPost::insertGetId([
                'user_id'=>$request->user_id,
                'group_id'=>$request->community_id,
                'type'=>$request->post_type,
                'location'=>$request->location,
                'post_text'=>$request->post_text
            ]);
        if($request->post_type=='media')
        {
            if(!$request->media && $post_id>0){
                return response()->json(['status'=>FALSE,'msg'=>'Media is missing'],400);
            } else {
                $media=json_decode($request->media);
                foreach($media as $raw)
                {
                    $media_array[]=array(
                        'group_id'=>$request->community_id,
                        'media_type'=>$raw->type,
                        'group_media'=>$raw->media,
                        'group_post_id'=>$post_id
                    );
                }
                GroupMedia::insert($media_array);
            }
        }
        if($post_id){
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['POST_ADD_SUCCESS']]); 
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
       
    }
/*************community function End**** */

/********** Start Jobs Functions **********/

    public function addJob(Request $request)
    {   
        $res = $this->validation($request,'user_id,category_id,title,description');
        if($res){
            return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        $today = date('Y-m-d H:i:s');
        $request->merge(['start_date'=>$today, 'end_date'=>date('Y-m-d H:i:s',strtotime($today ."+ 30 days"))]);
        $lastId = JobPost::insertGetId($request->except(['job_logo','job_education']));
        if($lastId){
            if($request->job_education){
                $education =json_decode($request->job_education);
                foreach($education as $raw)
                {
                    $edu_array[]=[
                        'job_id'=>$lastId,
                        'degree_id'=>$raw->degree_id,
                    ];
                }
                JobEducation::insert($edu_array);
            }
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['JOB_ADD_SUCCESS']]);
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
    }
    
    public function getAllJobByUser(Request $request)
    {
        $jobs = JobPost::with('job_category','job_education','applied_users')->where('user_id',$request->user_id)->whereStatus(1);
        $data['job_list'] =  $jobs->get();
        $data['total_jobs'] =  $jobs->count();
        return response()->json(['status'=>TRUE,'msg'=>'','data'=>$data]);
    }

    public function getJobById(Request $request)
    {
        $data['job_details'] = JobPost::with('job_category','job_education')->where('id',$request->job_id)->whereStatus(1)->first();
        return response()->json(['status'=>TRUE,'msg'=>'','data'=>$data]);
    }

    public function pauseJob(Request $request)
    {
        if(JobPost::where('id',$request->job_id)->update(['status'=>0])){
            return response()->json(['status'=>TRUE,'msg'=>'Job Pause Successfully.']);
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
    }

    public function deleteJob(Request $request)
    {
        if(JobPost::where('id',$request->job_id)->delete()){
            return response()->json(['status'=>TRUE,'msg'=>'Job Delete Successfully.']);
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
    }
    public function applyJob(Request $request)
    {
        $res = $this->validation($request,'user_id,job_id');
        if($res){
            return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        $lastId = JobApplyUser::insertGetId(['job_id'=>$request->job_id,'user_id'=>$request->user_id]);
        if($lastId){
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['JOB_APPLY_SUCCESS']]);
        }
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
    }

/********** End Jobs Functions **********/



/******************* Market Place Function  **************************/

public function addMarketplaceProduct(Request $request)
{
    $res = $this->validation($request,'seller_id,product_title,market_place_category_id,priec,location');
    if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
    }

    $response=Marketplace::insertGetId($request->all());
    if($response)
    return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]);
    else
    return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
}
public function deleteMarketplaceProduct(Request $request)
{
    $res = $this->validation($request,'seller_id,product_id');
    if($res){
        return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
    }

    if(Marketplace::where('seller_id',$request->seller_id)->where('id',$request->product_id)->delete())
    return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1']]);
    else
    return response()->json(['status'=>FALSE,'msg'=>config('constants')['SERVER_ERROR']],400);
}
public function marketPlaceCategory()
{
     $category=Marketplace_category::all();
     if(count($category)>0)
     return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS'],'data'=>$category]);
     else
     return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400);

}
public function marketPlace()
{
     $marketplace=Marketplace::with('product_single_image')->get();
     if(count($marketplace)>0)
     return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS'],'data'=>$marketplace]);
     else
     return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400);

}
public function marketPlaceByCategory(Request $request)
{
     $array = explode(',', $request->category_id);
     $marketplace=Marketplace::whereIn('market_place_category_id',$array)->with('product_single_image')->get();
     if(count($marketplace)>0)
     return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS'],'data'=>$marketplace]);
     else
     return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400);

}
public function marketPlaceProductDetails($id)
{
     //$array = explode(',', $request->category_id);
     $marketplace=Marketplace::where('id',$id)->with('product_image')->first();
     //dd($marketplace);
     if(count($marketplace)>0)
     return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS'],'data'=>$marketplace]);
     else
     return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400);

}

/******************************************************************* */

/**********************Librery function ***************************/

public function librerySearch(Request $request)
{

    $search_string='';
    $category_id='';
    $access_type='';
    $item_type='';
    $language='';
    $publication='';
    $journal='';
    $isbn='';
    $short_by='';
    if($request->search_string)
    {
        $search_string="title LIKE '%$request->search_string%'";
    }
    if($request->category_id!='')
    {
        $category_id=" and category_id='$request->category_id'";
    }
    if($request->access_type!='')
    {
        $access_type=" and access_type='$request->access_type'";
    }
    if($request->item_type!='')
    {
        
        $array=explode(',', $request->item_type);
         $array = implode("','",$array);
         $item_type=" and item_type IN ('$array')";

    }
    if($request->language!='')
    {
        $language=" and language='$request->language'";
    }
    if($request->publication_date_form!='' && $request->publication_date_to!='')
    {
        $publication=" and publication_from >='$request->publication_date_form' and publication_to <= '$request->publication_date_to'";
    }
    if($request->journal!='')
    {
        $journal=" and journal='$request->journal'";
    }
    if($request->isbn!='')
    {
        $isbn=" and isbn='$request->isbn'";
    }
    if($request->short_by!='')
    {
        if($request->short_by=='1')
        $short_by=" order by read_count DESC";
        if($request->short_by=='2')
        $short_by=" order by created_at DESC";
        if($request->short_by=='3')
        $short_by=" order by created_at ASC";
    }
    $search = DB::select("select * from libreries where $search_string $category_id $access_type $item_type $language $publication $journal $isbn $short_by");
  //dd("select * from libreries where $search_string $category_id $access_type $item_type $language $publication $journal $isbn $short_by");
    if(count($search)>0)
    return response()->json(['status'=>TRUE,'msg'=>config('constants')['SUCCESS1'],'data'=>$search]);
    else
    return response()->json(['status'=>FALSE,'msg'=>config('constants')['NOT_FOUND']],400);
}


 ///////// get user profile by email and mobile API //////////////////
    public function getUserProfile(Request $request){
        $res = $this->validation($request,'user_key');
        if($res){
            return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        $user = User::where('mobile',$request->user_key)->orWhere('email',$request->user_key)->first();
        if($user){
            $email_verified=$mobile_verified=$user->profile_completed=$profile_completed=true;
                if($user->email_verified==0)
                    $email_verified=false;
                if($user->mobile_verified==0)
                    $mobile_verified=false;
                if($user->profile_completed==0)
                    $profile_completed=false;

                $data=array(
                    'id'=>$user->id,
                    'full_name'=>$user->full_name,
                    'dob'=>$user->dob,
                    'email'=>$user->email,
                    'user_name'=>$user->user_name,
                    'login_type'=>$user->login_type,
                    'profile_image'=>$user->profile_image,
                    'cover_image'=>$user->cover_image,
                    'mobile'=>$user->mobile,
                    'device_type'=>$user->device_type,
                    'device_token'=>$user->device_token,
                    'status'=>$user->status,
                    'country_id'=>$user->country_id,
                    'level_id'=>$user->level_id,
                    'specialitie_id'=>$user->specialitie_id,
                    'profession_id'=>$user->profession_id,
                    'profession_skill'=>$user->profession_skill,
                    'about_me'=>$user->about_me,
                    'email_verified'=>$email_verified,
                    'mobile_verified'=>$mobile_verified,
                    'profile_completed'=>$profile_completed,
                    'company_registration_number'=>$user->company_registration_number,
                    'remember_token'=>$user->remember_token
                    
                );
                return response()->json(['status'=>TRUE,'msg'=>'','data'=>$data]);
        } else {
            return response()->json(['status'=>FALSE,'msg'=>config('constants')['INVALID_USERNAME_OR_EMAIL']],400);
        }
    }

    ///////// send user otp in email API //////////////////
    public function sendResetPasswordOTP(Request $request){
        $res = $this->validation($request,'user_id');
        if($res){
            return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        $uData = User::whereId($request->user_id)->first();
        if($uData){
            $otp=$this->generateRandomString();
            if(User::whereId($request->user_id)->update(['otp'=>$otp])){
                $mailData['params']=['email'=>$uData->email,'user_id'=>$uData->id,'user_name'=>$uData->full_name,'otp'=>$otp,'subject'=>'MeddiFellow Reset Password OTP','template'=>'reset_password_otp'];
                $this->core->SendEmail($mailData);
                return response()->json(['status'=>TRUE,'msg'=>config('constants')['OTP_EMAIL_SEND']]);
            }
        } 
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['INVALID_USER']],400);

    }
     ///////// update user new password API //////////////////
    public function updateNewPassword(Request $request){
        $res = $this->validation($request,'otp,password');
        if($res){
            return response()->json(['status'=>FALSE,'msg'=>implode ('/n ',$res)],400);
        }
        $uData = User::whereId($request->user_id)->where('otp',$request->otp)->first();
        if($uData){
            User::whereId($request->user_id)->update(['password'=>Hash::make($request->password)]);
             $mailData['params']=['email'=>$uData->email,'user_id'=>$uData->id,'user_name'=>$uData->full_name,'subject'=>'MeddiFellow Password Update','template'=>'update_password_success'];
            $this->core->SendEmail($mailData);
            return response()->json(['status'=>TRUE,'msg'=>config('constants')['PASSWORD_UPDATED']]);
        } 
        return response()->json(['status'=>FALSE,'msg'=>config('constants')['INVALID_OTP']],400);
    }

    ////////// Is Email Unique API/////////////////////
    public function isEmailUnique(Request $request){
        if(User::where('email',$request->email)->count()){
           return response()->json(['status'=>FALSE,'msg'=>config('constants')['EMAIL_ALREADY_EXIST']]);
        }else{
           return response()->json(['status'=>TRUE,'msg'=>config('constants')['EMAIL_AVILABLE']],400);
        }
    }

    ///////////////////////////////////////General Functions//////////////////////////////////////////////////
    private function generateRandomString($length = 5){
        $characters = '12345678998765432145789129862275634236578686523546'.time();
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    private function getUnusedRandomFileName($extention) {
        return md5(microtime()) .'.'. $extention;
    }
    public function mc_encrypt($string) {
        if(!$string){return false;}
        $encrypt_method = "AES-256-CBC";
        $key = hash('sha256', 'hVTCI4j9H7PlQz0F');
        $iv = substr(hash('sha256', 'goforsys@#123'), 0, 16);
        $output = trim($this->safe_b64encode(openssl_encrypt($string, $encrypt_method, $key, 0, $iv)));    
        return $output; 
     }
     public function mc_decrypt($string) {
         if(!$string){return false;}
         $encrypt_method = "AES-256-CBC";
         $key = hash('sha256', 'hVTCI4j9H7PlQz0F');
         $iv = substr(hash('sha256', 'goforsys@#123'), 0, 16);    
         $output = trim(openssl_decrypt($this->safe_b64decode($string), $encrypt_method, $key, 0, $iv));
         return $output; 
     }
     public function safe_b64encode($string) {
        $data = base64_encode($string);
        $data = str_replace(array('+','/','='),array('-','_',''),$data);
        return $data;
    }
    public function safe_b64decode($string) {
        $data = str_replace(array('-','_'),array('+','/'),$string);
        $mod4 = strlen($data) % 4;
        if ($mod4) {
            $data .= substr('====', $mod4);
        }
        return base64_decode($data);
    }
    

}
