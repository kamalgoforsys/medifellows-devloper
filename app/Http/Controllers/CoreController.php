<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use PHPMailerAutoload; 
use PHPMailer\PHPMailer\PHPMailer;
use Mail;
use App\Country,App\Level,App\Speciality,App\Sub_speciality,App\Qualification,App\University,App\Intership;
use Auth,DB,Hash;
class CoreController extends Controller
{
    public function __construct(){}
    public function error($msg){
        return response()->json(['status'=>201,'msg'=>$msg],'201');
    }
    public function success($data,$msg=null){
        if($msg!='')
            return response()->json(['status'=>TRUE,'msg'=>$msg,'data'=>$data],'200');
            else
        return response()->json(['status'=>TRUE,'msg'=>config('constants')['LISTING_GET'],'data'=>$data],'200');
    }

    public function SendEmail($data){
        
        $mail = $this->mailConfig();
        $mail->addAddress($data['params']['email']);
        $mail->Subject = $data['params']['subject'];
        if(isset($data['params']['template']) && $data['params']['template']!=''){
            $mail->Body = view('email_templates/'.$data['params']['template'],$data);
        } else {
            $mail->Body = $data['params']['msg'];
        }
        $mail->isHTML(true);
        if(!$mail->Send()) {            
           $res = FALSE;            
        } else {
            $res = TRUE;
        }
        //dd($res);
        return $res;
     }

     function mailConfig(){
        $mail = new PHPMailer;
        $mail->isSMTP();        
        //$mail->SMTPDebug=1;        
        $mail->SMTPAuth = true;
        $mail->Host = config('constants.email_config')['host'];
        $mail->Username = config('constants.email_config')['username'];
        $mail->Password = config('constants.email_config')['password'];
        $mail->SMTPSecure = config('constants.email_config')['smtp_secure'];
        $mail->Port = '465';
        $mail->From = config('constants.email_config')['from_email'];
        $mail->FromName = config('constants.email_config')['from_name'];
        $mail->addReplyTo(config('constants.email_config')['reply_to'], 'Email Notification');  
        return $mail;
    }
    public function country(){
        $country = Country::get();
        return $country;
    }
    public function level(){
        $level = Level::get();
        return $level;
    }
    public function speciality(){
        $speciality = Speciality::get();
        return $speciality;
    }
    public function sub_speciality(){
        $sub_speciality = Sub_speciality::get();
        return $sub_speciality;
    }
    public function qualification(){
        $qualification = Qualification::get();
        return $qualification;
    }
    public function university(){
        $university = University::get();
        return $university;
    }
    public function intership($id){
        $intership = Intership::where('university_id',$id)->get();
        return $intership;
    }
    
    public function city(){
        $city = City::with('area')->where('status','Enable')->get();
        return $city;
    }
    
    public function address($customer_id){
        return DB::table('address as a')
                        ->where('customer_id',$customer_id)
                        ->join('cities as c','c.id','=','a.city_id')
                        ->join('area','area.id','=','a.area_id')
                        ->select('a.*','c.city','area.area')
                        ->get();
    }
    public function passwordMatch($user_id,$string){
        $data['user'] = Customer::find($user_id);
        if($data['user']){
            if(password_verify($string , $data['user']->password))
                return $data;
            else
                return false;
        }
        else
            return false;
    }
    public function checkCustomerExist($key){
        $customer=Customer::where('mobile',$key)->first();
        if(!empty($customer))
        {
            if($customer->status=='Enable')
                $data['enable'] = $customer;
            else
                $data['disabled'] = $customer;
            return $data;
        }
        else
            return NULL;
    }
    public function updateDevice($id,$data){
        $dataArray['device_type'] = $data['device_type'];
        $dataArray['token'] = $data['token'];
        return Customer::where(['id'=>$id])->update($dataArray);
    }
    public function getCustomerById($id){
        return Customer::find($id);
    }
    public function cityIdByName($name){
        $cityData = City::where('city','LIKE',$name)->where('status','Enable')->first(['id']);
        if($cityData && $name!=null)
            return $cityData->id;
        return FALSE;
    }
    
}
