<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class Post_tag extends Model
{
    protected $table='post_tag';

    public function post_info(){
        return $this->belongsTo('App\User_post','post_id','id')->with('user_info','post_media_details');    
        
    }
    public function like_count()
    {
        return $this->hasMany('App\Post_meta','post_id','id')->where('type','Like');
    }
    public function is_like()
    {
        return $this->hasMany('App\Post_meta','post_id','id')->where('type','Like');
    }
    public function comment_count()
    {
        return $this->hasMany('App\Post_meta','post_id','id')->where('type','Comment');
    }
    public function share_count()
    {
        return $this->hasMany('App\Post_meta','post_id','id')->where('type','Share');
    }
}
