<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class Follows extends Model
{
    public function user_follower_details(){
        return $this->hasOne('App\User','id','follower_id');
    }
    public function user_following_details(){
        return $this->hasOne('App\User','id','user_id');
    }
}
