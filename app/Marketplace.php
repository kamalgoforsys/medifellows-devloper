<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class Marketplace extends Model
{
    protected $table='market_place_product';
    //public $with=['user_info'];
    public function product_image(){
        return $this->hasMany('App\Marketplace_gallery','product_id','id');  
        
    }
    public function product_single_image(){
        return $this->hasOne('App\Marketplace_gallery','product_id','id');  
        
    }
    public function user_info(){
        return $this->belongsTo('App\User','user_id','id');  
        
    }
    public function user_info_with(){
        return $this->belongsTo('App\User','user_id','id')->with('speciality','user_sub_speciality');  
        
    }
    public function product_view_count(){
        return $this->hasMany('App\Marketplace_count','marketplace_id','id');       
    }
    public function marketplace_saved(){
        return $this->hasMany('App\Marketplace_saved','marketplace_id','id');       
    }
}
