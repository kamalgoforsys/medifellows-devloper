<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class Profile_muted_group extends Model
{
    protected $table='profile_muted_group';
    protected $fillable=['user_id', 'group_id', 'home', 'notification', 'any', 'from_follow', 'expire', 'expire_type'];
    public function group_info(){
        return $this->belongsTo('App\Group','group_id','id');    
        
    }
    
}

