<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class JobPost extends Model
{
	//public $with=['owner'];

    public function owner(){
        return $this->hasOne('App\User','id','user_id');         
    }
    public function job_category(){
        return $this->hasOne('App\JobCategory','id','category_id');          
    }
    public function job_employment_type(){
        return $this->hasOne('App\Job_employment_type','id','employement_type');          
    }
    public function job_seniority_level(){
        return $this->hasOne('App\Job_seniority_level','id','seniority_level');          
    }
    public function job_education(){
        return $this->hasMany('App\JobEducation','job_id','id')->with('qualification');       
    }
    public function applied_users(){
        return $this->hasMany('App\JobApplyUser','job_id','id')->with('user');         
    }
    public function job_view_count(){
        return $this->hasMany('App\Job_count','job_id','id');       
    }
    public function job_saved(){
        return $this->hasMany('App\Job_saved','job_id','id');       
    }
}
