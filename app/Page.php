<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class Page extends Model
{
    public function page_admin(){
        return $this->belongsTo('App\User','user_id','id');  
        
    }
    public function page_post(){
        return $this->hasMany('App\PagePost')->with('page_post_media','user_info');  
        
    }
    
}
