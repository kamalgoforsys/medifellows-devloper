<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class Event_co_hosts extends Model
{
    public function co_host(){
        return $this->belongsTo('App\User','co_host_id','id');  
        
    }
}
