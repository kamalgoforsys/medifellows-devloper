<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class User_friend extends Model
{
    public function user_details(){
        return $this->hasOne('App\User','id','friend_id')->with('speciality');
    }
    
}
