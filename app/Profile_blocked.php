<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class Profile_blocked extends Model
{
    protected $table='profile_blocked';
    public function user_info(){
        return $this->belongsTo('App\User','block_user_id','id');    
        
    }
}
