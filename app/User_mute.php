<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class User_mute extends Model
{
    protected $table='user_mute';
    public function user_info(){
        return $this->belongsTo('App\User','mute_user_id','id');    
        
    }
}
