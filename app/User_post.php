<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class User_post extends Model
{
 
    public function post_media(){
        return $this->hasMany('App\User_media');  
        
    }
    public function post_media_details(){
        return $this->hasMany('App\User_media');  
        
    }
    public function user_info(){
        return $this->belongsTo('App\User','user_id','id')->select('id','full_name','profile_image');    
        
    }
    public function user_info_friend(){
        return $this->belongsTo('App\User','friend_id','id')->select('id','full_name','profile_image');    
        
    }
    public function like_count()
    {
        return $this->hasMany('App\Post_meta','post_id','id')->where('type','Like');
    }
    public function is_like()
    {
        return $this->hasMany('App\Post_meta','post_id','id')->where('type','Like');
    }
    public function comment_count()
    {
        return $this->hasMany('App\Post_meta','post_id','id')->where('type','Comment');
    }
    public function share_count()
    {
        return $this->hasMany('App\Post_meta','post_id','id')->where("type","Share");
    }
}
