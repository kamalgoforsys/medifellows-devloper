<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class User_media extends Model
{
    public function album(){
        return $this->hasOne('App\User_album','id','album_id');
    }
}
