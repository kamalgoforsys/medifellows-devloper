<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class User_block extends Model
{
    protected $table='user_block';
    public function user_info(){
        return $this->belongsTo('App\User','block_user_id','id');    
        
    }
}
