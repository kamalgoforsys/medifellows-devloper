<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class Profile_muted extends Model
{
    protected $table='profile_muted';
    protected $fillable=['user_id', 'mute_user_id', 'home', 'notification', 'any', 'from_follow', 'expire', 'expire_type'];
    public function user_info(){
        return $this->belongsTo('App\User','mute_user_id','id');    
        
    }
    
}

