<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class User_qualification extends Model
{
    public function qualification(){
        return $this->hasMany('App\Qualification','id','qualification_id');  
        
    }
    public function speciality(){
        return $this->hasMany('App\Speciality','id','speciality_id');  
        
    }
}
