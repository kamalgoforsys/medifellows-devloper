<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class Group_meta extends Model
{
    protected $fillable=['type','post_user_id','user_id','post_id','comment_text'];
   
    public function post_info(){
        return $this->belongsTo('App\GroupPost','post_id','id')->with('user_info','group_post_media');    
        
    }
    public function like_count()
    {
        return $this->hasMany('App\Group_meta','post_id','id')->where('type','Like');
    }
   
    public function comment_count()
    {
        return $this->hasMany('App\Group_meta','post_id','id')->where('type','Comment');
    }
    public function share_count()
    {
        return $this->hasMany('App\Group_meta','post_id','id')->where('type','Share');
    }
    
    public function like_user_info(){
        return $this->belongsTo('App\User','user_id','id')->with('user_title');  
        
    }
    
    public function comment_user_info()
    {
        return $this->belongsTo('App\User','user_id','id');    
    }

    public function is_friend()
    {
        return $this->belongsTo('App\User_friend','user_id','friend_id')->where('status','Accepted');    
    }

    public function comment_like(){
        return $this->hasMany('App\Group_comment_meta','comment_id','id')->where('type','Like');  
        
    }
    public function comment_reply(){
        return $this->hasMany('App\Group_comment_meta','comment_id','id')->where('type','Reply')->with('user_info');  
        
    }
    public function is_liked()
    {
        return $this->hasMany('App\Group_meta','post_id','id')->where('type','Like');
    }
    public function is_like()
    {
        return $this->belongsTo('App\Group_comment_meta','id','comment_id')->where('type','Like');    
    }

}
