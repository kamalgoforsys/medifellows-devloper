<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class Group_report extends Model
{
    public function posted_by(){
        return $this->belongsTo('App\User','post_user_id','id');    
        
    }
    public function reported_by(){
        return $this->belongsTo('App\User','report_by_id','id');    
        
    }
    public function post(){
        return $this->belongsTo('App\GroupPost','post_id','id');    
        
    }
}
