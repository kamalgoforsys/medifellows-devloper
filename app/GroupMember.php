<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class GroupMember extends Model
{
    //public $with=['member_details'];
    public function member_details(){
        return $this->belongsTo('App\User','user_id','id');    
        
    }
    public function group_details(){
        return $this->belongsTo('App\Group','group_id','id')->select('id','image','title','description');    
        
    }
    public function blocked(){
        return $this->hasOne('App\Group_blocked','user_id','user_id');
    }
    public function muted(){
        return $this->hasOne('App\Group_mute','user_id','user_id');
    }
}
