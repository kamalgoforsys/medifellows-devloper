<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $fillable = [
         'title,full_name,first_name','last_name','country_id','email','user_name','login_type','profile_image','cover_image','mobile','device_type','device_token','status','profession_id','profession_skill','leavel','about_me','company_registration_number','remember_token'
     ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
     protected $hidden = [
         'password',
     ];
     public function user_sub_speciality(){
        return $this->hasMany('App\User_sub_speciality')->with('sub_speciality');  
        
    }
    public function user_title(){
        return $this->hasOne('App\Title','id','title');
    }
    
    public function speciality(){
        return $this->hasOne('App\Speciality','id','specialitie_id');
    }
    
    public function address(){
        return $this->hasOne('App\Country','id','country_id');
    }
    public function user_qualification(){
        return $this->hasMany('App\User_qualification')->with('qualification','speciality');  
        
    }
    public function current_work_place(){
        return $this->hasMany('App\User_workspace')->where('status','1');
    }
    public function user_website(){
        return $this->hasMany('App\User_contact_detail')->where('meta_type','Website');  
        
    }
    public function user_mobile(){
        return $this->hasMany('App\User_contact_detail')->where('meta_type','Phone');  
        
    }
    public function user_level(){
        return $this->hasOne('App\Level','id','level_id');  
        
    }
    public function user_intern_infos(){
        return $this->hasMany('App\User_intern_info');
    }
    public function field_of_interest(){
        return $this->hasMany('App\Field_of_interest')->with('field');
    }


}
