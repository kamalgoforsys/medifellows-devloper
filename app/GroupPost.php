<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class GroupPost extends Model
{
    protected $table='group_posts';
    public $with=['group_post_media'];
    
    public function group_post_media(){
        return $this->hasMany('App\GroupMedia','group_post_id','id');  
        
    }
    public function group_info(){
        return $this->belongsTo('App\Group','group_id','id')->select('id','image','title');    
        
    }
    public function user_info(){
        return $this->belongsTo('App\User','user_id','id')->select('id','full_name','profile_image');    
        
    }
    public function user_info_friend(){
        return $this->belongsTo('App\User','friend_id','id')->select('id','full_name','profile_image');    
        
    }
    public function like_count()
    {
        return $this->hasMany('App\Group_meta','post_id','id')->where('type','Like');
    }
    public function is_like()
    {
        return $this->hasMany('App\Group_meta','post_id','id')->where('type','Like');
    }
    public function comment_count()
    {
        return $this->hasMany('App\Group_meta','post_id','id')->where('type','Comment');
    }
    public function share_count()
    {
        return $this->hasMany('App\Group_meta','post_id','id')->where('type','Share');
    }
}
