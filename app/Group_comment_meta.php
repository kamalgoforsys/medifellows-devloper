<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class Group_comment_meta extends Model
{
    protected $fillable=['type','user_id','comment_id','text'];
    //post_meta
    public function user_info(){
        return $this->belongsTo('App\User','user_id','id')->select('id','profile_image','full_name');  
        
    }
    

}
