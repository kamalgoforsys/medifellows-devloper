<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class Page_review extends Model
{
    protected $fillable=['user_id','page_id','review'];
}
