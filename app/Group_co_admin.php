<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class Group_co_admin extends Model
{
    protected $table='groups_co_admin';
    //public $with=['member_details'];
    public function member_details(){
        return $this->belongsTo('App\User','user_id','id');    
        
    }
}
