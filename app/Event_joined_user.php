<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class Event_joined_user extends Model
{
    public function joined_user(){
        return $this->belongsTo('App\User','joined_user_id','id');  
        
    }
    public function host(){
        return $this->belongsTo('App\User','joined_by','id');  
        
    }
    public function event_details(){
        return $this->belongsTo('App\Event','event_id','id');  
        
    }
}
