<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class EventPost extends Model
{
    protected $table='event_posts';
    public $with=['event_post_media'];
    
    public function event_post_media(){
        return $this->hasMany('App\EventMedia','event_post_id','id');  
        
    }
    public function event_info(){
        return $this->belongsTo('App\Event','event_id','id')->select('id','event_image','title_of_event');    
        
    }
    public function user_info(){
        return $this->belongsTo('App\User','user_id','id')->select('id','full_name','profile_image');    
        
    }
    public function user_info_friend(){
        return $this->belongsTo('App\User','friend_id','id')->select('id','full_name','profile_image');    
        
    }
    public function like_count()
    {
        return $this->hasMany('App\Event_meta','post_id','id')->where('type','Like');
    }
    public function is_like()
    {
        return $this->hasMany('App\Event_meta','post_id','id')->where('type','Like');
    }
    public function comment_count()
    {
        return $this->hasMany('App\Event_meta','post_id','id')->where('type','Comment');
    }
    public function share_count()
    {
        return $this->hasMany('App\Event_meta','post_id','id')->where('type','Share');
    }
}
