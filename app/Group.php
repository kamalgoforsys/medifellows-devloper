<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class Group extends Model
{
    public function group_admin(){
        return $this->belongsTo('App\User','user_id','id');  
        
    }
    public function group_member(){
        return $this->hasMany('App\GroupMember','group_id','id')->where('role','Members')->where('status','Joined');  
        
    }
    public function group_post(){
        return $this->hasMany('App\GroupPost')->with('group_post_media','user_info');  
        
    }
    public function group_moderators(){
        return $this->hasMany('App\GroupMember','group_id','id')->where('role','Moderator')->with('member_details');  
        
    }
}
