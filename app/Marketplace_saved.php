<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class Marketplace_saved extends Model
{
    protected $table='marketplace_saved';
    public function product_single_image(){
        return $this->hasOne('App\Marketplace_gallery','product_id','marketplace_id');  
        
    }
    public function user_info(){
        return $this->belongsTo('App\User','marketplace_user_id','id')->with('address');  
        
    }
    public function marketplace_info(){
        return $this->belongsTo('App\Marketplace','marketplace_id','id');  
        
    }
}
