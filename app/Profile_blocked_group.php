<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class Profile_blocked_group extends Model
{
    protected $table='profile_blocked_group';
    public function group_info(){
        return $this->belongsTo('App\Group','group_id','id');    
        
    }
}
