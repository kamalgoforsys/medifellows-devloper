<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class JobEducation extends Model
{
	public function qualification(){
        return $this->hasMany('App\Qualification','id','degree_id');  
        
    }
}
