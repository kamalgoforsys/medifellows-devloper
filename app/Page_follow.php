<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class Page_follow extends Model
{
    protected $table='page_follows';
    protected $fillable=['user_id','page_id'];
}
