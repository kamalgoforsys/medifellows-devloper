<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class PagePost extends Model
{
    protected $table='page_posts';
    public $with=['page_post_media'];
    
    public function page_post_media(){
        return $this->hasMany('App\Page_media','page_post_id','id');  
        
    }
    public function page_info(){
        return $this->belongsTo('App\Page','page_id','id')->select('id','image','title');    
        
    }
    public function user_info(){
        return $this->belongsTo('App\User','user_id','id')->select('id','full_name','profile_image');    
        
    }
    public function user_info_friend(){
        return $this->belongsTo('App\User','friend_id','id')->select('id','full_name','profile_image');    
        
    }
    public function like_count()
    {
        return $this->hasMany('App\Page_meta','post_id','id')->where('type','Like');
    }
    public function is_like()
    {
        return $this->hasMany('App\Page_meta','post_id','id')->where('type','Like');
    }
    public function comment_count()
    {
        return $this->hasMany('App\Page_meta','post_id','id')->where('type','Comment');
    }
    public function share_count()
    {
        return $this->hasMany('App\Page_meta','post_id','id')->where('type','Share');
    }
}
