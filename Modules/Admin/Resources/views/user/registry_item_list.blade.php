@extends('admin::layouts.master')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Registry Item List

            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Registry</a></li>
                <li class="active">Registry Item List</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Registry Item List</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                                title="Collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                        <table id="example" class="table table-striped table-bordered nowrap" cellspacing="0" width="100%">
        <thead>
            <tr>
            <tr>
                <th>Category</th>
                <th>Item</th>
                <th>Price</th>
                <th>Qty</th>
                <th>Status</th>

            </tr>
            </tr>
        </thead>
        <tbody>
        @foreach($registry_item as $data)
        <tr>
                <td>{{$data->product->category->cat_name}}</td>
                <td>{{$data->product->product_name}}</td>
                <td>{{$data->product->product_price}}</td>
                <td>{{$data->qty}}</td>
                <td>{{$data->status}}</td>
                            </tr>
        @endforeach
        </tbody>
        </table>
                </div>
            </div>
            <!-- /.box -->

        </section>
        <!-- /.content -->
    </div>

@endsection
