@extends('admin::layouts.master')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Registry List

      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Registry</a></li>
        <li class="active">Registry List</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Registry List</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif


        <div class="box-body">
            <table class="table table-striped">
                <tbody>
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Name</th>
                  <th>Date</th>
                  <th>Created_at</th>
                  <th>Action</th>
                </tr>
                @php $i=1;@endphp
                @foreach($registry as $class)
                    <tr>
                        <td>{{$i++}}</td>
                        <td>{{$class->name}}</td>
                        <td>{{$class->date_from}}- {{$class->date_to}}</td>
                        <td> {{ $class->created_at }}</td>
                        <td>
                        <a class="btn btn-default btn-sm " href="{{url('admin/registry_item_list',$class->id)}}">Item List</a>
                        <a class="btn btn-default btn-sm " href="{{url('admin/check_list',$class->id)}}">Check List</a>
                        </td>

                    </tr>
                @endforeach
            </tbody>
        </table>

        </div>
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
<script>
    $(document).on('click','.change_status',function(){
        if(confirm('Are You Sure ?'))
        {
            var id=$(this).data("id");
            var val=$(this).data("value");
            $.post('sub_cat_status/'+id,
                {
                    _token: "{{ csrf_token() }}",  
                    val: val
                }, 
                function(response){
                    location.reload();
                
            });
        }
    });
</script>
@endsection