@extends('admin::layouts.master')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Change Password page

      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Change Password</a></li>
        <li class="active">Change Password page</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Change Password</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif
            @if ($message = Session::get('error'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
            {{Form::open(array('url'=>'admin/update_password','id'=>'password-form','class'=>'form-horizontal','enctype' => 'multipart/form-data'))}}
        <div class="box-body">
            <div class="form-group">
                {{Form::label('old_password','Old Password',array('class'=>'col-sm-2 control-label'))}}
                <div class="col-sm-10">
                {{Form::password('old_password',array('class'=>'form-control','placeholder'=>'Enter Old Password'))}}
                </div>
            </div>
            <div class="form-group">
                {{Form::label('new_password','New Password',array('class'=>'col-sm-2 control-label'))}}
                <div class="col-sm-10">
                    {{Form::password('new_password',array('id'=>'new_password','class'=>'form-control','placeholder'=>'Enter New Password'))}}
                </div>
            </div>
            <div class="form-group">
                {{Form::label('confirm_password','Confirm Password',array('class'=>'col-sm-2 control-label'))}}
                <div class="col-sm-10">
                    {{Form::password('confirm_password',array('class'=>'form-control','placeholder'=>'Enter Confirm Password'))}}
                </div>
            </div>
              </div>
              <div class="box-footer pull-right">
                <button type="reset" class="btn btn-default">Cancel</button>
                <button type="submit" class="btn btn-info ">Submit</button>
              </div>
           {{Form::close()}}
        </div>
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
@endsection