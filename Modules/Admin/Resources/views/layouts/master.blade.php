<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>MeriGift</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ URL::asset('css/font-awesome.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ URL::asset('css/ionicons.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ URL::asset('css/AdminLTE.min.css')}}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{ URL::asset('css/_all-skins.min.css')}}">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="{{ URL::asset('js/bootstrap.min.js')}}"></script>
<script src="{{URL::asset('js/jquery.validate.min.js')}}"></script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="../../index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>M</b>G</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>MeriGift</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              @if(Auth::user()->image)
                <img src="{{URL::asset('user_image/'.Auth::user()->image)}}" class="user-image" alt="User Image">
              @else
                <img src="{{URL::asset('img/avatar5.png')}}" class="user-image" alt="User Image">
              @endif
              <span class="hidden-xs">{{Auth::user()->name}}</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                @if(Auth::user()->image)
                <img src="{{URL::asset('user_image/'.Auth::user()->image)}}" class="img-circle" alt="User Image">
                @else
                <img src="{{URL::asset('img/avatar5.png')}}" class="img-circle" alt="User Image">
                @endif
                <p>
                  {{Auth::user()->name}}
                </p>
              </li>
              <!-- Menu Body -->
              <li class="user-body">
                <div class="row">
                  <div class="col-xs-6 text-center">
                    <a href="{{url('admin/profile/'.Auth::user()->id)}}">Profile</a>
                  </div>
                  <div class="col-xs-6 text-center">
                    <a href="{{url('admin/change_password')}}">Change Password</a>
                  </div>
                </div>
                <!-- /.row -->
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
                <div class="pull-right">
                  <a class="btn btn-default btn-flat" href="{{ route('logout') }}"
                      onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                      Logout
                  </a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          @if(Auth::user()->image)
            <img src="{{URL::asset('user_image/'.Auth::user()->image)}}" class="img-circle" alt="User Image">
          @else
            <img src="{{URL::asset('img/avatar5.png')}}" class="img-circle" alt="User Image">
          @endif

        </div>
        <div class="pull-left info">
          <p>{{Auth::user()->name}}</p>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">

        <li>
          <a href="{{url('admin/dashboard')}}">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li>
          <a href="{{url('admin/category_list')}}">
            <i class="fa fa-th"></i> <span>Category</span>
          </a>
        </li>
        <li>
          <a href="{{url('admin/sub_category_list')}}">
            <i class="fa fa-th"></i> <span>Sub Category</span>
          </a>
        </li>
        <li>
          <a href="{{url('admin/product_type_list')}}">
            <i class="fa fa-th"></i> <span>Product Type</span>
          </a>
        </li>
        <li>
          <a href="{{url('admin/brand_list')}}">
            <i class="fa fa-th"></i> <span>Brand</span>
          </a>
        </li>
        <li>
          <a href="{{url('admin/product_list')}}">
            <i class="fa fa-th"></i> <span>Product</span>
          </a>
        </li>
        <li>
          <a href="{{url('admin/user_list')}}">
            <i class="fa fa-th"></i> <span>User</span>
          </a>
        </li>
        <li>
          <a href="{{url('admin/plan_list')}}">
            <i class="fa fa-th"></i> <span>Plan</span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  
  @yield('content')

  <footer class="main-footer">
    <strong>Copyright &copy; {{date('Y')}} <a href="#">MeriGift</a>.</strong> All rights
    reserved.
  </footer>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>

<script src="{{ URL::asset('js/jquery.slimscroll.min.js')}}"></script>
<script src="{{ URL::asset('js/fastclick.js')}}"></script>
<script src="{{URL::asset('js/adminlte.min.js')}}"></script>
<script src="{{URL::asset('js/demo.js')}}"></script>
<!-- <script src="{{URL::asset('js/additional-methods.js')}}"></script>
 -->
<script src="{{URL::asset('js/custom_validations.js')}}"></script>


</body>
</html>
