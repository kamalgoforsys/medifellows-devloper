@extends('admin::layouts.master')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Update Profile

      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Update Profile</a></li>

      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Update Profile</h3>


        </div>
        <div class="box-body">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif
            @if ($message = Session::get('error'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
                {{Form::model($profile,array('url'=>array('admin/update_profile',$profile->id),'id'=>'plan-form','class'=>'form-horizontal','enctype' => 'multipart/form-data'))}}
        <div class="box-body">
            <div class="form-group">
                {{Form::label('name','Name',array('class'=>'col-sm-2 control-label'))}}
                <div class="col-sm-10">
                {{Form::text('name',null,array('class'=>'form-control','placeholder'=>'Enter Name'))}}
                </div>
            </div>
            <div class="form-group">
                {{Form::label('contact_number','Contact Number',array('class'=>'col-sm-2 control-label'))}}
                <div class="col-sm-10">
                    {{Form::text('contact_number',null,array('id'=>'contact_number','class'=>'form-control','placeholder'=>'Enter Contact Number'))}}
                </div>
            </div>
            <div class="form-group">
                {{Form::label('image','Image',array('class'=>'col-sm-2 control-label'))}}
                <div class="col-sm-10">
                    {{Form::file('image',array('class'=>'form-control'))}}
                    @if($profile->image!='')
                        <span ><img src="{{URL::asset('user_image/'.$profile->image)}}"></span>
                    @endif
                </div>


            </div>

              </div>
              <div class="box-footer pull-right">
                <button type="reset" class="btn btn-default">Cancel</button>
                <button type="submit" class="btn btn-info ">Submit</button>
              </div>
           {{Form::close()}}
        </div>
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
@endsection