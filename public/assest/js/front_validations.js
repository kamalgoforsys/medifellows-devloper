$(function(){
    $("#product-form").validate({
        rules: {
            product_name: "required",
            category_id: "required",
            sub_category_id: "required",
            product_type_id: "required",
            brand_id:"required",
            product_price:"required"
        },
        messages:{
            product_name:{
                required:"Enter Brand",
            },
            category_id:{
                required:"Select Category",
            },
            sub_category_id:{
                required:"Select Sub Category",
            },
            product_type_id:{
                required:"Select Product Type",
            },
            brand_id:{
                required:"Select Brand",
            },
            product_price:{
                required:"Enter Product Price"
            }
        }
    });
    $("#plan-form").validate({
        rules: {
            title: "required",
            "features[]":"required",
            amount:"required"
        },
        messages:{
            title:{
                required:"Enter Plan Title",
            },
            "features[]":{
                required:"Select Features",
            },
            amount:{
                required:"Enter Amount",
            },
        }
    });
    $("#password-form").validate({
        rules: {
            old_password: "required",
            new_password: "required",
            confirm_password: {
                required:true,
                equalTo: "#new_password"
            }
        },
    });
    
});
$(document).ready(function(){
    $('#registry-item-form').validate({
        rules:{
            registries_id:"required",
            qty:{
                required:true,
                number:true,
            }
        }
    });
});