$(function(){
    $("#category-form").validate({
        rules: {
            cat_name: "required",
        },
        messages:{
            cat_name:{
                required:"Enter Category Name",
            }
        }
    });
    $("#sub-category-form").validate({
        rules: {
            category_id: "required",
            sub_cat_name:"required"
        },
        messages:{
            category_id:{
                required:"Select Category",
            },
            sub_cat_name:{
                required:"Enter Sub Category Name",
            }
        }
    });
    $("#product-type-form").validate({
        rules: {
            pro_type_name: "required",
            category_id:"required",
            sub_category_id:"required"
        },
        messages:{
            pro_type_name:{
                required:"Enter Product Type",
            },
            category_id:{
                required:"Select Category",
            },
            sub_category_id:{
                required:"Select Sub Category",
            }
        }
    });
    $("#brand-form").validate({
        rules: {
            brand_name: "required",
            brand_image:
            {
                required:function(){
                    return($("#brand-id").val() === '');
                },
                accept: "image/*"
            },
            brand_logo:{
                    required:function(){
                        return($("#brand-id").val() === '');
                    },
                    accept: "image/*"
                }
        },
        messages:{
            brand_name:{
                required:"Enter Brand",
            },
            brand_image:{
                accept:"Select Image file",
                required:"Select Brand Image",
            },
            brand_logo:{
                accept:"Select Image file",
                required:"Select Brand Logo",
            },
        }
    });
    $("#product-form").validate({
        rules: {
            product_name: "required",
            category_id: "required",
            sub_category_id: "required",
            product_type_id: "required",
            brand_id:"required",
            product_price:"required"
        },
        messages:{
            product_name:{
                required:"Enter Brand",
            },
            category_id:{
                required:"Select Category",
            },
            sub_category_id:{
                required:"Select Sub Category",
            },
            product_type_id:{
                required:"Select Product Type",
            },
            brand_id:{
                required:"Select Brand",
            },
            product_price:{
                required:"Enter Product Price"
            }
        }
    });
    $("#plan-form").validate({
        rules: {
            title: "required",
            "features[]":"required",
        },
        messages:{
            title:{
                required:"Enter Plan Title",
            },
            "features[]":{
                required:"Select Features",
            }
        }
    });
    $("#password-form").validate({
        rules: {
            old_password: "required",
            new_password: "required",
            confirm_password: {
                required:true,
                equalTo: "#new_password"
            } 
        },
    });
    
});