<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/testmail', 'ApiController_V1@testmail')->name('testmail');
Route::get('v1/logout/{id}', 'ApiController_V1@logout')->name('logout');
Route::get('/email-verify/{email}', 'ApiController_V1@EmailVerify')->name('EmailVerify');
Route::get('/confirm-email-verify/{email}', 'ApiController_V1@changeEmailVerify');
Route::get('/check-email-verify/{id}', 'ApiController@checkEmailVerify')->name('checkEmailVerify');

Route::group(['middleware' => ['Bearer']], function()
{
	
	Route::post('/registration', 'ApiController@registration')->name('registration');
	Route::post('/check-exist', 'ApiController@checkExist')->name('checkExist');
	Route::post('/is-email-unique', 'ApiController@isEmailUnique');
	Route::post('/login', 'ApiController@login')->name('login');
	Route::get('/country', 'ApiController@country')->name('country');
	Route::get('/level', 'ApiController@level')->name('level');
	Route::get('/speciality', 'ApiController@speciality')->name('speciality');
	Route::get('/sub-speciality', 'ApiController@subSpeciality')->name('subSpeciality');
	Route::get('/qualification', 'ApiController@qualification')->name('qualification');
	Route::get('/university', 'ApiController@university')->name('university');
	Route::get('/intership/{id}', 'ApiController@intership')->name('intership');
	Route::post('/resend-email-verify-link', 'ApiController@resendEmailVerifyLink')->name('resendEmailVerifyLink');
	Route::get('/privacy-policy', 'ApiController@privacyPolicy')->name('privacyPolicy');

	Route::post('/forgot-password', 'ApiController@forgotPassword');
	Route::post('/forgot-password-otp-verified', 'ApiController@forgotPasswordOTPVerified');
	Route::post('/forgot-password-set-new-password', 'ApiController@forgotPasswordChangePassword');
	Route::post('/check-company', 'ApiController@checkCompany');
	Route::post('/get-user-profile', 'ApiController@getUserProfile');
	Route::post('/send-email-otp', 'ApiController@sendResetPasswordOTP');
	Route::post('/update-password', 'ApiController@updateNewPassword');
	
});
Route::group(['middleware' => ['Bearer','Authtoken']], function()
{

	Route::post('/change-password', 'ApiController@changePassword')->name('changePassword');
	Route::post('/add-mobile', 'ApiController@addMobileNumber');
	Route::post('/resend-otp', 'ApiController@resendOTP');
	Route::post('/mobile-otp-verified', 'ApiController@mobileOTPVerified');

	
	Route::post('/complete-profile', 'ApiController@completeProfile')->name('completeProfile');
	Route::post('/update-profile', 'ApiController@updateProfile')->name('updateProfile');
	Route::post('/update-profile-pic', 'ApiController@updateProfilePic')->name('updateProfilePic');
	Route::post('/update-cover-pic', 'ApiController@updateCoverPic')->name('updateCoverPic');
	Route::get('/landing-page/{id}', 'ApiController@landingPage')->name('landingPage');
	Route::get('/remove-profile-pic/{id}', 'ApiController@removeProfilePic')->name('removeProfilePic');
	Route::post('/add-post', 'ApiController@addPost')->name('addPost');
	Route::get('/user-post/{id}/{page}', 'ApiController@userPost')->name('userPost');
	Route::post('/post-all-like', 'ApiController@postAllLike')->name('postAllLike');
	Route::post('/post-like-dislike', 'ApiController@postLikeDislike')->name('postLikeDislike');
	Route::get('/post-comments/{id}', 'ApiController@postComments')->name('postComments');
	Route::post('/post-add-comment', 'ApiController@postAddComment')->name('postAddComment');
	Route::post('/post-edit-comment', 'ApiController@postEditComment')->name('postEditComment');
	Route::post('/post-delete-comment', 'ApiController@postDeleteComment')->name('postDeleteComment');
	Route::post('/create-album', 'ApiController@createAlbum')->name('createAlbum');
	Route::post('/create-album', 'ApiController@createAlbum')->name('createAlbum');
	Route::get('/gallery/{id}', 'ApiController@gallery')->name('gallery');
	Route::get('/uploads-image-video/{id}', 'ApiController@uploadsImageVideo')->name('uploadsImageVideo');
	Route::post('/album-details', 'ApiController@albumDetails')->name('albumDetails');
	Route::post('/delete-post', 'ApiController@deletePost')->name('deletePost');
	Route::get('/user-album/{id}', 'ApiController@userAlbum')->name('userAlbum');
	Route::get('/user-friends/{id}', 'ApiController@userFriends')->name('eventDetails');
	Route::post('/create-private-event', 'ApiController@createPrivateEvent')->name('createPrivateEvent');
	Route::get('/user-privete-events/{id}', 'ApiController@userPriveteEvents')->name('userPriveteEvents');
	Route::get('/event-details/{id}', 'ApiController@eventDetails')->name('eventDetails');
	Route::post('/delete-event', 'ApiController@deleteEvent')->name('deleteEvent');
	Route::post('/create-public-event', 'ApiController@createPublicEvent')->name('createPublicEvent ');
	Route::post('/create-group-event', 'ApiController@createGroupEvent')->name('createGroupEvent ');
	Route::post('/event-join-response', 'ApiController@eventJoinResponse')->name('eventJoinResponse ');
	Route::post('/event-search', 'ApiController@eventSearch')->name('eventSearch ');
	Route::post('/post-share', 'ApiController@postShare')->name('postShare ');
	Route::post('/add-group', 'ApiController@addGroup');
	Route::post('/add-group-member', 'ApiController@addGroupMember');
	Route::post('/add-group-post', 'ApiController@addGroupPost');
	Route::get('/get-group-by-id/{id}', 'ApiController@getGroupById');
	Route::post('/exit-group', 'ApiController@exitGroup');

	Route::post('/add-community', 'ApiController@addCommunity');
	Route::post('/add-community-member', 'ApiController@addCommunityMember');
	Route::get('/get-community-by-id/{id}', 'ApiController@getCommunityById');
	Route::post('/add-community-post', 'ApiController@addCommunityPost');

	Route::post('/add-new-job', 'ApiController@addJob');
	Route::get('/get-all-jobs/{user_id}', 'ApiController@getAllJobByUser');
	Route::get('/get-job-by-id/{job_id}', 'ApiController@getJobById');
	Route::get('/pause-job/{job_id}', 'ApiController@pauseJob');
	Route::get('/delete-job/{job_id}', 'ApiController@deleteJob');
	Route::post('/apply-job', 'ApiController@applyJob');

	Route::post('/add-marketplace-product', 'ApiController@addMarketplaceProduct');
	Route::post('/delete-marketplace-product', 'ApiController@deleteMarketplaceProduct');
	Route::get('/marketplace-category', 'ApiController@marketPlaceCategory');
	Route::get('/marketplace', 'ApiController@marketPlace');
	Route::post('/marketplace-by-category', 'ApiController@marketPlaceByCategory');
	Route::get('/marketplace-product-details/{id}', 'ApiController@marketPlaceProductDetails');

	Route::post('/follow-user', 'ApiController@followUser');
	Route::post('/un-follow-user', 'ApiController@unFollowUser');
	Route::get('/user-follower/{id}', 'ApiController@userFollower');
	Route::get('/user-following/{id}', 'ApiController@userFollowing');
	Route::post('/search-user', 'ApiController@searchUser');
	
	Route::post('/librery-search', 'ApiController@librerySearch');
	Route::post('/user-block', 'ApiController@userBlock');
	Route::post('/user-unblock', 'ApiController@userUnBlock');
	Route::get('/user-block-list/{id}', 'ApiController@blockList');

	


});

///////////////////////////V1////////////////////////////////////////////
Route::group(['prefix'=>'v1'], function()
{
	Route::get('/privacy-policy', 'ApiController_V1@privacyPolicy')->name('privacyPolicy');
	Route::get('/terms', 'ApiController_V1@terms')->name('terms');
});
Route::group(['prefix'=>'v1','middleware' => ['Bearer']], function()
{   
	Route::get('/title', 'ApiController_V1@userTitle');
	Route::get('/email-verify/{email}', 'ApiController_V1@EmailVerify')->name('EmailVerify');
    Route::get('/check-email-verify/{id}', 'ApiController_V1@checkEmailVerify')->name('checkEmailVerify');

	Route::post('/registration', 'ApiController_V1@registration')->name('registration');
	Route::post('/check-exist', 'ApiController_V1@checkExist')->name('checkExist');
	Route::post('/is-email-unique', 'ApiController_V1@isEmailUnique');
	Route::post('/login', 'ApiController_V1@login')->name('login');
	Route::get('/login-user-info/{id}', 'ApiController_V1@loginUserInfo')->name('login');
	Route::get('/country', 'ApiController_V1@country')->name('country');
	Route::get('/level', 'ApiController_V1@level')->name('level');
	Route::get('/speciality', 'ApiController_V1@speciality')->name('speciality');
	Route::get('/sub-speciality', 'ApiController_V1@subSpeciality')->name('subSpeciality');
	Route::get('/qualification', 'ApiController_V1@qualification')->name('qualification');
	Route::get('/university', 'ApiController_V1@university')->name('university');
	Route::get('/intership/{id}', 'ApiController_V1@intership')->name('intership');
	Route::post('/resend-email-verify-link', 'ApiController_V1@resendEmailVerifyLink')->name('resendEmailVerifyLink');
	

	Route::post('/forgot-password', 'ApiController_V1@forgotPassword');
	Route::post('/forgot-password-otp-verified', 'ApiController_V1@forgotPasswordOTPVerified');
	Route::post('/forgot-password-set-new-password', 'ApiController_V1@forgotPasswordChangePassword');
	Route::post('/check-company', 'ApiController_V1@checkCompany');
	Route::post('/get-user-profile', 'ApiController_V1@getUserProfile');
	Route::post('/send-email-otp', 'ApiController_V1@sendResetPasswordOTP');
	Route::post('/update-password', 'ApiController_V1@updateNewPassword');
	
});
Route::group(['prefix'=>'v1','middleware' => ['Bearer','Authtoken']], function()
{
	Route::post('/master-delete', 'ApiController_V1@masterDelete')->name('masterDelete');
	Route::post('/change-password', 'ApiController_V1@changePassword')->name('changePassword');
	Route::post('/add-mobile', 'ApiController_V1@addMobileNumber');
	Route::post('/add-email', 'ApiController_V1@addEmail');
	Route::post('/add-email-verified', 'ApiController_V1@addEmailVerified');
	Route::post('/change-email', 'ApiController_V1@changeEmail');
	Route::post('/update-country', 'ApiController_V1@updateCountry');
	Route::post('/discoverability', 'ApiController_V1@discoverability');
	Route::post('/update-discoverability', 'ApiController_V1@updateDiscoverability');

	Route::get('/profile-blocked/{id}', 'ApiController_V1@profileBlocked');
	Route::post('/block-profile', 'ApiController_V1@blockProfile');
	Route::post('/un-block-profile', 'ApiController_V1@unBlockProfile');

	Route::post('/profile-add-mute', 'ApiController_V1@profileAddMute');
	Route::post('/profile-un-mute', 'ApiController_V1@profileUnMute');
	Route::get('/profile-muted-list/{id}', 'ApiController_V1@profileMutedList');

	Route::get('/profile-group-list/{id}', 'ApiController_V1@profileGroupList');
	Route::post('/profile-group-mute', 'ApiController_V1@profileGroupMute');
	Route::get('/profile-group-mute-list/{id}', 'ApiController_V1@profileMutedGroupList');
	Route::post('/profile-group-un-mute', 'ApiController_V1@profileGroupUnMute');
	Route::post('/profile-group-block', 'ApiController_V1@profileGroupBlock');
	Route::post('/profile-group-un-block', 'ApiController_V1@profileGroupUnBlock');
	Route::get('/profile-group-block-list/{id}', 'ApiController_V1@profileBlockedGroupList');
	Route::get('/photo-tagging/{id}', 'ApiController_V1@photoTagging');
	Route::post('/update-photo-tagging', 'ApiController_V1@updatePhotoTagging');


	Route::post('/mobile-otp-verified', 'ApiController_V1@mobileOTPVerified');

	Route::get('/get-profile/{id}', 'ApiController_V1@getProfile')->name('getProfile');
	Route::post('/update-about', 'ApiController_V1@updateAbout')->name('updateAbout');
	Route::post('/update-contact', 'ApiController_V1@updateContact')->name('updateContact');
	Route::post('/add-update-qulification', 'ApiController_V1@addUpdateQulification')->name('addUpdateQulification');
	Route::post('/add-update-education', 'ApiController_V1@addUpdateInterninfo')->name('addUpdateInterninfo');
	Route::post('/add-update-workplace', 'ApiController_V1@addUpdateWorkplace')->name('addUpdateWorkplace');
	Route::post('/update-intrest', 'ApiController_V1@updateIntrest')->name('updateIntrest');




	Route::post('/complete-profile', 'ApiController_V1@completeProfile')->name('completeProfile');
	Route::post('/update-profile', 'ApiController_V1@updateProfile')->name('updateProfile');
	Route::post('/update-profile-pic', 'ApiController_V1@updateProfilePic')->name('updateProfilePic');
	Route::post('/update-cover-pic', 'ApiController_V1@updateCoverPic')->name('updateCoverPic');
	Route::post('/profile-timeline', 'ApiController_V1@profileTimeline')->name('profileTimeline');
	Route::get('/remove-profile-pic/{id}', 'ApiController_V1@removeProfilePic')->name('removeProfilePic');
	Route::post('/add-post', 'ApiController_V1@addPost')->name('addPost');
	Route::post('/user-post', 'ApiController_V1@userPost')->name('userPost');
	Route::post('/delete-post', 'ApiController_V1@deletePost')->name('deletePost');
	Route::post('/user-feeds', 'ApiController_V1@userFeeds')->name('userFeeds');
	Route::post('/post-all-like', 'ApiController_V1@postAllLike')->name('postAllLike');
	Route::post('/post-like-dislike', 'ApiController_V1@postLikeDislike')->name('postLikeDislike');
	Route::post('/post-comments', 'ApiController_V1@postComments')->name('postComments');
	Route::post('/post-add-comment', 'ApiController_V1@postAddComment')->name('postAddComment');
	Route::post('/post-edit-comment', 'ApiController_V1@postEditComment')->name('postEditComment');
	Route::post('/post-delete-comment', 'ApiController_V1@postDeleteComment')->name('postDeleteComment');
	Route::post('/comment-like-dislike', 'ApiController_V1@commentLikeDislike')->name('commentLikeDislike');
	Route::post('/comment-reply', 'ApiController_V1@commentReply')->name('commentReply');
	Route::post('/post-details', 'ApiController_V1@postDetails')->name('postDetails');
	Route::post('/notification-list', 'ApiController_V1@notificationList')->name('notificationList');
	
	Route::post('/create-album', 'ApiController_V1@createAlbum')->name('createAlbum');
	Route::post('/create-album', 'ApiController_V1@createAlbum')->name('createAlbum');
	Route::get('/gallery/{id}', 'ApiController_V1@gallery')->name('gallery');
	Route::post('/album-upload', 'ApiController_V1@albumUpload')->name('albumUpload');
	Route::get('/uploads-image-video/{id}', 'ApiController_V1@uploadsImageVideo')->name('uploadsImageVideo');
	Route::post('/album-details', 'ApiController_V1@albumDetails')->name('albumDetails');
	
	Route::get('/user-album/{id}', 'ApiController_V1@userAlbum')->name('userAlbum');
	
	Route::get('/user-friends/{id}', 'ApiController_V1@userFriends')->name('eventDetails');
	Route::post('/create-event', 'ApiController_V1@createEvent')->name('createEvent');
	Route::post('/edit-event', 'ApiController_V1@editEvent')->name('editEvent');
	Route::get('/user-events/{id}', 'ApiController_V1@EventsHosted')->name('EventsHosted');
	Route::post('/event-details', 'ApiController_V1@eventDetails')->name('eventDetails');
	Route::post('/delete-event', 'ApiController_V1@deleteEvent')->name('deleteEvent');
	Route::post('/create-public-event', 'ApiController_V1@createPublicEvent')->name('createPublicEvent ');
	Route::post('/create-group-event', 'ApiController_V1@createGroupEvent')->name('createGroupEvent ');
	Route::post('/event-join-response', 'ApiController_V1@eventJoinResponse')->name('eventJoinResponse ');
	Route::post('/event-search', 'ApiController_V1@eventSearch')->name('eventSearch');
	Route::get('/user-groups/{id}', 'ApiController_V1@user_groups')->name('user_groups');
	Route::post('/event-invite', 'ApiController_V1@eventInviteFriends')->name('eventInviteFriends');
	Route::get('/event-home/{id}', 'ApiController_V1@EventHome')->name('EventHome');

	Route::post('/event-post-listing', 'ApiController_V1@eventPosts');
	Route::get('/event-media/{id}', 'ApiController_V1@eventMedia');
	Route::post('/add-event-post', 'ApiController_V1@eventAddPost');
	Route::get('/event-post-all-like/{id}', 'ApiController_V1@eventPostAllLike')->name('eventPostAllLike');
	Route::post('/event-post-like-dislike', 'ApiController_V1@eventPostLikeDislike')->name('eventPostLikeDislike');
	Route::post('/event-post-comments', 'ApiController_V1@eventPostComments')->name('eventPostComments');
	Route::post('/event-post-add-comment', 'ApiController_V1@eventPostAddComment')->name('eventPostAddComment');
	Route::post('/event-post-edit-comment', 'ApiController_V1@eventPostEditComment')->name('eventPostEditComment');
	Route::post('/event-post-delete-comment', 'ApiController_V1@eventPostDeleteComment')->name('eventPostDeleteComment');
	Route::post('/event-comment-like-dislike', 'ApiController_V1@eventCommentLikeDislike')->name('eventCommentLikeDislike');
	Route::post('/event-comment-reply', 'ApiController_V1@eventCommentReply')->name('eventCommentReply');
	Route::post('/event-post-share', 'ApiController_V1@eventPostShare')->name('eventPostShare');
	Route::post('/event-delete-post', 'ApiController_V1@eventDeletePost')->name('eventDeletePost');





	
	Route::post('/post-share', 'ApiController_V1@postShare')->name('postShare ');
	Route::post('/add-group', 'ApiController_V1@addGroup');
	Route::post('/edit-group', 'ApiController_V1@editGroup');
	Route::post('/add-group-member', 'ApiController_V1@addGroupMember');
	Route::post('/add-group-post', 'ApiController_V1@addGroupPost');
	Route::post('/group-details', 'ApiController_V1@groupDetails');
	Route::post('/exit-group-user', 'ApiController_V1@exitGroupUser');
	Route::post('/delete-group', 'ApiController_V1@deleteGroup');
	Route::post('/group-requests', 'ApiController_V1@groupRequestList');
	Route::post('/group-requests-update', 'ApiController_V1@groupRequestUpdate');
	Route::post('/group-friend-list', 'ApiController_V1@group_friends');
	Route::post('/group-members-list', 'ApiController_V1@groupMembersList');
	Route::post('/join-group', 'ApiController_V1@joinGroup');
	Route::post('/group-add-moderator', 'ApiController_V1@addModerators');
	Route::post('/group-remove-moderator', 'ApiController_V1@removeModerators');
	
	Route::post('/report-group-post', 'ApiController_V1@reportGroupPost');
	Route::get('/group-report-list/{id}', 'ApiController_V1@reportGroupList');
	Route::post('/group-request-admin', 'ApiController_V1@makeGroupAdmin');
	Route::post('/group-add-admin', 'ApiController_V1@responseMakeGroupAdmin');
	Route::post('/group-block', 'ApiController_V1@groupBlock');
	Route::post('/group-un-block', 'ApiController_V1@groupUnBlock');
	Route::post('/group-mute', 'ApiController_V1@groupMute');
	Route::post('/group-un-mute', 'ApiController_V1@groupUnMute');
	Route::get('/group-block-list/{id}', 'ApiController_V1@groupblockList');
	Route::get('/group-mute-list/{id}', 'ApiController_V1@groupMuteList');
	Route::get('/my-group-list/{id}', 'ApiController_V1@myGroups');
	
	Route::Post('/group-post-list', 'ApiController_V1@groupPosts');
	Route::Post('/group-search', 'ApiController_V1@groupSearch');
	Route::get('/group-media/{id}', 'ApiController_V1@GroupMedia');

	
	Route::get('/group-post-all-like/{id}', 'ApiController_V1@groupPostAllLike')->name('groupPostAllLike');
	Route::post('/group-post-like-dislike', 'ApiController_V1@groupPostLikeDislike')->name('groupPostLikeDislike');
	Route::post('/group-post-comments', 'ApiController_V1@groupPostComments')->name('groupPostComments');
	Route::post('/group-post-add-comment', 'ApiController_V1@groupPostAddComment')->name('groupPostAddComment');
	Route::post('/group-post-edit-comment', 'ApiController_V1@groupPostEditComment')->name('groupPostEditComment');
	Route::post('/group-post-delete-comment', 'ApiController_V1@groupPostDeleteComment')->name('groupPostDeleteComment');
	Route::post('/group-comment-like-dislike', 'ApiController_V1@groupCommentLikeDislike')->name('groupCommentLikeDislike');
	Route::post('/group-comment-reply', 'ApiController_V1@groupCommentReply')->name('groupCommentReply');
	Route::post('/group-post-share', 'ApiController_V1@groupPostShare')->name('groupPostShare');
	Route::post('/group-delete-post', 'ApiController_V1@groupDeletePost')->name('groupDeletePost');
	
	
	Route::post('/add-community', 'ApiController_V1@addCommunity');
	Route::post('/add-community-member', 'ApiController_V1@addCommunityMember');
	Route::get('/get-community-by-id/{id}', 'ApiController_V1@getCommunityById');
	Route::post('/add-community-post', 'ApiController_V1@addCommunityPost');

	Route::post('/add-new-job', 'ApiController_V1@addJob');
	Route::post('/edit-job', 'ApiController_V1@updateJob');
	Route::get('/get-all-jobs/{user_id}', 'ApiController_V1@getAllJobByUser');
	Route::post('/get-job-by-id', 'ApiController_V1@getJobById');
	Route::get('/pause-job/{job_id}', 'ApiController_V1@pauseJob');
	Route::get('/un-pause-job/{job_id}', 'ApiController_V1@unPauseJob');
	Route::get('/delete-job/{job_id}', 'ApiController_V1@deleteJob');
	Route::post('/apply-job', 'ApiController_V1@applyJob');
	Route::post('/search-job', 'ApiController_V1@jobSearch');
	Route::get('/saved-jobs/{id}', 'ApiController_V1@jobSaved');
	Route::post('/saved-job-delete/', 'ApiController_V1@jobRemoveSaved');
	Route::post('/add-saved-job/', 'ApiController_V1@addSavedJob');
	Route::post('/job-listing/', 'ApiController_V1@jobListing');

	Route::post('/add-marketplace-product', 'ApiController_V1@addMarketplaceProduct');
	Route::post('/edit-marketplace-product', 'ApiController_V1@editMarketplaceProduct');
	Route::post('/delete-marketplace-product', 'ApiController_V1@deleteMarketplaceProduct');
	Route::get('/marketplace-category', 'ApiController_V1@marketPlaceCategory');
	Route::get('/marketplace', 'ApiController_V1@marketPlace');
	Route::post('/marketplace-search', 'ApiController_V1@marketPlaceSearch');
	Route::post('/marketplace-saved', 'ApiController_V1@marketPlaceSaveRecent');
	Route::post('/marketplace-remove-saved', 'ApiController_V1@marketplaceRemoveSaved');
	Route::post('/marketplace-add-saved', 'ApiController_V1@addSavedProduct');
	Route::get('/my-marketPlace/{id}', 'ApiController_V1@myMarketPlace');
	Route::post('/marketplace-product-details', 'ApiController_V1@marketPlaceProductDetails');
	Route::post('/delete-marketplace-product', 'ApiController_V1@deleteMarketplaceProduct');


	Route::post('/globle-search', 'ApiController_V1@globleSearch');
	Route::post('/follow-user', 'ApiController_V1@followUser');
	Route::post('/add-friend', 'ApiController_V1@addFriend');
	Route::get('/friends-request/{id}', 'ApiController_V1@friendsRequest');
	Route::post('/response-friends-request', 'ApiController_V1@respondeFriendsRequest');
	Route::post('/un-follow-user', 'ApiController_V1@unFollowUser');
	Route::get('/user-follower/{id}', 'ApiController_V1@userFollower');
	Route::get('/user-following/{id}', 'ApiController_V1@userFollowing');
	Route::post('/search-user', 'ApiController_V1@searchUser');
	
	Route::post('/librery-search', 'ApiController_V1@librerySearch');
	Route::post('/user-block', 'ApiController_V1@userBlock');
	Route::post('/user-unblock', 'ApiController_V1@userUnBlock');
	Route::get('/user-block-list/{id}', 'ApiController_V1@blockList');

	Route::post('/user-mute', 'ApiController_V1@userMute');
	Route::post('/user-un-mute', 'ApiController_V1@userUnMute');
	Route::post('/friend-profile', 'ApiController_V1@viewFriendsprofile');
	Route::get('/suggested/{id}', 'ApiController_V1@suggested');
	Route::post('/un-friend', 'ApiController_V1@unFriend');
	

	Route::post('/add-page', 'ApiController_V1@addUpdatePage');
	Route::post('/edit-page', 'ApiController_V1@addUpdatePage');
	Route::post('/update-publish', 'ApiController_V1@updatePublish');
	Route::post('/page-details', 'ApiController_V1@pageDetails');
	Route::post('/page-post-listing', 'ApiController_V1@pagePosts');
	Route::get('/page-media/{id}', 'ApiController_V1@pageMedia');
	Route::post('/page-add-post', 'ApiController_V1@pageAddPost');
	Route::get('/page-post-all-like/{id}', 'ApiController_V1@pagePostAllLike');
	Route::post('/page-post-like-dislike', 'ApiController_V1@pagePostLikeDislike');
	Route::post('/page-post-comments', 'ApiController_V1@pagePostComments');
	Route::post('/page-post-add-comment', 'ApiController_V1@pagePostAddComment');
	Route::post('/page-post-edit-comment', 'ApiController_V1@pagePostEditComment');
	Route::post('/page-post-delete-comment', 'ApiController_V1@pagePostDeleteComment');
	Route::post('/page-comment-like-dislike', 'ApiController_V1@pageCommentLikeDislike');
	Route::post('/page-comment-reply', 'ApiController_V1@pageCommentReply');
	Route::post('/page-post-share', 'ApiController_V1@pagePostShare');
	Route::post('/page-delete-post', 'ApiController_V1@pageDeletePost');
	Route::post('/page-add-review', 'ApiController_V1@pageAddReview');
	Route::post('/page-review', 'ApiController_V1@pageReview');
	Route::post('/page-follow', 'ApiController_V1@FollowPage');
	Route::post('/my-pages', 'ApiController_V1@myPages');




});
